package net.tardis.mod.schematics;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.registries.SchematicTypes;
import net.tardis.mod.schematics.types.SchematicType;
import net.tardis.mod.tileentities.ConsoleTile;

/**
 * Template for an generic object that can be used to unlock objects for a Tardis
 * <br> This is used in Spectrometer Recipes
 * <p> This object is data driven via JSONs. 
 * <br> JSON files are stored under data/YourModID/schematics
 * <p> Example JSON Format:
 * <pre>
 * {
 *   "type:"tardis:console", - This is the Schematic Type's Registry ID
 *   "display_name":"Steam Console", - Display text for users
 *   "translated": false, - If this Schematic will use Language files to populate its display name. If true, use a translation key such as "schematic.mymod.my_schematic"
 *   [Schematic Type's extra data here] - This can be anything ranging from a simple "result" field defining the result or custom conditions defined by the mod
 * }
 * </pre>
 * */
public abstract class Schematic{
	
	protected ResourceLocation key;
	private SchematicType type;
	private String displayName = "";
	private TranslationTextComponent translatedName = null;
	private boolean usesTranslatedName = false;

	public Schematic(SchematicType type){
		this.type = type;
	}

	/** Handle what to do if the item holding this schematic was added to the console's sonic port*/
	public abstract void onConsumedByTARDIS(ConsoleTile tile, PlayerEntity player);

	public ResourceLocation getId(){
		return key;
	}

	public Schematic setId(ResourceLocation key){
		this.key = key;
		return this;
	}

	public SchematicType getType(){
		return this.type;
	}

	public String getDisplayName() {

		if(this.translatedName != null && this.usesTranslatedName)
			return translatedName.getString();

		return this.displayName;
	}

	public void setDisplayName(String displayName){
		this.displayName = displayName;
		this.usesTranslatedName = false;
	}

    /** Set the Translation Key for this Schematic */
	public void setTranslation(String translationKey){
		this.translatedName = new TranslationTextComponent(translationKey);
		this.usesTranslatedName = true;
	}

    /** If this Schematic should use a translated name.*/
	public boolean isUsingTranslatedName(){
		return this.usesTranslatedName;
	}
	
    /** Set if this Schematic will use a translated name */
	public void setUseTranslatedName(boolean isTranslated) {
		this.usesTranslatedName = isTranslated;
	}
	
	/** Handles serialisation of the schematic to a packet. INTERNAL USE ONLY*/
    public static void encodeToPacket(Map<ResourceLocation, Schematic> inputData, PacketBuffer buffer) {
    	//The order of writing must match the order and object type as the decodeFromPacket function
    	buffer.writeInt(inputData.size()); //1 - Integer
        for (Schematic schematic : inputData.values()) {
            buffer.writeResourceLocation(schematic.getType().getRegistryName()); //2 - ResourceLocation
            schematic = schematic.getType().serialize(schematic, buffer);
            buffer.writeResourceLocation(schematic.getId()); //3 - ResourceLocation
            buffer.writeBoolean(schematic.isUsingTranslatedName()); //4 - Boolean
            buffer.writeString(schematic.getDisplayName()); //5 - String
        }
    }
    
    /** Handles deserialisation of the schematic from a packet. INTERNAL USE ONLY*/
    public static Map<ResourceLocation, Schematic> decodeFromPacket(PacketBuffer buffer) {
    	//The order of reading must match the order and object type as the encodeToPacket function
    	Map<ResourceLocation, Schematic> schematics = new HashMap<>();
        int schematicSize = buffer.readInt(); //1 - Integer
        for(int i = 0; i < schematicSize; ++i) {
            SchematicType type = SchematicTypes.REGISTRY.get().getValue(buffer.readResourceLocation()); // 2 - ResourceLocation
            Schematic schematic = type.deserialize(buffer);
            ResourceLocation schematicID = buffer.readResourceLocation(); //3 - ResourceLocation
            schematic.setId(schematicID);
            schematic.setUseTranslatedName(buffer.readBoolean()); //4 - Boolean
            if (schematic.isUsingTranslatedName())
                schematic.setDisplayName(buffer.readString(Constants.PACKET_STRING_LENGTH)); //5 - String - case 1
            else
            	schematic.setTranslation(buffer.readString(Constants.PACKET_STRING_LENGTH)); //5 - String - case 2
            schematics.put(schematicID, schematic);
        }
        return schematics;
    }

}
