package net.tardis.mod.network.packets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import com.google.common.collect.Lists;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.fml.network.NetworkEvent.Context;
import net.tardis.mod.client.ClientPacketHandler;
import net.tardis.mod.client.guis.monitors.WaypointMonitorScreen;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.network.Network;
import net.tardis.mod.tileentities.WaypointBankTile;

public class WaypointOpenMessage {

	@Nullable
	Map<BlockPos, List<SpaceTimeCoord>> map;
	
	public WaypointOpenMessage(Map<BlockPos, List<SpaceTimeCoord>> map) {
		this.map = map;
	}
	
	@Nullable
	public Map<BlockPos, List<SpaceTimeCoord>> getWaypointMap() {
		return this.map;
	}
	
	public static void encode(WaypointOpenMessage mes, PacketBuffer buffer) {
		
		if(mes.map == null) {
			buffer.writeInt(0);
			return;
		}
		
		buffer.writeInt(mes.map.size());
		for(Entry<BlockPos, List<SpaceTimeCoord>> entry : mes.map.entrySet()) {
			buffer.writeBlockPos(entry.getKey());
			buffer.writeInt(entry.getValue().size());
			for(SpaceTimeCoord coord : entry.getValue()) {
				buffer.writeCompoundTag(coord.serialize());
			}
		}
	}
	
	public static WaypointOpenMessage decode(PacketBuffer buffer) {
		
		Map<BlockPos, List<SpaceTimeCoord>> coords = new HashMap<BlockPos, List<SpaceTimeCoord>>();
		int size = buffer.readInt();
		
		if(size == 0)
			return new WaypointOpenMessage(null);
		
		for(int i = 0; i < size; ++i) {
			BlockPos pos = buffer.readBlockPos();
			int size1 = buffer.readInt();
			List<SpaceTimeCoord> c = new ArrayList<>();
			for(int x = 0; x < size1; ++x){
				c.add(SpaceTimeCoord.deserialize(buffer.readCompoundTag()));
			}
			coords.put(pos, c);
		}
		
		return new WaypointOpenMessage(coords);
	}
	
	public static void handle(WaypointOpenMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			
			//On Server
			if(context.get().getDirection() == NetworkDirection.PLAY_TO_SERVER) {
				final HashMap<BlockPos, List<SpaceTimeCoord>> map = new HashMap<BlockPos, List<SpaceTimeCoord>>();
				
				for(TileEntity te : context.get().getSender().world.loadedTileEntityList) {
					if(te instanceof WaypointBankTile) {
						
						WaypointBankTile bank = (WaypointBankTile)te;
						
						List<SpaceTimeCoord> coords = Lists.newArrayList(bank.getWaypoints());
						
						map.put(bank.getPos(), coords);
						
					}
				}
				
				Network.sendTo(new WaypointOpenMessage(map), context.get().getSender());
				
			}
			else {
				ClientPacketHandler.handleWaypointOpenClient(mes);
			}
			
		});
		context.get().setPacketHandled(true);
	}
}
