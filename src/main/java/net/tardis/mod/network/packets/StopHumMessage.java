package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.client.Minecraft;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.registries.InteriorHumRegistry;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.sounds.InteriorHum;

public class StopHumMessage{

    private String soundPath;

    public StopHumMessage() {
    }

    public StopHumMessage(String soundPath) {
        this.soundPath = soundPath;
    }

    public static void encode(StopHumMessage mes, PacketBuffer buf) {
        buf.writeString(mes.soundPath);
    }

    public static StopHumMessage decode(PacketBuffer buf) {
        return new StopHumMessage(buf.readString(25));
    }

    public static void handle(StopHumMessage mes,  Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(()->{
            InteriorHum humToStop = InteriorHumRegistry.HUM_REGISTRY.get().getValue(new ResourceLocation(Tardis.MODID, mes.soundPath));
            Minecraft.getInstance().getSoundHandler().stop(humToStop.getLoopedSoundEvent().getName(), SoundCategory.AMBIENT);
        });
        ctx.get().setPacketHandled(true);
    }
}

