package net.tardis.mod.client.guis.manual;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import com.google.common.collect.Lists;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.datafixers.util.Pair;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.client.gui.widget.button.ChangePageButton;
import net.minecraft.item.ItemStack;
import net.minecraft.resources.IResource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.guis.manual.pages.Page;
import net.tardis.mod.client.guis.widgets.ChangeChapterButton;
import net.tardis.mod.contexts.gui.GuiItemContext;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.UpdateManualPageMessage;

public class ManualScreen extends Screen {

    public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/manual.png");
    private List<Chapter> chapters = Lists.newArrayList();

    private int pageIndex = 0;
    private int chapterIndex = 0;
    private String modid;

    protected ManualScreen(ITextComponent titleIn, String modid) {
        super(titleIn);
        this.modid = modid;
        this.read();
    }

    public ManualScreen(GuiContext context){
        this(new StringTextComponent("Manual"), Tardis.MODID);
        if (context instanceof GuiItemContext) {
            ItemStack stack = ((GuiItemContext) context).getItemStack();
            if (stack.hasTag()) {
            	if(stack.getTag().contains("page"))
                    pageIndex = stack.getTag().getInt("page");
            	if (stack.getTag().contains("chapter"))
            		chapterIndex = stack.getTag().getInt("chapter");
            }
        }
    }
    
    @Override
    public void closeScreen() {
        Network.sendToServer(new UpdateManualPageMessage(this.pageIndex, this.chapterIndex));
        super.closeScreen();
    }

    @Override
    protected void init() {
        super.init();

        this.buttons.clear();

        this.addButton(new ChangePageButton(width / 2 + 85, height / 2 + 45, true, button -> {
            this.turnPage(true);
        }, true));

        this.addButton(new ChangePageButton(width / 2 - 110, height / 2 + 45, false, button -> {
            this.turnPage(false);
        }, true));
        
        this.addButton(new ChangeChapterButton(width / 2 + 95, height / 2 - 85, true, button -> {
            this.turnChapter(true);
        }, true));

        this.addButton(new ChangeChapterButton(width / 2 - 110, height / 2 - 85, false, button -> {
            this.turnChapter(false);
        }, true));

    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        this.renderBackground(matrixStack);
        Minecraft.getInstance().getTextureManager().bindTexture(this.getTexture());
        this.blit(matrixStack, (width - 256) / 2, (height - 187) / 2, 0, 0, 256, 187);

        super.render(matrixStack, mouseX, mouseY, partialTicks);

        int x = width / 2 - 110, y = height / 2 - 70;

        //Render Pages
        Pair<Page, Page> pages = this.getPages();
        if(pages.getFirst() != null) {
            pages.getFirst().render(matrixStack, this.font, this.getGlobalPageNumber(), x, y, this.width, this.height);
        }
        if(pages.getSecond() != null){
        	pages.getSecond().render(matrixStack, this.font, this.getGlobalPageNumber() + 1, x + 120, y, this.width, this.height);
        }
    }

    public void turnPage(boolean forward){

        //if can change page in same chapter
        Chapter currentChapter = this.getChapter();
        if(currentChapter == null){
            this.chapterIndex = 0;
            return;
        }

        if(forward){
            if(this.pageIndex + 2 < currentChapter.getPages().size()){
                this.pageIndex += 2;
            }
            //If outside the chapter
            else{
                //Try to get next chapter
                if(this.chapterIndex + 1 < this.chapters.size()){
                    this.chapterIndex += 1;
                    this.pageIndex = 0;
                }
            }
        }
        //If turning back
        else {
            //If still in chapter
            if(this.pageIndex - 2 >= 0)
                this.pageIndex -= 2;
            //if out of chapter
            else{
                //try to get the previous chapter
                if(this.chapterIndex - 1 >= 0){
                    this.chapterIndex -= 1;
                    this.pageIndex = this.getChapter().getPages().size() - 1;
                }
            }
        }

    }
    
    public void turnChapter(boolean forward) {
    	Chapter currentChapter = this.getChapter();
        if(currentChapter == null){
            this.chapterIndex = 0;
            return;
        }
        
        if(forward){
            if(this.chapterIndex + 1 < chapters.size()){
                this.chapterIndex += 1;
                this.pageIndex = 0;
            }
        }
        else {
        	//try to get the previous chapter
            if(this.chapterIndex - 1 >= 0){
                this.chapterIndex -= 1;
                this.pageIndex = 0;
            }
        }
    }
    
    public void turnToChapter(int chapterIndex) {
    	if (chapterIndex < chapters.size()) {
    		this.chapterIndex = chapterIndex;
    		this.pageIndex = 0;
    	}
    }

    public int getGlobalPageNumber(){
        int pages = 0;
        for(int i = 0; i < this.chapterIndex; ++i){
            pages += this.chapters.get(i).getPages().size();
        }
        return pages + this.pageIndex;
    }

    public Chapter getChapter(){
        if(this.chapterIndex < this.chapters.size())
            return this.chapters.get(this.chapterIndex);
        return null;
    }
    
    public int getChapterIndex() {
    	return this.chapterIndex;
    }

    public Pair<Page, Page> getPages(){
        Chapter chapter = this.getChapter();
        if(chapter != null && this.pageIndex < chapter.getPages().size()){

            Page page2 = null;

            if(this.pageIndex + 1 < chapter.getPages().size())
                page2 = chapter.getPages().get(this.pageIndex + 1);

            return new Pair<Page, Page>(this.getChapter().getPages().get(this.pageIndex), page2);
        }
        return new Pair<Page, Page>(null, null);
    }
    
    public ResourceLocation getTexture() {
        return TEXTURE;
    }

    @Override
	public <T extends Widget> T addButton(T button) {
		return super.addButton(button);
	}

	private void read(){
    	try {
    		ResourceLocation indexLocation = this.getManualIndexResourceLocation();
    		IResource resource = getManualResourceNullable(indexLocation);
            if (resource == null) {
            	indexLocation = Helper.createRL("manual/en_us/index/main.json");
                resource = getManualResourceNullable(indexLocation);
            }
            JsonObject root = new JsonParser().parse(new InputStreamReader(resource.getInputStream())).getAsJsonObject();
            Index index = Index.read(indexLocation, root);
            //Pull all chapters
            this.chapters.clear();

            for(Chapter chapter : index.getChapters()){
                this.chapters.add(chapter);
            }
    	}
    	catch(IOException e){
            e.printStackTrace();
        }
        
    }

    public ResourceLocation getManualIndexResourceLocation() {
        return Index.getIndexResourceLocation(new ResourceLocation(this.modid, "main"));
    }
    
    private static IResource getManualResourceNullable(ResourceLocation rl) throws IOException{
        return Minecraft.getInstance().getResourceManager().getResource(rl);
    }
    
}