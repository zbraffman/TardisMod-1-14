package net.tardis.mod.client.guis.widgets;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SimpleSound;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.text.StringTextComponent;
import net.tardis.mod.client.guis.manual.ManualScreen;

public class ChangeChapterButton extends Button{
    private final boolean isForward;
    private final boolean playTurnSound;

	public ChangeChapterButton(int x, int y, boolean isForward, IPressable onPress, boolean playTurnSound) {
		super(x, y, 12, 16, StringTextComponent.EMPTY, onPress);
		this.isForward = isForward;
		this.playTurnSound = playTurnSound;
	}

	@Override
	public void renderWidget(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
	      Minecraft.getInstance().getTextureManager().bindTexture(ManualScreen.TEXTURE);
	      int u = 5;
	      int v = 228;
	      if (this.isHovered()) {
	         u += 22;
	      }

	      if (!this.isForward) {
	         v += 14;
	      }

	      this.blit(matrixStack, this.x, this.y, u, v, 17, 12);
	}
	
	@Override
	public void playDownSound(SoundHandler handler) {
      if (this.playTurnSound) {
         handler.play(SimpleSound.master(SoundEvents.ITEM_BOOK_PAGE_TURN, 1.0F));
      }
   }
	

}
