package net.tardis.mod.client.animation;

import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public abstract class ExteriorAnimation implements IExteriorAnimation {
	
	protected ExteriorTile exterior;
	private ExteriorAnimationEntry type;
	
	public ExteriorAnimationEntry getType(){
		return type;
	}

    public ExteriorAnimation(ExteriorAnimationEntry entry, ExteriorTile tile) {
		this.type = entry;
		this.exterior = tile;
	}
	
}