package net.tardis.mod.client.models.consoles;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.tardis.mod.client.models.LightModelRenderer;
import net.tardis.mod.client.models.TileModel;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.controls.IncModControl;
import net.tardis.mod.controls.LandingTypeControl;
import net.tardis.mod.controls.LandingTypeControl.EnumLandType;
import net.tardis.mod.controls.RandomiserControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.controls.XControl;
import net.tardis.mod.controls.YControl;
import net.tardis.mod.controls.ZControl;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;

public class CoralConsoleModel extends EntityModel< Entity > implements TileModel< CoralConsoleTile > {
    private final ModelRenderer all;
    private final ModelRenderer rotor_orig;
    private final ModelRenderer rotarcase2;
    private final ModelRenderer block;
    private final ModelRenderer lever6;
    private final ModelRenderer wires8;
    private final ModelRenderer wires7;
    private final ModelRenderer mirror;
    private final ModelRenderer wires6;
    private final ModelRenderer wires5;
    private final ModelRenderer wires4;
    private final ModelRenderer buttons;
    private final ModelRenderer wires3;
    private final ModelRenderer wires2;
    private final ModelRenderer wires;
    private final ModelRenderer bone112;
    private final ModelRenderer bone111;
    private final ModelRenderer bone106;
    private final ModelRenderer land_type;
    private final ModelRenderer bone103;
    private final ModelRenderer important_controls;
    private final ModelRenderer fastreturn;
    private final ModelRenderer lever;
    private final ModelRenderer refuel;
    private final ModelRenderer doorcontrol;
    private final ModelRenderer keyboardxyz;
    private final ModelRenderer throttle;
    private final ModelRenderer lever3;
    private final ModelRenderer Sonicport;
    private final ModelRenderer Incrementincrease;
    private final ModelRenderer lever2;
    private final ModelRenderer monitor;
    private final ModelRenderer handbreak;
    private final ModelRenderer lever4;
    private final ModelRenderer lever5;
    private final ModelRenderer wheel;
    private final ModelRenderer randomiser_and_facingcontrol;
    private final ModelRenderer throttle3;
    private final ModelRenderer facing_control;
    private final ModelRenderer randomiser;
    private final ModelRenderer Comuni_Phone;
    private final ModelRenderer telepathic_circuit;
    private final ModelRenderer crystal;
    private final ModelRenderer lever8;
    private final ModelRenderer nope;
    private final ModelRenderer keyboard_behind_monitor;
    private final ModelRenderer keyboard_by_monitor;
    private final ModelRenderer bone100;
    private final ModelRenderer bone101;
    private final ModelRenderer bone102;
    private final ModelRenderer bone99;
    private final ModelRenderer bone92;
    private final ModelRenderer control6;
    private final ModelRenderer control4;
    private final ModelRenderer control2;
    private final LightModelRenderer x;
	private final LightModelRenderer y;
	private final LightModelRenderer z;
    private final ModelRenderer Shell;
    private final ModelRenderer Shellone;
    private final ModelRenderer bone;
    private final ModelRenderer bone2;
    private final ModelRenderer bone3;
    private final ModelRenderer bone4;
    private final ModelRenderer bone5;
    private final ModelRenderer bone7;
    private final ModelRenderer bone6;
    private final ModelRenderer Shellone2;
    private final ModelRenderer bone8;
    private final ModelRenderer bone9;
    private final ModelRenderer bone10;
    private final ModelRenderer bone11;
    private final ModelRenderer bone12;
    private final ModelRenderer bone13;
    private final ModelRenderer bone14;
    private final ModelRenderer Shellone3;
    private final ModelRenderer bone15;
    private final ModelRenderer bone16;
    private final ModelRenderer bone17;
    private final ModelRenderer bone18;
    private final ModelRenderer bone19;
    private final ModelRenderer bone20;
    private final ModelRenderer bone21;
    private final ModelRenderer Shellone4;
    private final ModelRenderer bone22;
    private final ModelRenderer bone23;
    private final ModelRenderer bone24;
    private final ModelRenderer bone25;
    private final ModelRenderer bone26;
    private final ModelRenderer bone27;
    private final ModelRenderer bone28;
    private final ModelRenderer Shellone5;
    private final ModelRenderer bone29;
    private final ModelRenderer bone30;
    private final ModelRenderer bone31;
    private final ModelRenderer bone32;
    private final ModelRenderer bone33;
    private final ModelRenderer bone34;
    private final ModelRenderer bone35;
    private final ModelRenderer Shellone6;
    private final ModelRenderer bone36;
    private final ModelRenderer bone37;
    private final ModelRenderer bone38;
    private final ModelRenderer bone39;
    private final ModelRenderer bone40;
    private final ModelRenderer bone41;
    private final ModelRenderer bone42;
    private final ModelRenderer bone50;
    private final ModelRenderer bone51;
    private final ModelRenderer bone52;
    private final ModelRenderer bone53;
    private final ModelRenderer bone54;
    private final ModelRenderer bone55;
    private final ModelRenderer bone56;
    private final ModelRenderer bone57;
    private final ModelRenderer bone58;
    private final ModelRenderer bone59;
    private final ModelRenderer bone60;
    private final ModelRenderer bone61;
    private final ModelRenderer bone62;
    private final ModelRenderer bone63;
    private final ModelRenderer bone64;
    private final ModelRenderer bone65;
    private final ModelRenderer bone66;
    private final ModelRenderer bone67;
    private final ModelRenderer bone68;
    private final ModelRenderer bone69;
    private final ModelRenderer bone70;
    private final ModelRenderer bone71;
    private final ModelRenderer bone72;
    private final ModelRenderer bone73;
    private final ModelRenderer bone74;
    private final ModelRenderer bone75;
    private final ModelRenderer bone76;
    private final ModelRenderer bone77;
    private final ModelRenderer bone78;
    private final ModelRenderer bone79;
    private final ModelRenderer bone80;
    private final ModelRenderer bone81;
    private final ModelRenderer bone82;
    private final ModelRenderer bone83;
    private final ModelRenderer bone84;
    private final ModelRenderer bone85;
    private final ModelRenderer bone43;
    private final ModelRenderer bone86;
    private final ModelRenderer bone87;
    private final ModelRenderer bone88;
    private final ModelRenderer bone89;
    private final ModelRenderer bone90;
    private final ModelRenderer bone91;
    private final ModelRenderer bone93;
    private final ModelRenderer bone94;
    private final ModelRenderer bone95;
    private final ModelRenderer bone96;
    private final ModelRenderer bone97;
    private final ModelRenderer bone98;
    private final ModelRenderer bone105;
    private final LightModelRenderer glow;
    private final LightModelRenderer keyboard_behind_monitor_glow;
    private final LightModelRenderer keyboard_by_monitor_glow;
    private final ModelRenderer bone44;
    private final ModelRenderer refuel2;
    private final ModelRenderer doorcontrol2;
    private final ModelRenderer land_type_red;
    private final LightModelRenderer monitor_screen;
    private final LightModelRenderer throttle_glow;
    private final LightModelRenderer shell_glow;
    private final ModelRenderer bone150;
    private final ModelRenderer bone151;
    private final ModelRenderer bone152;
    private final ModelRenderer bone153;
    private final ModelRenderer bone154;
    private final ModelRenderer bone155;
    private final LightModelRenderer rotartop;
    private final LightModelRenderer rotarbottom;
    private final ModelRenderer phone;
    private final LightModelRenderer land_type_glow;
    private final LightModelRenderer telepathic_circuit_glow;
    private final LightModelRenderer crystal2;
    private final LightModelRenderer lever7;
    private final ModelRenderer nope2;
    private final LightModelRenderer block2_glow;
    private final LightModelRenderer lever9;
    private final LightModelRenderer bone45_glow;

    public CoralConsoleModel() {
        textureWidth = 200;
        textureHeight = 200;

        all = new ModelRenderer(this);
        all.setRotationPoint(-0.2F, 24.0F, 0.0F);


        rotor_orig = new ModelRenderer(this);
        rotor_orig.setRotationPoint(0.0F, 0.0F, 0.0F);
        all.addChild(rotor_orig);


        rotarcase2 = new ModelRenderer(this);
        rotarcase2.setRotationPoint(0.5F, -47.2F, -2.0F);
        rotor_orig.addChild(rotarcase2);
        rotarcase2.setTextureOffset(147, 16).addBox(-1.5F, -27.0F, 7.7F, 2.0F, 42.0F, 1.0F, 0.0F, false);
        rotarcase2.setTextureOffset(147, 16).addBox(-1.5F, -27.0F, -4.3F, 2.0F, 42.0F, 1.0F, 0.0F, false);
        rotarcase2.setTextureOffset(147, 16).addBox(-6.6F, -27.0F, 1.1F, 1.0F, 42.0F, 2.0F, 0.0F, false);
        rotarcase2.setTextureOffset(147, 16).addBox(5.0F, -27.0F, 1.1F, 1.0F, 42.0F, 2.0F, 0.0F, false);

        block = new ModelRenderer(this);
        block.setRotationPoint(6.9F, -27.5F, 1.4F);
        all.addChild(block);
        setRotationAngle(block, -0.0524F, -1.0472F, -0.0873F);


        lever6 = new ModelRenderer(this);
        lever6.setRotationPoint(-2.9869F, -0.7455F, 16.8686F);
        block.addChild(lever6);
        setRotationAngle(lever6, 0.0F, 0.0F, 0.0349F);
        lever6.setTextureOffset(140, 180).addBox(-2.7968F, 0.7509F, -0.0227F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        lever6.setTextureOffset(140, 180).addBox(-2.5976F, 2.2172F, 7.4084F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        lever6.setTextureOffset(145, 142).addBox(-1.7935F, -0.3712F, 1.5765F, 2.0F, 2.0F, 1.0F, 0.1F, false);
        lever6.setTextureOffset(145, 142).addBox(-2.5944F, 1.6028F, 3.9886F, 2.0F, 2.0F, 2.0F, 0.1F, false);
        lever6.setTextureOffset(149, 173).addBox(-2.589F, 1.0077F, 3.9121F, 2.0F, 2.0F, 2.0F, -0.1F, false);
        lever6.setTextureOffset(145, 142).addBox(-1.7935F, -0.3712F, -5.4235F, 2.0F, 2.0F, 1.0F, 0.1F, false);
        lever6.setTextureOffset(140, 180).addBox(-3.7968F, 0.7509F, -1.0227F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        lever6.setTextureOffset(140, 180).addBox(-1.4887F, 2.2419F, 6.3644F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        lever6.setTextureOffset(140, 180).addBox(-3.5976F, 2.2172F, 6.4084F, 1.0F, 1.0F, 1.0F, -0.2F, false);

        wires8 = new ModelRenderer(this);
        wires8.setRotationPoint(-0.4F, -33.9F, 2.6F);
        all.addChild(wires8);
        setRotationAngle(wires8, -0.3491F, 0.5236F, 0.0175F);
        wires8.setTextureOffset(0, 0).addBox(-0.6984F, 7.1499F, 13.4498F, 8.0F, 1.0F, 1.0F, -0.1F, false);
        wires8.setTextureOffset(170, 173).addBox(0.3016F, 7.1499F, 13.4498F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires8.setTextureOffset(48, 32).addBox(3.3016F, 7.1499F, 13.4498F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires8.setTextureOffset(0, 0).addBox(-1.3984F, 7.1499F, 8.4498F, 1.0F, 1.0F, 6.0F, -0.1F, false);
        wires8.setTextureOffset(170, 173).addBox(-1.3984F, 7.1499F, 12.4498F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires8.setTextureOffset(170, 173).addBox(-1.3984F, 7.1499F, 9.4498F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires8.setTextureOffset(178, 190).addBox(-5.0843F, 8.0173F, 18.5024F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        wires8.setTextureOffset(148, 140).addBox(-4.9843F, 7.6173F, 20.5024F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        wires8.setTextureOffset(158, 136).addBox(-4.0843F, 7.6173F, 20.5024F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        wires8.setTextureOffset(158, 136).addBox(-3.1843F, 7.6173F, 20.5024F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        wires8.setTextureOffset(158, 136).addBox(-3.1843F, 7.6173F, 19.5024F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        wires8.setTextureOffset(158, 136).addBox(0.4134F, 7.6475F, 19.2662F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        wires8.setTextureOffset(178, 169).addBox(1.7855F, 7.5573F, 16.0213F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        wires8.setTextureOffset(148, 140).addBox(-4.0843F, 7.6173F, 19.5024F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        wires8.setTextureOffset(148, 140).addBox(-0.4866F, 7.6475F, 19.2662F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        wires8.setTextureOffset(148, 140).addBox(0.8855F, 7.5573F, 16.0213F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        wires8.setTextureOffset(153, 97).addBox(-4.9843F, 7.6173F, 19.5024F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        wires8.setTextureOffset(158, 136).addBox(-4.9843F, 7.6173F, 18.5024F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        wires8.setTextureOffset(148, 140).addBox(-3.1843F, 7.6173F, 18.5024F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        wires8.setTextureOffset(148, 140).addBox(0.4134F, 7.6475F, 18.2662F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        wires8.setTextureOffset(148, 140).addBox(1.7855F, 7.5573F, 15.0213F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        wires8.setTextureOffset(158, 136).addBox(-4.0843F, 7.6173F, 18.5024F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        wires8.setTextureOffset(158, 136).addBox(-0.4866F, 7.6475F, 18.2662F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        wires8.setTextureOffset(178, 169).addBox(0.8855F, 7.5573F, 15.0213F, 1.0F, 1.0F, 1.0F, -0.2F, false);

        wires7 = new ModelRenderer(this);
        wires7.setRotationPoint(2.3F, -30.7F, 8.1F);
        all.addChild(wires7);
        setRotationAngle(wires7, -0.3491F, 0.5236F, 0.0175F);
        wires7.setTextureOffset(0, 0).addBox(-0.6984F, 7.1499F, 13.4498F, 6.0F, 1.0F, 1.0F, -0.1F, false);
        wires7.setTextureOffset(170, 173).addBox(0.3016F, 7.1499F, 13.4498F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires7.setTextureOffset(48, 32).addBox(3.3016F, 7.1499F, 13.4498F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires7.setTextureOffset(0, 0).addBox(-1.3984F, 7.1499F, 8.4498F, 1.0F, 1.0F, 6.0F, -0.1F, false);
        wires7.setTextureOffset(170, 173).addBox(-1.3984F, 7.1499F, 12.4498F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires7.setTextureOffset(170, 173).addBox(-1.3984F, 7.1499F, 9.4498F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        mirror = new ModelRenderer(this);
        mirror.setRotationPoint(-0.4F, -28.6F, 0.5F);
        all.addChild(mirror);
        setRotationAngle(mirror, -0.2618F, 2.0944F, 0.0F);
        mirror.setTextureOffset(143, 139).addBox(-1.916F, 0.1712F, 8.0863F, 4.0F, 1.0F, 6.0F, 0.0F, false);
        mirror.setTextureOffset(145, 173).addBox(-1.916F, -0.4084F, 7.931F, 4.0F, 1.0F, 6.0F, -0.2F, false);

        wires6 = new ModelRenderer(this);
        wires6.setRotationPoint(6.1F, -31.3F, 1.4F);
        all.addChild(wires6);
        setRotationAngle(wires6, -0.3491F, 1.5708F, 0.0524F);
        wires6.setTextureOffset(0, 0).addBox(0.715F, 6.241F, 15.9368F, 6.0F, 1.0F, 1.0F, -0.1F, false);
        wires6.setTextureOffset(170, 173).addBox(1.715F, 6.241F, 15.9368F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires6.setTextureOffset(153, 180).addBox(4.715F, 6.241F, 15.9368F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires6.setTextureOffset(0, 0).addBox(0.015F, 6.241F, 11.9368F, 1.0F, 1.0F, 5.0F, -0.1F, false);
        wires6.setTextureOffset(170, 173).addBox(0.015F, 6.241F, 14.9368F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires6.setTextureOffset(170, 173).addBox(0.015F, 6.241F, 11.9368F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        wires5 = new ModelRenderer(this);
        wires5.setRotationPoint(2.3F, -33.0F, 7.0F);
        all.addChild(wires5);
        setRotationAngle(wires5, -0.3491F, 1.5708F, 0.0524F);
        wires5.setTextureOffset(0, 0).addBox(0.715F, 6.241F, 15.9368F, 6.0F, 1.0F, 1.0F, -0.1F, false);
        wires5.setTextureOffset(170, 173).addBox(1.715F, 6.241F, 15.9368F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires5.setTextureOffset(48, 32).addBox(4.715F, 6.241F, 15.9368F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires5.setTextureOffset(0, 0).addBox(0.015F, 6.241F, 10.9368F, 1.0F, 1.0F, 6.0F, -0.1F, false);
        wires5.setTextureOffset(170, 173).addBox(0.015F, 6.241F, 14.9368F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires5.setTextureOffset(170, 173).addBox(0.015F, 6.241F, 11.9368F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        wires4 = new ModelRenderer(this);
        wires4.setRotationPoint(-1.7F, -35.0F, 1.4F);
        all.addChild(wires4);
        setRotationAngle(wires4, -0.3491F, 1.5708F, 0.0524F);
        wires4.setTextureOffset(0, 0).addBox(0.715F, 6.241F, 15.9368F, 6.0F, 1.0F, 1.0F, -0.1F, false);
        wires4.setTextureOffset(170, 173).addBox(1.715F, 6.241F, 15.9368F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires4.setTextureOffset(48, 32).addBox(4.715F, 6.241F, 15.9368F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires4.setTextureOffset(0, 0).addBox(0.015F, 6.241F, 10.9368F, 1.0F, 1.0F, 6.0F, -0.1F, false);
        wires4.setTextureOffset(170, 173).addBox(0.015F, 6.241F, 14.9368F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires4.setTextureOffset(170, 173).addBox(0.015F, 6.241F, 11.9368F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        buttons = new ModelRenderer(this);
        buttons.setRotationPoint(0.7F, -19.8F, 2.1F);
        all.addChild(buttons);
        setRotationAngle(buttons, -0.2269F, 1.0472F, -0.0349F);
        buttons.setTextureOffset(0, 101).addBox(2.1221F, -6.2642F, 15.7892F, 1.0F, 1.0F, 2.0F, 0.0F, false);
        buttons.setTextureOffset(5, 98).addBox(0.1221F, -6.2642F, 15.7892F, 1.0F, 1.0F, 2.0F, 0.0F, false);
        buttons.setTextureOffset(148, 126).addBox(2.7221F, -6.2642F, 15.7892F, 1.0F, 1.0F, 2.0F, 0.1F, false);
        buttons.setTextureOffset(148, 126).addBox(-0.4779F, -6.2642F, 15.7892F, 1.0F, 1.0F, 2.0F, 0.1F, false);

        wires3 = new ModelRenderer(this);
        wires3.setRotationPoint(-0.7F, -35.3F, 5.4F);
        all.addChild(wires3);
        setRotationAngle(wires3, -0.3665F, 2.7576F, 0.0524F);
        wires3.setTextureOffset(0, 0).addBox(-1.2538F, 5.265F, 18.4746F, 6.0F, 1.0F, 1.0F, -0.1F, false);
        wires3.setTextureOffset(170, 173).addBox(-0.2538F, 5.265F, 18.4746F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires3.setTextureOffset(6, 88).addBox(2.7462F, 5.265F, 18.4746F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires3.setTextureOffset(0, 0).addBox(-1.9538F, 5.265F, 13.4746F, 1.0F, 1.0F, 6.0F, -0.1F, false);
        wires3.setTextureOffset(170, 173).addBox(-1.9538F, 5.265F, 17.4746F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires3.setTextureOffset(170, 173).addBox(-1.9538F, 5.265F, 14.4746F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        wires2 = new ModelRenderer(this);
        wires2.setRotationPoint(0.3F, -34.6F, 4.4F);
        all.addChild(wires2);
        setRotationAngle(wires2, -0.3665F, 2.6704F, 0.0524F);
        wires2.setTextureOffset(0, 0).addBox(-1.012F, 5.301F, 18.3807F, 6.0F, 1.0F, 1.0F, -0.1F, false);
        wires2.setTextureOffset(10, 110).addBox(-0.012F, 5.301F, 18.3807F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires2.setTextureOffset(10, 110).addBox(2.988F, 5.301F, 18.3807F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires2.setTextureOffset(0, 0).addBox(-1.712F, 5.301F, 13.3807F, 1.0F, 1.0F, 6.0F, -0.1F, false);
        wires2.setTextureOffset(10, 110).addBox(-1.712F, 5.301F, 17.3807F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires2.setTextureOffset(13, 75).addBox(-1.712F, 5.301F, 14.3807F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        wires = new ModelRenderer(this);
        wires.setRotationPoint(0.3F, -33.2F, 3.1F);
        all.addChild(wires);
        setRotationAngle(wires, -0.3665F, -2.6704F, 0.0F);
        wires.setTextureOffset(0, 0).addBox(-3.7361F, 5.0583F, 18.1904F, 6.0F, 1.0F, 1.0F, -0.1F, false);
        wires.setTextureOffset(10, 110).addBox(-2.7361F, 5.0583F, 18.1904F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires.setTextureOffset(10, 110).addBox(0.2639F, 5.0583F, 18.1904F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires.setTextureOffset(0, 0).addBox(-4.4361F, 5.0583F, 13.1904F, 1.0F, 1.0F, 6.0F, -0.1F, false);
        wires.setTextureOffset(10, 110).addBox(-4.4361F, 5.0583F, 17.1904F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        wires.setTextureOffset(13, 75).addBox(-4.4361F, 5.0583F, 14.1904F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        bone112 = new ModelRenderer(this);
        bone112.setRotationPoint(1.4F, -32.7F, -12.2F);
        all.addChild(bone112);
        setRotationAngle(bone112, -0.2618F, 0.0F, 0.0F);
        bone112.setTextureOffset(156, 133).addBox(-3.2F, 1.3492F, 21.1647F, 1.0F, 2.0F, 1.0F, 0.0F, false);

        bone111 = new ModelRenderer(this);
        bone111.setRotationPoint(3.6F, -32.8F, -12.2F);
        all.addChild(bone111);
        setRotationAngle(bone111, -0.2618F, 0.0F, 0.0F);
        bone111.setTextureOffset(138, 142).addBox(-3.2F, 1.3492F, 20.1647F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone111.setTextureOffset(134, 167).addBox(-3.2F, 0.8145F, 20.2285F, 2.0F, 2.0F, 2.0F, -0.2F, false);

        bone106 = new ModelRenderer(this);
        bone106.setRotationPoint(13.7F, -19.0F, -10.2F);
        all.addChild(bone106);
        setRotationAngle(bone106, 0.4363F, -0.5236F, 0.0F);
        bone106.setTextureOffset(10, 110).addBox(-4.483F, -1.7618F, -3.3971F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        bone106.setTextureOffset(15, 111).addBox(-4.483F, -2.215F, -3.1858F, 2.0F, 2.0F, 4.0F, -0.2F, false);
        bone106.setTextureOffset(10, 110).addBox(-6.3187F, -4.472F, 0.8894F, 1.0F, 4.0F, 1.0F, 0.0F, false);

        land_type = new ModelRenderer(this);
        land_type.setRotationPoint(-6.0F, 0.0F, 1.45F);
        bone106.addChild(land_type);
        land_type.setTextureOffset(157, 182).addBox(-0.2589F, -6.4945F, -0.1653F, 1.0F, 3.0F, 1.0F, 0.0F, false);
        land_type.setTextureOffset(157, 182).addBox(-0.2625F, -6.382F, -1.1073F, 1.0F, 3.0F, 1.0F, 0.0F, false);
        land_type.setTextureOffset(157, 182).addBox(0.3473F, -6.6424F, -0.4825F, 1.0F, 3.0F, 1.0F, 0.0F, false);
        land_type.setTextureOffset(157, 182).addBox(-0.8187F, -6.6507F, -0.5002F, 1.0F, 3.0F, 1.0F, 0.0F, false);

        bone103 = new ModelRenderer(this);
        bone103.setRotationPoint(4.4F, -19.0F, -15.5F);
        all.addChild(bone103);
        setRotationAngle(bone103, 0.4363F, -0.0349F, 0.0524F);
        bone103.setTextureOffset(144, 171).addBox(-1.0049F, -2.2733F, -2.7182F, 2.0F, 2.0F, 6.0F, 0.0F, false);
        bone103.setTextureOffset(0, 0).addBox(-1.5049F, -2.7733F, -3.7182F, 3.0F, 3.0F, 1.0F, 0.0F, false);
        bone103.setTextureOffset(0, 0).addBox(-1.5049F, -2.7733F, 3.2818F, 3.0F, 3.0F, 1.0F, 0.0F, false);
        bone103.setTextureOffset(0, 0).addBox(-1.4278F, -2.7021F, -0.6249F, 3.0F, 3.0F, 2.0F, 0.0F, false);

        important_controls = new ModelRenderer(this);
        important_controls.setRotationPoint(0.0F, 0.0F, 0.0F);
        all.addChild(important_controls);


        fastreturn = new ModelRenderer(this);
        fastreturn.setRotationPoint(0.2F, -26.2F, 3.0F);
        important_controls.addChild(fastreturn);
        fastreturn.setTextureOffset(14, 77).addBox(-0.7038F, 9.1768F, -26.4935F, 7.0F, 4.0F, 3.0F, 0.0F, false);
        fastreturn.setTextureOffset(14, 77).addBox(2.2962F, 13.1768F, -26.3935F, 3.0F, 1.0F, 3.0F, 0.0F, false);

        lever = new ModelRenderer(this);
        lever.setRotationPoint(5.0F, 11.1F, -25.4F);
        fastreturn.addChild(lever);
        setRotationAngle(lever, 0.0F, 0.0F, -0.6981F);
        lever.setTextureOffset(123, 174).addBox(-1.2374F, -0.5149F, -0.5935F, 6.0F, 1.0F, 1.0F, 0.0F, false);
        lever.setTextureOffset(141, 140).addBox(4.1626F, -1.0149F, -1.0935F, 3.0F, 2.0F, 2.0F, -0.4F, false);

        refuel = new ModelRenderer(this);
        refuel.setRotationPoint(3.7F, -24.3F, 4.4F);
        important_controls.addChild(refuel);
        setRotationAngle(refuel, 0.384F, -1.0472F, -0.4363F);
        refuel.setTextureOffset(17, 102).addBox(-16.5571F, -2.2346F, -0.0542F, 3.0F, 1.0F, 3.0F, 0.0F, false);

        doorcontrol = new ModelRenderer(this);
        doorcontrol.setRotationPoint(-2.2F, -22.3F, 0.0F);
        important_controls.addChild(doorcontrol);
        setRotationAngle(doorcontrol, 0.384F, -1.0472F, -0.4363F);
        doorcontrol.setTextureOffset(16, 109).addBox(-18.2891F, -1.3017F, 3.777F, 2.0F, 1.0F, 2.0F, 0.0F, false);

        keyboardxyz = new ModelRenderer(this);
        keyboardxyz.setRotationPoint(-2.2F, -22.3F, 0.0F);
        important_controls.addChild(keyboardxyz);
        setRotationAngle(keyboardxyz, 0.384F, -1.0472F, -0.4363F);
        keyboardxyz.setTextureOffset(30, 189).addBox(-19.5528F, -1.5521F, -6.4794F, 4.0F, 1.0F, 10.0F, 0.0F, false);
        keyboardxyz.setTextureOffset(81, 180).addBox(-17.5528F, -1.7521F, 2.3206F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        keyboardxyz.setTextureOffset(81, 180).addBox(-17.5528F, -1.7521F, -0.0794F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        keyboardxyz.setTextureOffset(81, 180).addBox(-17.5528F, -1.7521F, -2.5794F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        keyboardxyz.setTextureOffset(81, 180).addBox(-19.2528F, -1.7521F, 2.0206F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboardxyz.setTextureOffset(81, 180).addBox(-19.2528F, -1.7521F, 0.0206F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboardxyz.setTextureOffset(81, 180).addBox(-19.2528F, -1.7521F, -1.9794F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboardxyz.setTextureOffset(81, 180).addBox(-19.2528F, -1.7521F, -3.9794F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboardxyz.setTextureOffset(81, 180).addBox(-19.2528F, -1.7521F, -5.9794F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboardxyz.setTextureOffset(74, 178).addBox(-17.5528F, -1.7521F, -6.2794F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboardxyz.setTextureOffset(81, 180).addBox(-17.5528F, -1.7521F, -5.0794F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        keyboardxyz.setTextureOffset(88, 188).addBox(-17.5528F, -1.7521F, 1.1206F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboardxyz.setTextureOffset(88, 188).addBox(-17.5528F, -1.7521F, -3.7794F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboardxyz.setTextureOffset(88, 188).addBox(-17.5528F, -1.7521F, -1.3794F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        throttle = new ModelRenderer(this);
        throttle.setRotationPoint(1.9F, -29.0F, 0.4F);
        important_controls.addChild(throttle);
        setRotationAngle(throttle, -0.2618F, 0.0F, 0.0F);
        throttle.setTextureOffset(105, 136).addBox(-1.9F, -0.7235F, 11.1022F, 2.0F, 2.0F, 6.0F, -0.1F, false);
        throttle.setTextureOffset(74, 177).addBox(-1.9F, -1.0235F, 11.1022F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        throttle.setTextureOffset(79, 167).addBox(-2.4F, -0.2235F, 11.6022F, 3.0F, 2.0F, 6.0F, 0.0F, false);
        throttle.setTextureOffset(108, 16).addBox(-2.4F, -0.7235F, 10.1022F, 3.0F, 2.0F, 1.0F, 0.0F, false);
        throttle.setTextureOffset(84, 163).addBox(-0.2F, -1.2235F, 11.1022F, 1.0F, 3.0F, 6.0F, 0.0F, false);
        throttle.setTextureOffset(110, 19).addBox(-2.6F, -1.2235F, 11.1022F, 1.0F, 2.0F, 6.0F, 0.0F, false);
        throttle.setTextureOffset(12, 104).addBox(-3.9F, 0.7765F, 11.1022F, 2.0F, 1.0F, 6.0F, 0.0F, false);
        throttle.setTextureOffset(12, 104).addBox(-3.4F, 0.3901F, 10.9987F, 2.0F, 1.0F, 6.0F, -0.1F, false);
        throttle.setTextureOffset(100, 8).addBox(-4.1F, -0.0235F, 11.1022F, 1.0F, 1.0F, 6.0F, -0.2F, false);

        lever3 = new ModelRenderer(this);
        lever3.setRotationPoint(-3.0F, 0.5518F, 14.9068F);
        throttle.addChild(lever3);
        setRotationAngle(lever3, 1.1345F, 0.0F, 0.0F);
        lever3.setTextureOffset(141, 171).addBox(-0.5F, -4.3119F, -0.715F, 1.0F, 5.0F, 1.0F, -0.1F, false);
        lever3.setTextureOffset(0, 0).addBox(-0.5F, -5.1119F, -0.715F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        Sonicport = new ModelRenderer(this);
        Sonicport.setRotationPoint(1.9F, -29.0F, 0.4F);
        important_controls.addChild(Sonicport);
        setRotationAngle(Sonicport, -0.2618F, 0.0F, 0.0F);
        Sonicport.setTextureOffset(136, 175).addBox(-3.3F, 1.2526F, 19.1389F, 1.0F, 2.0F, 3.0F, 0.0F, false);
        Sonicport.setTextureOffset(136, 175).addBox(-1.1F, 1.2526F, 19.1389F, 1.0F, 2.0F, 3.0F, 0.0F, false);
        Sonicport.setTextureOffset(136, 175).addBox(-2.7F, 1.2267F, 21.2354F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        Sonicport.setTextureOffset(136, 175).addBox(-2.7F, 1.2166F, 18.9551F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        Sonicport.setTextureOffset(136, 131).addBox(-3.2F, 1.4458F, 19.1906F, 3.0F, 2.0F, 3.0F, -0.2F, false);

        Incrementincrease = new ModelRenderer(this);
        Incrementincrease.setRotationPoint(-5.3F, -21.3F, -2.0F);
        important_controls.addChild(Incrementincrease);
        setRotationAngle(Incrementincrease, -0.2269F, 1.0297F, -0.0349F);
        Incrementincrease.setTextureOffset(132, 176).addBox(1.9462F, -7.6683F, 13.7385F, 1.0F, 1.0F, 6.0F, 0.0F, false);
        Incrementincrease.setTextureOffset(132, 176).addBox(-0.0538F, -7.6683F, 13.7385F, 1.0F, 1.0F, 6.0F, 0.0F, false);
        Incrementincrease.setTextureOffset(148, 126).addBox(2.5462F, -7.6683F, 13.7385F, 1.0F, 1.0F, 6.0F, 0.1F, false);
        Incrementincrease.setTextureOffset(148, 126).addBox(0.9462F, -7.6683F, 13.7385F, 1.0F, 1.0F, 6.0F, 0.1F, false);
        Incrementincrease.setTextureOffset(148, 126).addBox(-0.6538F, -7.6683F, 13.7385F, 1.0F, 1.0F, 6.0F, 0.1F, false);
        Incrementincrease.setTextureOffset(132, 176).addBox(0.9462F, -7.6683F, 19.7385F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        lever2 = new ModelRenderer(this);
        lever2.setRotationPoint(1.475F, -7.2F, 20.9F);
        Incrementincrease.addChild(lever2);
        lever2.setTextureOffset(136, 10).addBox(-0.498F, -1.4443F, -0.385F, 1.0F, 2.0F, 1.0F, -0.2F, false);
        lever2.setTextureOffset(136, 23).addBox(-0.498F, -1.4443F, -0.385F, 1.0F, 1.0F, 1.0F, -0.1F, false);

        monitor = new ModelRenderer(this);
        monitor.setRotationPoint(-0.1F, -28.0F, 0.4F);
        important_controls.addChild(monitor);
        setRotationAngle(monitor, 0.0F, -0.5236F, 0.0F);
        monitor.setTextureOffset(132, 176).addBox(-4.6261F, 3.0F, 12.7797F, 9.0F, 1.0F, 2.0F, 0.0F, false);
        monitor.setTextureOffset(132, 176).addBox(-4.6261F, 4.0F, 12.7797F, 9.0F, 1.0F, 3.0F, 0.0F, false);
        monitor.setTextureOffset(132, 176).addBox(-4.6261F, -4.0F, 11.7797F, 9.0F, 1.0F, 3.0F, 0.0F, false);
        monitor.setTextureOffset(184, 115).addBox(-3.8065F, -3.7F, 14.9993F, 2.0F, 2.0F, 0.0F, 0.0F, false);
        monitor.setTextureOffset(132, 176).addBox(-3.6261F, -3.0F, 12.7797F, 7.0F, 6.0F, 1.0F, 0.0F, false);
        monitor.setTextureOffset(138, 138).addBox(-1.0261F, -1.0F, 1.7797F, 2.0F, 2.0F, 11.0F, 0.0F, false);
        monitor.setTextureOffset(138, 138).addBox(-1.0261F, 1.5F, 2.7797F, 2.0F, 2.0F, 10.0F, 0.0F, false);
        monitor.setTextureOffset(132, 176).addBox(-4.6261F, -3.0F, 12.7797F, 1.0F, 6.0F, 2.0F, 0.0F, false);
        monitor.setTextureOffset(132, 176).addBox(3.3739F, -3.0F, 12.7797F, 1.0F, 6.0F, 2.0F, 0.0F, false);

        handbreak = new ModelRenderer(this);
        handbreak.setRotationPoint(-0.1F, -29.7F, 0.8F);
        important_controls.addChild(handbreak);
        setRotationAngle(handbreak, -0.3491F, 2.0944F, 0.0F);
        handbreak.setTextureOffset(145, 173).addBox(-1.4519F, -0.5427F, 15.4909F, 4.0F, 1.0F, 6.0F, 0.0F, false);
        handbreak.setTextureOffset(144, 129).addBox(-0.5349F, -1.546F, 15.5001F, 3.0F, 1.0F, 6.0F, 0.0F, false);
        handbreak.setTextureOffset(115, 140).addBox(-1.1519F, -2.0427F, 15.4909F, 2.0F, 2.0F, 6.0F, -0.2F, false);
        handbreak.setTextureOffset(115, 140).addBox(-1.9519F, -1.5427F, 16.4909F, 2.0F, 2.0F, 4.0F, 0.0F, false);

        lever4 = new ModelRenderer(this);
        lever4.setRotationPoint(2.05F, -0.3F, 20.2F);
        handbreak.addChild(lever4);
        setRotationAngle(lever4, 0.7854F, 0.0F, 0.0F);
        lever4.setTextureOffset(144, 129).addBox(-0.6019F, -4.3604F, -0.5778F, 1.0F, 5.0F, 1.0F, -0.1F, false);
        lever4.setTextureOffset(13, 106).addBox(-2.6019F, -5.1604F, -0.5778F, 3.0F, 1.0F, 1.0F, 0.0F, false);

        lever5 = new ModelRenderer(this);
        lever5.setRotationPoint(2.05F, -0.4482F, 17.025F);
        handbreak.addChild(lever5);
        setRotationAngle(lever5, 0.7854F, 0.0F, 0.0F);
        lever5.setTextureOffset(144, 129).addBox(-0.5019F, -4.3518F, -0.4028F, 1.0F, 5.0F, 1.0F, -0.1F, false);
        lever5.setTextureOffset(13, 106).addBox(-1.5019F, -5.2122F, -0.4028F, 2.0F, 1.0F, 1.0F, 0.0F, false);

        wheel = new ModelRenderer(this);
        wheel.setRotationPoint(-4.4F, -0.4482F, 17.1068F);
        handbreak.addChild(wheel);
        wheel.setTextureOffset(83, 171).addBox(1.6481F, -2.1265F, 1.9841F, 1.0F, 3.0F, 1.0F, -0.2F, false);
        wheel.setTextureOffset(83, 171).addBox(1.6481F, -2.1265F, -0.0159F, 1.0F, 3.0F, 1.0F, -0.2F, false);
        wheel.setTextureOffset(83, 171).addBox(1.8481F, -1.5265F, 0.3841F, 1.0F, 2.0F, 2.0F, -0.2F, false);
        wheel.setTextureOffset(83, 171).addBox(1.6481F, -2.1265F, -0.0159F, 1.0F, 1.0F, 3.0F, -0.2F, false);
        wheel.setTextureOffset(83, 171).addBox(1.6481F, -0.1265F, -0.0159F, 1.0F, 1.0F, 3.0F, -0.2F, false);

        randomiser_and_facingcontrol = new ModelRenderer(this);
        randomiser_and_facingcontrol.setRotationPoint(-7.1F, -29.0F, 3.4F);
        important_controls.addChild(randomiser_and_facingcontrol);
        setRotationAngle(randomiser_and_facingcontrol, -0.3491F, 0.5236F, 0.0F);
        randomiser_and_facingcontrol.setTextureOffset(5, 108).addBox(3.166F, 1.4211F, 15.5757F, 3.0F, 2.0F, 3.0F, 0.0F, false);
        randomiser_and_facingcontrol.setTextureOffset(108, 68).addBox(6.1892F, 2.1355F, 10.689F, 3.0F, 2.0F, 3.0F, 0.0F, false);
        randomiser_and_facingcontrol.setTextureOffset(109, 81).addBox(9.2595F, 2.6188F, 15.2089F, 3.0F, 2.0F, 3.0F, 0.0F, false);
        randomiser_and_facingcontrol.setTextureOffset(5, 108).addBox(3.166F, 1.0211F, 15.5757F, 3.0F, 2.0F, 3.0F, -0.1F, false);

        throttle3 = new ModelRenderer(this);
        throttle3.setRotationPoint(4.0F, 0.0F, 4.0F);
        randomiser_and_facingcontrol.addChild(throttle3);
        throttle3.setTextureOffset(5, 108).addBox(5.0883F, 2.0547F, 15.8814F, 3.0F, 2.0F, 3.0F, 0.0F, false);
        throttle3.setTextureOffset(5, 108).addBox(5.0883F, 1.6547F, 15.8814F, 3.0F, 2.0F, 3.0F, -0.1F, false);

        facing_control = new ModelRenderer(this);
        facing_control.setRotationPoint(7.6F, 1.2877F, 12.1F);
        randomiser_and_facingcontrol.addChild(facing_control);
        setRotationAngle(facing_control, 0.0F, 3.1416F, 0.0F);
        facing_control.setTextureOffset(9, 148).addBox(-1.5866F, -1.7285F, -1.547F, 3.0F, 3.0F, 3.0F, -0.4F, false);
        facing_control.setTextureOffset(186, 86).addBox(-1.5866F, -2.0285F, -1.547F, 3.0F, 3.0F, 3.0F, -0.5F, false);
        facing_control.setTextureOffset(178, 91).addBox(-1.5866F, -1.4285F, -1.297F, 3.0F, 3.0F, 3.0F, -0.5F, false);
        facing_control.setTextureOffset(186, 86).addBox(-1.8366F, -1.7285F, -1.547F, 3.0F, 3.0F, 3.0F, -0.5F, false);
        facing_control.setTextureOffset(186, 86).addBox(-1.5866F, -1.7285F, -1.797F, 3.0F, 3.0F, 3.0F, -0.5F, false);
        facing_control.setTextureOffset(178, 91).addBox(-1.5866F, -1.7285F, -1.547F, 3.0F, 3.0F, 3.0F, -0.5F, false);
        facing_control.setTextureOffset(178, 91).addBox(-1.3366F, -1.7285F, -1.547F, 3.0F, 3.0F, 3.0F, -0.5F, false);

        randomiser = new ModelRenderer(this);
        randomiser.setRotationPoint(10.725F, 1.5F, 16.85F);
        randomiser_and_facingcontrol.addChild(randomiser);
        randomiser.setTextureOffset(9, 144).addBox(-1.4315F, -1.7356F, -1.453F, 3.0F, 3.0F, 3.0F, -0.4F, false);
        randomiser.setTextureOffset(181, 97).addBox(-1.4315F, -2.0356F, -1.453F, 3.0F, 3.0F, 3.0F, -0.5F, false);
        randomiser.setTextureOffset(181, 97).addBox(-1.4315F, -1.4356F, -1.453F, 3.0F, 3.0F, 3.0F, -0.5F, false);
        randomiser.setTextureOffset(181, 97).addBox(-1.1315F, -1.7356F, -1.453F, 3.0F, 3.0F, 3.0F, -0.5F, false);
        randomiser.setTextureOffset(181, 97).addBox(-1.4315F, -1.7356F, -1.203F, 3.0F, 3.0F, 3.0F, -0.5F, false);
        randomiser.setTextureOffset(181, 97).addBox(-1.4315F, -1.7356F, -1.703F, 3.0F, 3.0F, 3.0F, -0.5F, false);
        randomiser.setTextureOffset(181, 97).addBox(-1.7315F, -1.7356F, -1.453F, 3.0F, 3.0F, 3.0F, -0.5F, false);

        Comuni_Phone = new ModelRenderer(this);
        Comuni_Phone.setRotationPoint(5.9F, -30.0F, -1.1F);
        important_controls.addChild(Comuni_Phone);
        setRotationAngle(Comuni_Phone, 0.0F, 1.5708F, 0.3491F);
        Comuni_Phone.setTextureOffset(4, 145).addBox(-1.6F, 4.9314F, 8.3125F, 5.0F, 1.0F, 6.0F, 0.0F, false);
        Comuni_Phone.setTextureOffset(178, 190).addBox(-6.1F, 5.5629F, 13.6707F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        Comuni_Phone.setTextureOffset(108, 161).addBox(-5.1F, 5.3335F, 9.3305F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        Comuni_Phone.setTextureOffset(108, 161).addBox(-5.1F, 4.8892F, 9.3816F, 3.0F, 1.0F, 3.0F, -0.2F, false);
        Comuni_Phone.setTextureOffset(108, 161).addBox(3.1F, 5.5287F, 13.7647F, 3.0F, 1.0F, 3.0F, -0.2F, false);
        Comuni_Phone.setTextureOffset(108, 161).addBox(-5.1F, 4.4449F, 9.4327F, 3.0F, 1.0F, 3.0F, -0.4F, false);
        Comuni_Phone.setTextureOffset(108, 161).addBox(3.1F, 5.0844F, 13.8158F, 3.0F, 1.0F, 3.0F, -0.4F, false);
        Comuni_Phone.setTextureOffset(101, 19).addBox(1.4F, 3.5314F, 8.3125F, 2.0F, 1.0F, 6.0F, -0.2F, false);
        Comuni_Phone.setTextureOffset(114, 11).addBox(1.4F, 4.1314F, 12.3125F, 2.0F, 1.0F, 2.0F, -0.2F, false);
        Comuni_Phone.setTextureOffset(148, 130).addBox(1.9F, 4.1314F, 13.8125F, 1.0F, 1.0F, 2.0F, -0.2F, false);
        Comuni_Phone.setTextureOffset(148, 130).addBox(0.9F, 4.7314F, 14.8125F, 2.0F, 1.0F, 1.0F, -0.2F, false);
        Comuni_Phone.setTextureOffset(148, 130).addBox(-0.7F, 4.7314F, 15.0125F, 2.0F, 1.0F, 1.0F, -0.2F, false);
        Comuni_Phone.setTextureOffset(148, 130).addBox(-1.3F, 4.8314F, 14.0125F, 1.0F, 1.0F, 2.0F, -0.2F, false);
        Comuni_Phone.setTextureOffset(114, 11).addBox(1.4F, 4.1314F, 8.3125F, 2.0F, 1.0F, 2.0F, -0.2F, false);
        Comuni_Phone.setTextureOffset(158, 136).addBox(-6.0F, 5.1629F, 13.6707F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        Comuni_Phone.setTextureOffset(160, 100).addBox(-4.5F, 4.9936F, 8.2882F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        Comuni_Phone.setTextureOffset(72, 178).addBox(-0.7F, 4.5314F, 11.3125F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        Comuni_Phone.setTextureOffset(158, 136).addBox(-5.1F, 5.1629F, 13.6707F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        Comuni_Phone.setTextureOffset(72, 178).addBox(-1.6F, 4.5314F, 12.3125F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        Comuni_Phone.setTextureOffset(153, 97).addBox(-6.0F, 5.1629F, 14.6707F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        Comuni_Phone.setTextureOffset(72, 178).addBox(0.2F, 4.5314F, 12.3125F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        Comuni_Phone.setTextureOffset(158, 136).addBox(-4.2F, 5.1629F, 14.6707F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        Comuni_Phone.setTextureOffset(72, 178).addBox(-0.7F, 4.5314F, 13.3125F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        Comuni_Phone.setTextureOffset(158, 136).addBox(-5.1F, 5.1629F, 15.6707F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        Comuni_Phone.setTextureOffset(158, 136).addBox(-4.2F, 5.1629F, 15.6707F, 1.0F, 1.0F, 1.0F, -0.2F, false);

        telepathic_circuit = new ModelRenderer(this);
        telepathic_circuit.setRotationPoint(3.9F, -20.4F, 4.3F);
        important_controls.addChild(telepathic_circuit);
        setRotationAngle(telepathic_circuit, 0.0F, -1.5708F, 0.0F);


        crystal = new ModelRenderer(this);
        crystal.setRotationPoint(-4.0F, 0.5518F, 20.9068F);
        telepathic_circuit.addChild(crystal);
        setRotationAngle(crystal, -0.4363F, 0.0F, 0.0F);
        crystal.setTextureOffset(105, 136).addBox(-1.0F, -0.2253F, -2.0453F, 2.0F, 1.0F, 1.0F, -0.1F, false);
        crystal.setTextureOffset(140, 172).addBox(-1.0F, -0.2253F, 0.9547F, 2.0F, 1.0F, 1.0F, -0.1F, false);
        crystal.setTextureOffset(105, 136).addBox(0.6F, -0.316F, 0.5124F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        crystal.setTextureOffset(105, 136).addBox(0.6F, -0.316F, -1.4876F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        crystal.setTextureOffset(140, 172).addBox(-1.5F, -0.316F, 0.5124F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        crystal.setTextureOffset(105, 136).addBox(-1.5F, -0.316F, -1.4876F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        crystal.setTextureOffset(105, 136).addBox(1.0F, -0.2253F, -1.0453F, 1.0F, 1.0F, 2.0F, -0.1F, false);
        crystal.setTextureOffset(140, 172).addBox(-2.0F, -0.2253F, -1.0453F, 1.0F, 1.0F, 2.0F, -0.1F, false);

        lever8 = new ModelRenderer(this);
        lever8.setRotationPoint(1.0F, -1.4482F, 14.9068F);
        telepathic_circuit.addChild(lever8);
        setRotationAngle(lever8, -0.4363F, 0.0F, 0.0F);
        lever8.setTextureOffset(105, 136).addBox(-2.0F, -0.979F, 6.1202F, 1.0F, 1.0F, 1.0F, 0.1F, false);
        lever8.setTextureOffset(153, 180).addBox(-5.8F, -1.1744F, -1.0326F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        lever8.setTextureOffset(153, 180).addBox(-5.8F, -1.1988F, -1.9267F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        lever8.setTextureOffset(105, 136).addBox(0.0F, -0.5258F, 6.3315F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        lever8.setTextureOffset(105, 136).addBox(-0.9F, -0.5563F, 5.2139F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        lever8.setTextureOffset(105, 136).addBox(1.0F, -0.5258F, 6.3315F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        lever8.setTextureOffset(105, 136).addBox(0.1F, -0.5563F, 5.2139F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        lever8.setTextureOffset(105, 136).addBox(-2.0F, -1.1194F, 0.9791F, 1.0F, 1.0F, 1.0F, 0.1F, false);
        lever8.setTextureOffset(153, 180).addBox(-8.0F, -0.7569F, 1.1482F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        lever8.setTextureOffset(153, 180).addBox(-8.0F, -0.8654F, 2.0906F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        lever8.setTextureOffset(153, 180).addBox(-10.0F, -0.8654F, 2.0906F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        lever8.setTextureOffset(153, 180).addBox(-6.8F, -0.818F, -3.0871F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        lever8.setTextureOffset(153, 180).addBox(-4.8F, -1.1744F, -1.0326F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        lever8.setTextureOffset(153, 180).addBox(-4.8F, -1.1988F, -1.9267F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        lever8.setTextureOffset(153, 180).addBox(-4.8F, -1.1387F, -3.002F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        lever8.setTextureOffset(153, 180).addBox(-6.8F, -1.1744F, -1.0326F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        lever8.setTextureOffset(153, 180).addBox(-6.8F, -1.1988F, -1.9267F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        lever8.setTextureOffset(153, 180).addBox(-6.8F, -1.1387F, -3.002F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        lever8.setTextureOffset(153, 180).addBox(-5.8F, -1.1387F, -3.002F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        lever8.setTextureOffset(153, 180).addBox(-8.0F, -0.6174F, 2.9786F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        lever8.setTextureOffset(153, 180).addBox(-10.0F, -0.7569F, 1.1482F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        lever8.setTextureOffset(153, 180).addBox(-10.0F, -0.6174F, 2.9786F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        lever8.setTextureOffset(141, 138).addBox(-12.8F, -0.4183F, 2.0784F, 9.0F, 1.0F, 1.0F, -0.1F, false);
        lever8.setTextureOffset(141, 138).addBox(-5.8F, -0.6484F, -4.7943F, 1.0F, 1.0F, 6.0F, -0.1F, false);
        lever8.setTextureOffset(141, 138).addBox(-5.1F, -0.7344F, -6.5789F, 1.0F, 1.0F, 3.0F, -0.1F, false);

        nope = new ModelRenderer(this);
        nope.setRotationPoint(-4.0124F, -1.8123F, 17.5686F);
        telepathic_circuit.addChild(nope);
        setRotationAngle(nope, 0.4363F, 3.1416F, 0.0F);
        nope.setTextureOffset(155, 184).addBox(-1.5F, -1.8577F, -1.4094F, 3.0F, 3.0F, 3.0F, -0.5F, false);
        nope.setTextureOffset(155, 184).addBox(-1.5F, -1.2577F, -1.4094F, 3.0F, 3.0F, 3.0F, -0.5F, false);
        nope.setTextureOffset(155, 184).addBox(-1.2F, -1.5577F, -1.4094F, 3.0F, 3.0F, 3.0F, -0.5F, false);
        nope.setTextureOffset(155, 184).addBox(-1.5F, -1.5577F, -1.1094F, 3.0F, 3.0F, 3.0F, -0.5F, false);
        nope.setTextureOffset(155, 184).addBox(-1.5F, -1.5577F, -1.7094F, 3.0F, 3.0F, 3.0F, -0.5F, false);
        nope.setTextureOffset(155, 184).addBox(-1.8F, -1.5577F, -1.4094F, 3.0F, 3.0F, 3.0F, -0.5F, false);

        keyboard_behind_monitor = new ModelRenderer(this);
        keyboard_behind_monitor.setRotationPoint(0.9F, -26.6F, 0.5F);
        all.addChild(keyboard_behind_monitor);
        setRotationAngle(keyboard_behind_monitor, -0.6109F, 0.9599F, -0.6981F);
        keyboard_behind_monitor.setTextureOffset(30, 189).addBox(-14.4986F, 0.0036F, -6.3213F, 4.0F, 1.0F, 10.0F, 0.0F, false);
        keyboard_behind_monitor.setTextureOffset(30, 189).addBox(-14.4986F, 0.0036F, -6.3213F, 4.0F, 1.0F, 10.0F, 0.0F, false);
        keyboard_behind_monitor.setTextureOffset(81, 180).addBox(-12.4986F, -0.1964F, -4.9213F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_behind_monitor.setTextureOffset(81, 180).addBox(-12.4986F, -0.1964F, -4.9213F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_behind_monitor.setTextureOffset(88, 188).addBox(-12.4986F, -0.1964F, 1.2787F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_behind_monitor.setTextureOffset(88, 188).addBox(-12.4986F, -0.1964F, 1.2787F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_behind_monitor.setTextureOffset(81, 180).addBox(-12.4986F, -0.1964F, 0.0787F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_behind_monitor.setTextureOffset(81, 180).addBox(-12.4986F, -0.1964F, 0.0787F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_behind_monitor.setTextureOffset(88, 188).addBox(-12.4986F, -0.1964F, -3.6213F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_behind_monitor.setTextureOffset(88, 188).addBox(-12.4986F, -0.1964F, -3.6213F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_behind_monitor.setTextureOffset(81, 180).addBox(-12.4986F, -0.1964F, -2.4213F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_behind_monitor.setTextureOffset(81, 180).addBox(-12.4986F, -0.1964F, -2.4213F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_behind_monitor.setTextureOffset(88, 188).addBox(-12.4986F, -0.1964F, -1.2213F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_behind_monitor.setTextureOffset(88, 188).addBox(-12.4986F, -0.1964F, -1.2213F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        keyboard_by_monitor = new ModelRenderer(this);
        keyboard_by_monitor.setRotationPoint(-2.2F, -25.3F, 6.7F);
        all.addChild(keyboard_by_monitor);
        setRotationAngle(keyboard_by_monitor, -0.6109F, 0.9425F, -0.6981F);
        keyboard_by_monitor.setTextureOffset(30, 189).addBox(-14.5034F, 0.6849F, -6.4929F, 4.0F, 2.0F, 10.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(30, 189).addBox(-14.5034F, 0.6849F, -6.4929F, 4.0F, 2.0F, 10.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(81, 180).addBox(-12.5034F, 0.4849F, 2.3071F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(81, 180).addBox(-12.5034F, 0.4849F, 2.3071F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(81, 180).addBox(-14.2034F, 0.4849F, 2.0071F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(81, 180).addBox(-14.2034F, 0.4849F, 2.0071F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(81, 180).addBox(-14.2034F, 0.4849F, 0.0071F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(81, 180).addBox(-14.2034F, 0.4849F, 0.0071F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(81, 180).addBox(-14.2034F, 0.4849F, -1.9929F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(81, 180).addBox(-14.2034F, 0.4849F, -1.9929F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(81, 180).addBox(-14.2034F, 0.4849F, -3.9929F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(81, 180).addBox(-14.2034F, 0.4849F, -3.9929F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(81, 180).addBox(-14.2034F, 0.4849F, -5.9929F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(81, 180).addBox(-14.2034F, 0.4849F, -5.9929F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(81, 180).addBox(-12.5034F, 0.4849F, -5.0929F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(81, 180).addBox(-12.5034F, 0.4849F, -5.0929F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(144, 182).addBox(-9.9174F, 1.399F, -4.6268F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(144, 182).addBox(-9.9174F, 1.399F, -4.6268F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(81, 180).addBox(-12.5034F, 0.4849F, -0.0929F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(81, 180).addBox(-12.5034F, 0.4849F, -0.0929F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(144, 182).addBox(-9.9174F, 1.399F, 0.3732F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(144, 182).addBox(-9.9174F, 1.399F, 0.3732F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(81, 180).addBox(-12.5034F, 0.4849F, -2.5929F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor.setTextureOffset(81, 180).addBox(-12.5034F, 0.4849F, -2.5929F, 2.0F, 1.0F, 1.0F, 0.0F, false);

        bone100 = new ModelRenderer(this);
        bone100.setRotationPoint(0.2F, -26.2F, 3.0F);
        all.addChild(bone100);
        setRotationAngle(bone100, 0.1222F, -0.4712F, -0.3491F);


        bone101 = new ModelRenderer(this);
        bone101.setRotationPoint(3.084F, -4.4256F, 3.9995F);
        bone100.addChild(bone101);
        setRotationAngle(bone101, 0.4363F, -1.0472F, -0.1745F);
        bone101.setTextureOffset(141, 185).addBox(-17.8455F, 1.9164F, -1.2631F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        bone101.setTextureOffset(141, 185).addBox(-18.8177F, 1.7146F, -1.7493F, 1.0F, 1.0F, 3.0F, 0.0F, false);
        bone101.setTextureOffset(140, 176).addBox(-24.513F, 2.0962F, -1.1586F, 4.0F, 1.0F, 2.0F, 0.0F, false);
        bone101.setTextureOffset(140, 176).addBox(-24.5416F, 1.0896F, -2.1651F, 4.0F, 2.0F, 1.0F, 0.0F, false);
        bone101.setTextureOffset(140, 176).addBox(-24.4952F, 1.0699F, 0.8345F, 4.0F, 2.0F, 1.0F, 0.0F, false);

        bone102 = new ModelRenderer(this);
        bone102.setRotationPoint(-1.2583F, -4.3874F, -5.0836F);
        bone100.addChild(bone102);
        setRotationAngle(bone102, 0.6109F, -0.9948F, -0.3491F);
        bone102.setTextureOffset(74, 190).addBox(-17.9423F, 2.1886F, -0.9521F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        bone102.setTextureOffset(74, 190).addBox(-18.9146F, 1.9868F, -1.4383F, 1.0F, 1.0F, 3.0F, 0.0F, false);

        bone99 = new ModelRenderer(this);
        bone99.setRotationPoint(2.2F, -31.8F, 5.3F);
        all.addChild(bone99);
        setRotationAngle(bone99, 0.1222F, -0.5061F, -0.2618F);
        bone99.setTextureOffset(143, 179).addBox(-23.6375F, 2.6065F, -4.0702F, 8.0F, 2.0F, 2.0F, 0.0F, false);
        bone99.setTextureOffset(0, 0).addBox(-24.4597F, 2.3356F, -4.5597F, 1.0F, 3.0F, 3.0F, 0.0F, false);
        bone99.setTextureOffset(147, 174).addBox(-27.4263F, 3.6628F, -5.098F, 2.0F, 3.0F, 3.0F, 0.0F, false);
        bone99.setTextureOffset(146, 177).addBox(-27.1999F, 2.6888F, -5.1048F, 2.0F, 3.0F, 3.0F, -0.5F, false);
        bone99.setTextureOffset(0, 0).addBox(-17.4421F, 2.2219F, -4.5422F, 1.0F, 3.0F, 3.0F, 0.0F, false);

        bone92 = new ModelRenderer(this);
        bone92.setRotationPoint(5.2F, -31.2F, 6.0F);
        all.addChild(bone92);
        setRotationAngle(bone92, 0.0873F, -0.5236F, -0.1745F);
        bone92.setTextureOffset(0, 0).addBox(-16.4147F, 2.7866F, -3.6388F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        bone92.setTextureOffset(126, 192).addBox(-20.4147F, 2.7866F, -4.6388F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        control6 = new ModelRenderer(this);
        control6.setRotationPoint(0.3F, -24.3F, 2.4F);
        all.addChild(control6);
        setRotationAngle(control6, 0.384F, -1.0472F, -0.4363F);
        control6.setTextureOffset(192, 174).addBox(-15.9698F, -0.7775F, 1.6328F, 2.0F, 1.0F, 2.0F, 0.0F, false);

        control4 = new ModelRenderer(this);
        control4.setRotationPoint(8.0F, -25.0F, -6.4F);
        all.addChild(control4);
        setRotationAngle(control4, 0.4363F, -1.0472F, -0.5236F);
        control4.setTextureOffset(153, 177).addBox(-17.7883F, 1.215F, 3.8865F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        control4.setTextureOffset(140, 140).addBox(-14.4072F, 0.5995F, 5.4355F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        control4.setTextureOffset(140, 140).addBox(-5.9111F, -0.9408F, 9.7778F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        control4.setTextureOffset(140, 140).addBox(-11.3542F, -0.1671F, 13.199F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        control4.setTextureOffset(81, 178).addBox(-14.4206F, 0.7391F, 15.3264F, 2.0F, 1.0F, 1.0F, 0.0F, false);

        control2 = new ModelRenderer(this);
        control2.setRotationPoint(0.7F, -24.0F, 3.4F);
        all.addChild(control2);
        setRotationAngle(control2, 0.384F, -1.0472F, -0.4363F);
        control2.setTextureOffset(148, 174).addBox(-19.5528F, -1.5521F, -4.4794F, 2.0F, 1.0F, 8.0F, 0.0F, false);
        control2.setTextureOffset(149, 183).addBox(-19.2528F, -1.7521F, 2.0206F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        control2.setTextureOffset(149, 183).addBox(-19.2528F, -1.7521F, 0.0206F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        control2.setTextureOffset(149, 183).addBox(-19.2528F, -1.7521F, -1.9794F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        control2.setTextureOffset(149, 183).addBox(-19.2528F, -1.7521F, -3.9794F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        
        x = new LightModelRenderer(this);
		x.setRotationPoint(0.0F, 0.0F, 0.0F);
		control2.addChild(x);
		x.setTextureOffset(149, 183).addBox(-19.2528F, -1.7521F, 0.0206F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		y = new LightModelRenderer(this);
		y.setRotationPoint(0.0F, 0.0F, 0.0F);
		control2.addChild(y);
		y.setTextureOffset(149, 183).addBox(-19.2528F, -1.7521F, -1.9794F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		z = new LightModelRenderer(this);
		z.setRotationPoint(0.0F, 0.0F, 0.0F);
		control2.addChild(z);
		z.setTextureOffset(149, 183).addBox(-19.2528F, -1.7521F, -3.9794F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        
        Shell = new ModelRenderer(this);
        Shell.setRotationPoint(-1.0F, 2.0F, -13.0F);
        all.addChild(Shell);


        Shellone = new ModelRenderer(this);
        Shellone.setRotationPoint(1.2F, -28.2F, 16.0F);
        Shell.addChild(Shellone);


        bone = new ModelRenderer(this);
        bone.setRotationPoint(-0.5F, 31.0F, -12.0F);
        Shellone.addChild(bone);
        bone.setTextureOffset(8, 53).addBox(-1.9F, -7.8284F, -5.8284F, 5.0F, 3.0F, 2.0F, 0.0F, false);

        bone2 = new ModelRenderer(this);
        bone2.setRotationPoint(0.5F, 26.0F, -10.0F);
        Shellone.addChild(bone2);
        setRotationAngle(bone2, 0.7854F, 0.0F, 0.0F);
        bone2.setTextureOffset(9, 71).addBox(-2.9F, -18.1213F, -4.1213F, 5.0F, 12.0F, 2.0F, 0.0F, false);

        bone3 = new ModelRenderer(this);
        bone3.setRotationPoint(0.0F, -16.0F, -2.0F);
        bone2.addChild(bone3);
        setRotationAngle(bone3, -0.4363F, 0.0F, 0.0F);
        bone3.setTextureOffset(9, 71).addBox(-2.9F, -5.0261F, -2.8191F, 5.0F, 4.0F, 2.0F, 0.0F, false);

        bone4 = new ModelRenderer(this);
        bone4.setRotationPoint(0.0F, -4.0F, 0.0F);
        bone3.addChild(bone4);
        setRotationAngle(bone4, -0.4363F, 0.0F, 0.0F);
        bone4.setTextureOffset(9, 71).addBox(-2.9F, -3.7385F, -2.9886F, 5.0F, 4.0F, 2.0F, 0.0F, false);

        bone5 = new ModelRenderer(this);
        bone5.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone4.addChild(bone5);
        setRotationAngle(bone5, -0.4363F, 0.0F, 0.0F);
        bone5.setTextureOffset(9, 71).addBox(-2.9F, -6.1252F, -4.2885F, 5.0F, 4.0F, 2.0F, 0.0F, false);

        bone7 = new ModelRenderer(this);
        bone7.setRotationPoint(-0.5F, 31.0F, -12.0F);
        Shellone.addChild(bone7);
        setRotationAngle(bone7, 0.1745F, 0.0F, 0.0F);
        bone7.setTextureOffset(9, 71).addBox(-1.9F, -29.4869F, 0.843F, 5.0F, 2.0F, 9.0F, 0.0F, false);

        bone6 = new ModelRenderer(this);
        bone6.setRotationPoint(-0.5F, 31.0F, -12.0F);
        Shellone.addChild(bone6);
        setRotationAngle(bone6, 0.3491F, 0.0F, 0.0F);
        bone6.setTextureOffset(9, 71).addBox(-1.9F, -28.8926F, -3.0494F, 5.0F, 2.0F, 9.0F, 0.0F, false);

        Shellone2 = new ModelRenderer(this);
        Shellone2.setRotationPoint(1.2F, -28.2F, 16.0F);
        Shell.addChild(Shellone2);
        setRotationAngle(Shellone2, 0.0F, -1.0472F, 0.0F);


        bone8 = new ModelRenderer(this);
        bone8.setRotationPoint(-0.5F, 31.0F, -12.0F);
        Shellone2.addChild(bone8);
        bone8.setTextureOffset(8, 53).addBox(-4.5481F, -7.8284F, -4.415F, 5.0F, 3.0F, 2.0F, 0.0F, false);

        bone9 = new ModelRenderer(this);
        bone9.setRotationPoint(0.5F, 26.0F, -10.0F);
        Shellone2.addChild(bone9);
        setRotationAngle(bone9, 0.7854F, 0.0F, 0.0F);
        bone9.setTextureOffset(9, 71).addBox(-5.5481F, -17.1219F, -3.1219F, 5.0F, 12.0F, 2.0F, 0.0F, false);

        bone10 = new ModelRenderer(this);
        bone10.setRotationPoint(0.0F, -16.0F, -2.0F);
        bone9.addChild(bone10);
        setRotationAngle(bone10, -0.4363F, 0.0F, 0.0F);
        bone10.setTextureOffset(9, 71).addBox(-5.5481F, -4.5427F, -1.4909F, 5.0F, 4.0F, 2.0F, 0.0F, false);

        bone11 = new ModelRenderer(this);
        bone11.setRotationPoint(0.0F, -4.0F, 0.0F);
        bone10.addChild(bone11);
        setRotationAngle(bone11, -0.4363F, 0.0F, 0.0F);
        bone11.setTextureOffset(9, 71).addBox(-5.5481F, -3.8617F, -1.5806F, 5.0F, 4.0F, 2.0F, 0.0F, false);

        bone12 = new ModelRenderer(this);
        bone12.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone11.addChild(bone12);
        setRotationAngle(bone12, -0.4363F, 0.0F, 0.0F);
        bone12.setTextureOffset(9, 71).addBox(-5.5481F, -6.8319F, -3.0645F, 5.0F, 4.0F, 2.0F, 0.0F, false);

        bone13 = new ModelRenderer(this);
        bone13.setRotationPoint(-0.5F, 31.0F, -12.0F);
        Shellone2.addChild(bone13);
        setRotationAngle(bone13, 0.1745F, 0.0F, 0.0F);
        bone13.setTextureOffset(9, 71).addBox(-4.5481F, -29.2415F, 2.2349F, 5.0F, 2.0F, 9.0F, 0.0F, false);

        bone14 = new ModelRenderer(this);
        bone14.setRotationPoint(-0.5F, 31.0F, -12.0F);
        Shellone2.addChild(bone14);
        setRotationAngle(bone14, 0.3491F, 0.0F, 0.0F);
        bone14.setTextureOffset(9, 71).addBox(-4.5481F, -28.4091F, -1.7213F, 5.0F, 2.0F, 9.0F, 0.0F, false);

        Shellone3 = new ModelRenderer(this);
        Shellone3.setRotationPoint(1.2F, -28.2F, 16.0F);
        Shell.addChild(Shellone3);
        setRotationAngle(Shellone3, 0.0F, -2.0944F, 0.0F);


        bone15 = new ModelRenderer(this);
        bone15.setRotationPoint(-0.5F, 31.0F, -12.0F);
        Shellone3.addChild(bone15);
        bone15.setTextureOffset(8, 53).addBox(-4.6481F, -7.8284F, -1.415F, 5.0F, 3.0F, 2.0F, 0.0F, false);

        bone16 = new ModelRenderer(this);
        bone16.setRotationPoint(0.5F, 26.0F, -10.0F);
        Shellone3.addChild(bone16);
        setRotationAngle(bone16, 0.7854F, 0.0F, 0.0F);
        bone16.setTextureOffset(9, 71).addBox(-5.6481F, -15.0006F, -1.0006F, 5.0F, 12.0F, 2.0F, 0.0F, false);

        bone17 = new ModelRenderer(this);
        bone17.setRotationPoint(0.0F, -16.0F, -2.0F);
        bone16.addChild(bone17);
        setRotationAngle(bone17, -0.4363F, 0.0F, 0.0F);
        bone17.setTextureOffset(9, 71).addBox(-5.6481F, -3.5166F, 1.3282F, 5.0F, 4.0F, 2.0F, 0.0F, false);

        bone18 = new ModelRenderer(this);
        bone18.setRotationPoint(0.0F, -4.0F, 0.0F);
        bone17.addChild(bone18);
        setRotationAngle(bone18, -0.4363F, 0.0F, 0.0F);
        bone18.setTextureOffset(9, 71).addBox(-5.6481F, -4.1232F, 1.408F, 5.0F, 4.0F, 2.0F, 0.0F, false);

        bone19 = new ModelRenderer(this);
        bone19.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone18.addChild(bone19);
        setRotationAngle(bone19, -0.4363F, 0.0F, 0.0F);
        bone19.setTextureOffset(8, 71).addBox(-5.6481F, -8.3319F, -0.4664F, 5.0F, 4.0F, 2.0F, 0.0F, false);

        bone20 = new ModelRenderer(this);
        bone20.setRotationPoint(-0.5F, 31.0F, -12.0F);
        Shellone3.addChild(bone20);
        setRotationAngle(bone20, 0.1745F, 0.0F, 0.0F);
        bone20.setTextureOffset(9, 71).addBox(-4.6481F, -28.7205F, 5.1894F, 5.0F, 2.0F, 9.0F, 0.0F, false);

        bone21 = new ModelRenderer(this);
        bone21.setRotationPoint(-0.5F, 31.0F, -12.0F);
        Shellone3.addChild(bone21);
        setRotationAngle(bone21, 0.3491F, 0.0F, 0.0F);
        bone21.setTextureOffset(9, 71).addBox(-4.6481F, -27.3831F, 1.0978F, 5.0F, 2.0F, 9.0F, 0.0F, false);

        Shellone4 = new ModelRenderer(this);
        Shellone4.setRotationPoint(1.2F, -28.2F, 16.0F);
        Shell.addChild(Shellone4);
        setRotationAngle(Shellone4, 0.0F, 3.1416F, 0.0F);


        bone22 = new ModelRenderer(this);
        bone22.setRotationPoint(-0.5F, 31.0F, -12.0F);
        Shellone4.addChild(bone22);
        bone22.setTextureOffset(8, 53).addBox(-2.1F, -7.8284F, 0.1716F, 5.0F, 3.0F, 2.0F, 0.0F, false);

        bone23 = new ModelRenderer(this);
        bone23.setRotationPoint(0.5F, 26.0F, -10.0F);
        Shellone4.addChild(bone23);
        setRotationAngle(bone23, 0.7854F, 0.0F, 0.0F);
        bone23.setTextureOffset(9, 71).addBox(-3.1F, -13.8787F, 0.1213F, 5.0F, 12.0F, 2.0F, 0.0F, false);

        bone24 = new ModelRenderer(this);
        bone24.setRotationPoint(0.0F, -16.0F, -2.0F);
        bone23.addChild(bone24);
        setRotationAngle(bone24, -0.4363F, 0.0F, 0.0F);
        bone24.setTextureOffset(9, 71).addBox(-3.1F, -2.9739F, 2.8191F, 5.0F, 4.0F, 2.0F, 0.0F, false);

        bone25 = new ModelRenderer(this);
        bone25.setRotationPoint(0.0F, -4.0F, 0.0F);
        bone24.addChild(bone25);
        setRotationAngle(bone25, -0.4363F, 0.0F, 0.0F);
        bone25.setTextureOffset(9, 71).addBox(-3.1F, -4.2615F, 2.9886F, 5.0F, 4.0F, 2.0F, 0.0F, false);

        bone26 = new ModelRenderer(this);
        bone26.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone25.addChild(bone26);
        setRotationAngle(bone26, -0.4363F, 0.0F, 0.0F);
        bone26.setTextureOffset(9, 71).addBox(-3.1F, -9.1252F, 0.9076F, 5.0F, 4.0F, 2.0F, 0.0F, false);

        bone27 = new ModelRenderer(this);
        bone27.setRotationPoint(-0.5F, 31.0F, -12.0F);
        Shellone4.addChild(bone27);
        setRotationAngle(bone27, 0.1745F, 0.0F, 0.0F);
        bone27.setTextureOffset(9, 71).addBox(-2.1F, -28.445F, 6.7519F, 5.0F, 2.0F, 9.0F, 0.0F, false);

        bone28 = new ModelRenderer(this);
        bone28.setRotationPoint(-0.5F, 31.0F, -12.0F);
        Shellone4.addChild(bone28);
        setRotationAngle(bone28, 0.3491F, 0.0F, 0.0F);
        bone28.setTextureOffset(9, 71).addBox(-2.1F, -26.8404F, 2.5887F, 5.0F, 2.0F, 9.0F, 0.0F, false);

        Shellone5 = new ModelRenderer(this);
        Shellone5.setRotationPoint(1.2F, -28.2F, 16.0F);
        Shell.addChild(Shellone5);
        setRotationAngle(Shellone5, 0.0F, 1.0472F, 0.0F);


        bone29 = new ModelRenderer(this);
        bone29.setRotationPoint(-0.5F, 31.0F, -12.0F);
        Shellone5.addChild(bone29);
        bone29.setTextureOffset(8, 53).addBox(0.6481F, -7.8284F, -4.2418F, 5.0F, 3.0F, 2.0F, 0.0F, false);

        bone30 = new ModelRenderer(this);
        bone30.setRotationPoint(0.5F, 26.0F, -10.0F);
        Shellone5.addChild(bone30);
        setRotationAngle(bone30, 0.7854F, 0.0F, 0.0F);
        bone30.setTextureOffset(9, 71).addBox(-0.3519F, -16.9994F, -2.9994F, 5.0F, 12.0F, 2.0F, 0.0F, false);

        bone31 = new ModelRenderer(this);
        bone31.setRotationPoint(0.0F, -16.0F, -2.0F);
        bone30.addChild(bone31);
        setRotationAngle(bone31, -0.4363F, 0.0F, 0.0F);
        bone31.setTextureOffset(8, 71).addBox(-0.3519F, -4.4834F, -1.3282F, 5.0F, 4.0F, 2.0F, 0.0F, false);

        bone32 = new ModelRenderer(this);
        bone32.setRotationPoint(0.0F, -4.0F, 0.0F);
        bone31.addChild(bone32);
        setRotationAngle(bone32, -0.4363F, 0.0F, 0.0F);
        bone32.setTextureOffset(8, 71).addBox(-0.3519F, -3.8768F, -1.408F, 5.0F, 4.0F, 2.0F, 0.0F, false);

        bone33 = new ModelRenderer(this);
        bone33.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone32.addChild(bone33);
        setRotationAngle(bone33, -0.4363F, 0.0F, 0.0F);
        bone33.setTextureOffset(8, 71).addBox(-0.3519F, -6.9185F, -2.9145F, 5.0F, 4.0F, 2.0F, 0.0F, false);

        bone34 = new ModelRenderer(this);
        bone34.setRotationPoint(-0.5F, 31.0F, -12.0F);
        Shellone5.addChild(bone34);
        setRotationAngle(bone34, 0.1745F, 0.0F, 0.0F);
        bone34.setTextureOffset(9, 71).addBox(0.6481F, -29.2114F, 2.4055F, 5.0F, 2.0F, 9.0F, 0.0F, false);

        bone35 = new ModelRenderer(this);
        bone35.setRotationPoint(-0.5F, 31.0F, -12.0F);
        Shellone5.addChild(bone35);
        setRotationAngle(bone35, 0.3491F, 0.0F, 0.0F);
        bone35.setTextureOffset(9, 71).addBox(0.6481F, -28.3499F, -1.5585F, 5.0F, 2.0F, 9.0F, 0.0F, false);

        Shellone6 = new ModelRenderer(this);
        Shellone6.setRotationPoint(1.2F, -28.2F, 16.0F);
        Shell.addChild(Shellone6);
        setRotationAngle(Shellone6, 0.0F, 2.0944F, 0.0F);


        bone36 = new ModelRenderer(this);
        bone36.setRotationPoint(-0.5F, 31.0F, -12.0F);
        Shellone6.addChild(bone36);
        bone36.setTextureOffset(8, 53).addBox(0.5481F, -7.8284F, -1.2418F, 5.0F, 3.0F, 2.0F, 0.0F, false);

        bone37 = new ModelRenderer(this);
        bone37.setRotationPoint(0.5F, 26.0F, -10.0F);
        Shellone6.addChild(bone37);
        setRotationAngle(bone37, 0.7854F, 0.0F, 0.0F);
        bone37.setTextureOffset(9, 71).addBox(-0.4519F, -14.8781F, -0.8781F, 5.0F, 12.0F, 2.0F, 0.0F, false);

        bone38 = new ModelRenderer(this);
        bone38.setRotationPoint(0.0F, -16.0F, -2.0F);
        bone37.addChild(bone38);
        setRotationAngle(bone38, -0.4363F, 0.0F, 0.0F);
        bone38.setTextureOffset(9, 71).addBox(-0.4519F, -3.4573F, 1.4909F, 5.0F, 4.0F, 2.0F, 0.0F, false);

        bone39 = new ModelRenderer(this);
        bone39.setRotationPoint(0.0F, -4.0F, 0.0F);
        bone38.addChild(bone39);
        setRotationAngle(bone39, -0.4363F, 0.0F, 0.0F);
        bone39.setTextureOffset(9, 71).addBox(-0.4519F, -4.1383F, 1.5806F, 5.0F, 4.0F, 2.0F, 0.0F, false);

        bone40 = new ModelRenderer(this);
        bone40.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone39.addChild(bone40);
        setRotationAngle(bone40, -0.4363F, 0.0F, 0.0F);
        bone40.setTextureOffset(9, 71).addBox(-0.4519F, -8.4185F, -0.3164F, 5.0F, 4.0F, 2.0F, 0.0F, false);

        bone41 = new ModelRenderer(this);
        bone41.setRotationPoint(-0.5F, 31.0F, -12.0F);
        Shellone6.addChild(bone41);
        setRotationAngle(bone41, 0.1745F, 0.0F, 0.0F);
        bone41.setTextureOffset(9, 71).addBox(0.5481F, -28.6905F, 5.3599F, 5.0F, 2.0F, 9.0F, 0.0F, false);

        bone42 = new ModelRenderer(this);
        bone42.setRotationPoint(-0.5F, 31.0F, -12.0F);
        Shellone6.addChild(bone42);
        setRotationAngle(bone42, 0.3491F, 0.0F, 0.0F);
        bone42.setTextureOffset(9, 71).addBox(0.5481F, -27.3238F, 1.2605F, 5.0F, 2.0F, 9.0F, 0.0F, false);

        bone50 = new ModelRenderer(this);
        bone50.setRotationPoint(1.2F, -28.2F, 16.0F);
        Shell.addChild(bone50);
        setRotationAngle(bone50, 0.0F, -0.5236F, 0.0F);
        bone50.setTextureOffset(98, 3).addBox(-6.4003F, 7.4F, 17.9631F, 10.0F, 5.0F, 2.0F, 0.0F, false);

        bone51 = new ModelRenderer(this);
        bone51.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone50.addChild(bone51);
        setRotationAngle(bone51, 0.0F, -0.4363F, 0.0F);
        bone51.setTextureOffset(98, 3).addBox(-5.3638F, 7.4F, 18.7976F, 8.0F, 5.0F, 2.0F, 0.0F, false);

        bone52 = new ModelRenderer(this);
        bone52.setRotationPoint(18.0F, 0.0F, 0.0F);
        bone50.addChild(bone52);
        setRotationAngle(bone52, 0.0F, 0.4363F, 0.0F);
        bone52.setTextureOffset(100, 3).addBox(-21.4879F, 7.4F, 10.0069F, 8.0F, 5.0F, 2.0F, 0.0F, false);

        bone53 = new ModelRenderer(this);
        bone53.setRotationPoint(1.2F, -28.2F, 16.0F);
        Shell.addChild(bone53);
        setRotationAngle(bone53, 0.0F, -1.5708F, 0.0F);
        bone53.setTextureOffset(98, 3).addBox(-7.9869F, 7.4F, 20.5112F, 10.0F, 5.0F, 2.0F, 0.0F, false);

        bone54 = new ModelRenderer(this);
        bone54.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone53.addChild(bone54);
        setRotationAngle(bone54, 0.0F, -0.4363F, 0.0F);
        bone54.setTextureOffset(98, 3).addBox(-5.7249F, 7.4F, 21.7775F, 8.0F, 5.0F, 2.0F, 0.0F, false);

        bone55 = new ModelRenderer(this);
        bone55.setRotationPoint(18.0F, 0.0F, 0.0F);
        bone53.addChild(bone55);
        setRotationAngle(bone55, 0.0F, 0.4363F, 0.0F);
        bone55.setTextureOffset(100, 3).addBox(-24.0027F, 7.4F, 11.6458F, 8.0F, 5.0F, 2.0F, 0.0F, false);

        bone56 = new ModelRenderer(this);
        bone56.setRotationPoint(1.2F, -28.2F, 16.0F);
        Shell.addChild(bone56);
        setRotationAngle(bone56, 0.0F, -2.618F, 0.0F);
        bone56.setTextureOffset(98, 3).addBox(-6.5735F, 7.4F, 23.1593F, 10.0F, 5.0F, 2.0F, 0.0F, false);

        bone57 = new ModelRenderer(this);
        bone57.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone56.addChild(bone57);
        setRotationAngle(bone57, 0.0F, -0.4363F, 0.0F);
        bone57.setTextureOffset(98, 3).addBox(-3.3248F, 7.4F, 23.5801F, 8.0F, 5.0F, 2.0F, 0.0F, false);

        bone58 = new ModelRenderer(this);
        bone58.setRotationPoint(18.0F, 0.0F, 0.0F);
        bone56.addChild(bone58);
        setRotationAngle(bone58, 0.0F, 0.4363F, 0.0F);
        bone58.setTextureOffset(100, 3).addBox(-23.8408F, 7.4F, 14.6431F, 8.0F, 5.0F, 2.0F, 0.0F, false);

        bone59 = new ModelRenderer(this);
        bone59.setRotationPoint(1.2F, -28.2F, 16.0F);
        Shell.addChild(bone59);
        setRotationAngle(bone59, 0.0F, 2.618F, 0.0F);
        bone59.setTextureOffset(98, 3).addBox(-3.5735F, 7.4F, 23.2593F, 10.0F, 5.0F, 2.0F, 0.0F, false);

        bone60 = new ModelRenderer(this);
        bone60.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone59.addChild(bone60);
        setRotationAngle(bone60, 0.0F, -0.4363F, 0.0F);
        bone60.setTextureOffset(98, 3).addBox(-0.5636F, 7.4F, 22.4029F, 8.0F, 5.0F, 2.0F, 0.0F, false);

        bone61 = new ModelRenderer(this);
        bone61.setRotationPoint(18.0F, 0.0F, 0.0F);
        bone59.addChild(bone61);
        setRotationAngle(bone61, 0.0F, 0.4363F, 0.0F);
        bone61.setTextureOffset(100, 3).addBox(-21.1642F, 7.4F, 16.0015F, 8.0F, 5.0F, 2.0F, 0.0F, false);

        bone62 = new ModelRenderer(this);
        bone62.setRotationPoint(1.2F, -28.2F, 16.0F);
        Shell.addChild(bone62);
        setRotationAngle(bone62, 0.0F, 1.5708F, 0.0F);
        bone62.setTextureOffset(98, 3).addBox(-1.9869F, 7.4F, 20.7112F, 10.0F, 5.0F, 2.0F, 0.0F, false);

        bone63 = new ModelRenderer(this);
        bone63.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone62.addChild(bone63);
        setRotationAngle(bone63, 0.0F, -0.4363F, 0.0F);
        bone63.setTextureOffset(98, 3).addBox(-0.2025F, 7.4F, 19.423F, 8.0F, 5.0F, 2.0F, 0.0F, false);

        bone64 = new ModelRenderer(this);
        bone64.setRotationPoint(18.0F, 0.0F, 0.0F);
        bone62.addChild(bone64);
        setRotationAngle(bone64, 0.0F, 0.4363F, 0.0F);
        bone64.setTextureOffset(100, 3).addBox(-18.6493F, 7.4F, 14.3627F, 8.0F, 5.0F, 2.0F, 0.0F, false);

        bone65 = new ModelRenderer(this);
        bone65.setRotationPoint(1.2F, -28.2F, 16.0F);
        Shell.addChild(bone65);
        setRotationAngle(bone65, 0.0F, 0.5236F, 0.0F);
        bone65.setTextureOffset(98, 3).addBox(-3.4003F, 7.4F, 18.0631F, 10.0F, 5.0F, 2.0F, 0.0F, false);

        bone66 = new ModelRenderer(this);
        bone66.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone65.addChild(bone66);
        setRotationAngle(bone66, 0.0F, -0.4363F, 0.0F);
        bone66.setTextureOffset(98, 3).addBox(-2.6026F, 7.4F, 17.6204F, 8.0F, 5.0F, 2.0F, 0.0F, false);

        bone67 = new ModelRenderer(this);
        bone67.setRotationPoint(18.0F, 0.0F, 0.0F);
        bone65.addChild(bone67);
        setRotationAngle(bone67, 0.0F, 0.4363F, 0.0F);
        bone67.setTextureOffset(100, 2).addBox(-18.8112F, 7.4F, 11.3654F, 8.0F, 5.0F, 2.0F, 0.0F, false);

        bone68 = new ModelRenderer(this);
        bone68.setRotationPoint(2.2F, -23.2F, 16.0F);
        Shell.addChild(bone68);
        setRotationAngle(bone68, 0.0F, -1.5708F, 0.0F);
        bone68.setTextureOffset(4, 15).addBox(-7.9869F, 7.4F, 10.5112F, 10.0F, 3.0F, 12.0F, 0.0F, false);

        bone69 = new ModelRenderer(this);
        bone69.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone68.addChild(bone69);
        setRotationAngle(bone69, 0.0F, -0.4363F, 0.0F);
        bone69.setTextureOffset(4, 15).addBox(-5.7249F, 7.4F, 11.7775F, 8.0F, 3.0F, 12.0F, 0.0F, false);

        bone70 = new ModelRenderer(this);
        bone70.setRotationPoint(18.0F, 0.0F, 0.0F);
        bone68.addChild(bone70);
        setRotationAngle(bone70, 0.0F, 0.4363F, 0.0F);
        bone70.setTextureOffset(4, 15).addBox(-24.0027F, 7.4F, 1.6458F, 8.0F, 3.0F, 12.0F, 0.0F, false);

        bone71 = new ModelRenderer(this);
        bone71.setRotationPoint(2.2F, -23.2F, 17.0F);
        Shell.addChild(bone71);
        setRotationAngle(bone71, 0.0F, -2.618F, 0.0F);
        bone71.setTextureOffset(6, 14).addBox(-6.5735F, 7.4F, 15.1593F, 10.0F, 3.0F, 10.0F, 0.0F, false);

        bone72 = new ModelRenderer(this);
        bone72.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone71.addChild(bone72);
        setRotationAngle(bone72, 0.0F, -0.4363F, 0.0F);
        bone72.setTextureOffset(6, 14).addBox(-3.3248F, 7.4F, 15.5801F, 8.0F, 3.0F, 10.0F, 0.0F, false);

        bone73 = new ModelRenderer(this);
        bone73.setRotationPoint(18.0F, 0.0F, 0.0F);
        bone71.addChild(bone73);
        setRotationAngle(bone73, 0.0F, 0.4363F, 0.0F);
        bone73.setTextureOffset(6, 14).addBox(-23.8408F, 7.4F, 6.6431F, 8.0F, 3.0F, 10.0F, 0.0F, false);

        bone74 = new ModelRenderer(this);
        bone74.setRotationPoint(0.2F, -23.2F, 17.0F);
        Shell.addChild(bone74);
        setRotationAngle(bone74, 0.0F, 2.618F, 0.0F);
        bone74.setTextureOffset(5, 18).addBox(-3.5735F, 7.4F, 12.2593F, 10.0F, 3.0F, 13.0F, 0.0F, false);

        bone75 = new ModelRenderer(this);
        bone75.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone74.addChild(bone75);
        setRotationAngle(bone75, 0.0F, -0.4363F, 0.0F);
        bone75.setTextureOffset(5, 18).addBox(-0.5636F, 7.4F, 11.4029F, 8.0F, 3.0F, 13.0F, 0.0F, false);

        bone76 = new ModelRenderer(this);
        bone76.setRotationPoint(18.0F, 0.0F, 0.0F);
        bone74.addChild(bone76);
        setRotationAngle(bone76, 0.0F, 0.4363F, 0.0F);
        bone76.setTextureOffset(5, 18).addBox(-21.1642F, 7.4F, 5.0015F, 8.0F, 3.0F, 13.0F, 0.0F, false);

        bone77 = new ModelRenderer(this);
        bone77.setRotationPoint(0.2F, -23.2F, 16.0F);
        Shell.addChild(bone77);
        setRotationAngle(bone77, 0.0F, 1.5708F, 0.0F);
        bone77.setTextureOffset(10, 16).addBox(-1.9869F, 7.4F, 9.7112F, 10.0F, 3.0F, 13.0F, 0.0F, false);

        bone78 = new ModelRenderer(this);
        bone78.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone77.addChild(bone78);
        setRotationAngle(bone78, 0.0F, -0.4363F, 0.0F);
        bone78.setTextureOffset(10, 16).addBox(-0.2025F, 7.4F, 8.423F, 8.0F, 3.0F, 13.0F, 0.0F, false);

        bone79 = new ModelRenderer(this);
        bone79.setRotationPoint(18.0F, 0.0F, 0.0F);
        bone77.addChild(bone79);
        setRotationAngle(bone79, 0.0F, 0.4363F, 0.0F);
        bone79.setTextureOffset(10, 16).addBox(-18.6493F, 7.4F, 3.3627F, 8.0F, 3.0F, 13.0F, 0.0F, false);

        bone80 = new ModelRenderer(this);
        bone80.setRotationPoint(0.2F, -23.2F, 15.0F);
        Shell.addChild(bone80);
        setRotationAngle(bone80, 0.0F, 0.5236F, 0.0F);
        bone80.setTextureOffset(6, 19).addBox(-3.4003F, 7.4F, 9.0631F, 10.0F, 3.0F, 11.0F, 0.0F, false);

        bone81 = new ModelRenderer(this);
        bone81.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone80.addChild(bone81);
        setRotationAngle(bone81, 0.0F, -0.4363F, 0.0F);
        bone81.setTextureOffset(6, 19).addBox(-2.6026F, 7.4F, 8.6204F, 8.0F, 3.0F, 11.0F, 0.0F, false);

        bone82 = new ModelRenderer(this);
        bone82.setRotationPoint(18.0F, 0.0F, 0.0F);
        bone80.addChild(bone82);
        setRotationAngle(bone82, 0.0F, 0.4363F, 0.0F);
        bone82.setTextureOffset(6, 19).addBox(-18.8112F, 7.4F, 2.3654F, 8.0F, 3.0F, 11.0F, 0.0F, false);

        bone83 = new ModelRenderer(this);
        bone83.setRotationPoint(2.2F, -23.2F, 15.0F);
        Shell.addChild(bone83);
        setRotationAngle(bone83, 0.0F, -0.5236F, 0.0F);
        bone83.setTextureOffset(7, 11).addBox(-6.4003F, 7.4F, 8.9631F, 10.0F, 3.0F, 11.0F, 0.0F, false);

        bone84 = new ModelRenderer(this);
        bone84.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone83.addChild(bone84);
        setRotationAngle(bone84, 0.0F, -0.4363F, 0.0F);
        bone84.setTextureOffset(7, 11).addBox(-5.3638F, 7.4F, 9.7976F, 8.0F, 3.0F, 11.0F, 0.0F, false);

        bone85 = new ModelRenderer(this);
        bone85.setRotationPoint(18.0F, 0.0F, 0.0F);
        bone83.addChild(bone85);
        setRotationAngle(bone85, 0.0F, 0.4363F, 0.0F);
        bone85.setTextureOffset(7, 11).addBox(-21.4879F, 7.4F, 1.0069F, 8.0F, 3.0F, 11.0F, 0.0F, false);

        bone43 = new ModelRenderer(this);
        bone43.setRotationPoint(1.2F, -28.2F, 16.0F);
        Shell.addChild(bone43);
        bone43.setTextureOffset(0, 8).addBox(-8.9F, 8.0F, -13.0F, 18.0F, 13.0F, 20.0F, 0.0F, false);
        bone43.setTextureOffset(0, 11).addBox(-10.9F, 10.0F, -15.0F, 22.0F, 7.0F, 24.0F, 0.0F, false);
        bone43.setTextureOffset(0, 18).addBox(-8.9F, 23.0F, -13.0F, 18.0F, 2.0F, 20.0F, 0.0F, false);
        bone43.setTextureOffset(0, 0).addBox(-7.9F, 19.0F, -12.0F, 16.0F, 5.0F, 18.0F, 0.0F, false);
        bone43.setTextureOffset(0, 15).addBox(-9.9F, 19.0F, -14.0F, 20.0F, 2.0F, 22.0F, 0.0F, false);

        bone86 = new ModelRenderer(this);
        bone86.setRotationPoint(1.2F, -28.2F, 16.0F);
        Shell.addChild(bone86);
        bone86.setTextureOffset(0, 0).addBox(-2.4F, -5.0F, 0.4F, 5.0F, 7.0F, 1.0F, 0.0F, false);

        bone87 = new ModelRenderer(this);
        bone87.setRotationPoint(1.2F, -28.2F, 16.0F);
        Shell.addChild(bone87);
        setRotationAngle(bone87, 0.0F, -1.0472F, 0.0F);
        bone87.setTextureOffset(0, 0).addBox(-4.9876F, -5.0F, 1.7785F, 5.0F, 7.0F, 1.0F, 0.0F, false);

        bone88 = new ModelRenderer(this);
        bone88.setRotationPoint(1.2F, -28.2F, 16.0F);
        Shell.addChild(bone88);
        setRotationAngle(bone88, 0.0F, -2.0944F, 0.0F);
        bone88.setTextureOffset(0, 0).addBox(-5.0876F, -5.0F, 4.7086F, 5.0F, 7.0F, 1.0F, 0.0F, false);

        bone89 = new ModelRenderer(this);
        bone89.setRotationPoint(1.2F, -28.2F, 16.0F);
        Shell.addChild(bone89);
        setRotationAngle(bone89, 0.0F, 3.1416F, 0.0F);
        bone89.setTextureOffset(0, 0).addBox(-2.6F, -5.0F, 6.2603F, 5.0F, 7.0F, 1.0F, 0.0F, false);

        bone90 = new ModelRenderer(this);
        bone90.setRotationPoint(1.2F, -28.2F, 16.0F);
        Shell.addChild(bone90);
        setRotationAngle(bone90, 0.0F, 1.0472F, 0.0F);
        bone90.setTextureOffset(0, 0).addBox(0.0876F, -5.0F, 1.9517F, 5.0F, 7.0F, 1.0F, 0.0F, false);

        bone91 = new ModelRenderer(this);
        bone91.setRotationPoint(1.2F, -28.2F, 16.0F);
        Shell.addChild(bone91);
        setRotationAngle(bone91, 0.0F, 2.0944F, 0.0F);
        bone91.setTextureOffset(0, 0).addBox(-0.0124F, -5.0F, 4.8818F, 5.0F, 7.0F, 1.0F, 0.0F, false);

        bone93 = new ModelRenderer(this);
        bone93.setRotationPoint(1.2F, -37.2F, 16.0F);
        Shell.addChild(bone93);
        bone93.setTextureOffset(88, 20).addBox(-4.0605F, 5.6F, 2.9952F, 8.0F, 1.0F, 1.0F, 0.0F, false);
        bone93.setTextureOffset(97, 24).addBox(-4.0605F, -42.4F, 2.9952F, 8.0F, 6.0F, 1.0F, 0.0F, false);
        bone93.setTextureOffset(88, 20).addBox(-4.0605F, 3.0F, 2.9952F, 8.0F, 1.0F, 1.0F, 0.0F, false);

        bone94 = new ModelRenderer(this);
        bone94.setRotationPoint(1.2F, -37.2F, 16.0F);
        Shell.addChild(bone94);
        setRotationAngle(bone94, 0.0F, -1.0472F, 0.0F);
        bone94.setTextureOffset(88, 20).addBox(-6.5703F, 5.6F, 4.5141F, 8.0F, 1.0F, 1.0F, 0.0F, false);
        bone94.setTextureOffset(101, 25).addBox(-6.5703F, -42.4F, 4.5141F, 8.0F, 6.0F, 1.0F, 0.0F, true);
        bone94.setTextureOffset(88, 20).addBox(-6.5703F, 3.0F, 4.5141F, 8.0F, 1.0F, 1.0F, 0.0F, false);

        bone95 = new ModelRenderer(this);
        bone95.setRotationPoint(1.2F, -37.2F, 16.0F);
        Shell.addChild(bone95);
        setRotationAngle(bone95, 0.0F, -2.0944F, 0.0F);
        bone95.setTextureOffset(88, 20).addBox(-6.5098F, 5.6F, -4.5529F, 8.0F, 1.0F, 13.0F, 0.0F, false);
        bone95.setTextureOffset(90, 16).addBox(-6.5098F, -42.4F, -4.5529F, 8.0F, 6.0F, 13.0F, 0.0F, true);
        bone95.setTextureOffset(88, 20).addBox(-6.5098F, 3.0F, -4.5529F, 8.0F, 1.0F, 13.0F, 0.0F, false);

        bone96 = new ModelRenderer(this);
        bone96.setRotationPoint(1.2F, -37.2F, 16.0F);
        Shell.addChild(bone96);
        setRotationAngle(bone96, 0.0F, 3.1416F, 0.0F);
        bone96.setTextureOffset(88, 20).addBox(-3.9395F, 5.6F, -3.1388F, 8.0F, 1.0F, 13.0F, 0.0F, false);
        bone96.setTextureOffset(88, 16).addBox(-3.9395F, -42.4F, -3.1388F, 8.0F, 6.0F, 13.0F, 0.0F, false);
        bone96.setTextureOffset(88, 20).addBox(-3.9395F, 3.0F, -3.1388F, 8.0F, 1.0F, 13.0F, 0.0F, false);

        bone97 = new ModelRenderer(this);
        bone97.setRotationPoint(1.2F, -37.2F, 16.0F);
        Shell.addChild(bone97);
        setRotationAngle(bone97, 0.0F, 1.0472F, 0.0F);
        bone97.setTextureOffset(88, 20).addBox(-1.4902F, 5.6F, 4.4093F, 8.0F, 1.0F, 1.0F, 0.0F, false);
        bone97.setTextureOffset(93, 23).addBox(-1.4902F, -42.4F, 4.4093F, 8.0F, 6.0F, 1.0F, 0.0F, false);
        bone97.setTextureOffset(88, 20).addBox(-1.4902F, 3.0F, 4.4093F, 8.0F, 1.0F, 1.0F, 0.0F, false);

        bone98 = new ModelRenderer(this);
        bone98.setRotationPoint(1.2F, -37.2F, 16.0F);
        Shell.addChild(bone98);
        setRotationAngle(bone98, 0.0F, 2.0944F, 0.0F);
        bone98.setTextureOffset(88, 20).addBox(-1.4297F, 5.6F, -4.6577F, 8.0F, 1.0F, 13.0F, 0.0F, false);
        bone98.setTextureOffset(88, 16).addBox(-1.4297F, -42.4F, -4.6577F, 8.0F, 6.0F, 13.0F, 0.0F, false);
        bone98.setTextureOffset(88, 20).addBox(-1.4297F, 3.0F, -4.6577F, 8.0F, 1.0F, 13.0F, 0.0F, false);

        bone105 = new ModelRenderer(this);
        bone105.setRotationPoint(9.7F, -19.0F, -12.2F);
        all.addChild(bone105);
        setRotationAngle(bone105, 0.4363F, -0.5236F, 0.0F);
        bone105.setTextureOffset(10, 110).addBox(-4.683F, -1.9082F, -3.7111F, 4.0F, 2.0F, 4.0F, 0.0F, false);
        bone105.setTextureOffset(67, 185).addBox(-4.1768F, -2.22F, -3.143F, 3.0F, 2.0F, 3.0F, 0.0F, false);

        glow = new LightModelRenderer(this);
        glow.setRotationPoint(9.5F, 5.0F, -12.2F);


        keyboard_behind_monitor_glow = new LightModelRenderer(this);
        keyboard_behind_monitor_glow.setRotationPoint(-8.8F, -7.6F, 12.7F);
        glow.addChild(keyboard_behind_monitor_glow);
        setRotationAngle(keyboard_behind_monitor_glow, -0.6109F, 0.9599F, -0.6981F);
        keyboard_behind_monitor_glow.setTextureOffset(185, 145).addBox(-14.1986F, -0.1964F, 2.1787F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_behind_monitor_glow.setTextureOffset(182, 124).addBox(-14.1986F, -0.1964F, 0.1787F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_behind_monitor_glow.setTextureOffset(185, 145).addBox(-14.1986F, -0.1964F, -1.8213F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_behind_monitor_glow.setTextureOffset(185, 145).addBox(-14.1986F, -0.1964F, -3.8213F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_behind_monitor_glow.setTextureOffset(185, 145).addBox(-14.1986F, -0.1964F, -5.8213F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        keyboard_by_monitor_glow = new LightModelRenderer(this);
        keyboard_by_monitor_glow.setRotationPoint(-11.9F, -6.3F, 18.9F);
        glow.addChild(keyboard_by_monitor_glow);
        setRotationAngle(keyboard_by_monitor_glow, -0.6109F, 0.9425F, -0.6981F);
        keyboard_by_monitor_glow.setTextureOffset(186, 142).addBox(-12.5034F, 0.4849F, 1.1071F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor_glow.setTextureOffset(186, 142).addBox(-12.5034F, 0.4849F, -1.3929F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor_glow.setTextureOffset(186, 142).addBox(-12.5034F, 0.4849F, -3.7929F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        keyboard_by_monitor_glow.setTextureOffset(186, 142).addBox(-12.5034F, 0.4849F, -6.2929F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        bone44 = new ModelRenderer(this);
        bone44.setRotationPoint(-4.5F, -12.2F, 18.2F);
        glow.addChild(bone44);
        setRotationAngle(bone44, 0.0873F, -0.5236F, -0.1745F);
        bone44.setTextureOffset(188, 140).addBox(-18.4147F, 2.7866F, -4.6388F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        refuel2 = new ModelRenderer(this);
        refuel2.setRotationPoint(-6.0F, -5.3F, 16.6F);
        glow.addChild(refuel2);
        setRotationAngle(refuel2, 0.384F, -1.0472F, -0.4363F);
        refuel2.setTextureOffset(107, 17).addBox(-16.0486F, -2.4317F, 0.507F, 2.0F, 1.0F, 2.0F, 0.0F, false);

        doorcontrol2 = new ModelRenderer(this);
        doorcontrol2.setRotationPoint(-11.9F, -3.3F, 12.2F);
        glow.addChild(doorcontrol2);
        setRotationAngle(doorcontrol2, 0.384F, -1.0472F, -0.4363F);
        doorcontrol2.setTextureOffset(100, 9).addBox(-17.8461F, -1.6152F, 4.2918F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        land_type_red = new ModelRenderer(this);
        land_type_red.setRotationPoint(0.0F, 0.0F, 0.0F);
        glow.addChild(land_type_red);
        setRotationAngle(land_type_red, 0.4363F, -0.5236F, 0.0F);
        land_type_red.setTextureOffset(182, 142).addBox(-3.734F, -3.1158F, -2.6444F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        monitor_screen = new LightModelRenderer(this);
        monitor_screen.setRotationPoint(-9.8F, -9.0F, 12.6F);
        glow.addChild(monitor_screen);
        setRotationAngle(monitor_screen, 0.0F, -0.5236F, 0.0F);
        monitor_screen.setTextureOffset(71, 142).addBox(-3.6261F, -3.0F, 12.9797F, 7.0F, 6.0F, 1.0F, 0.0F, false);

        throttle_glow = new LightModelRenderer(this);
        throttle_glow.setRotationPoint(-7.8F, -10.0F, 12.6F);
        glow.addChild(throttle_glow);
        setRotationAngle(throttle_glow, -0.2618F, 0.0F, 0.0F);
        throttle_glow.setTextureOffset(186, 142).addBox(-1.1F, -1.3235F, 11.6022F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        throttle_glow.setTextureOffset(195, 117).addBox(-1.1F, -1.3235F, 13.6022F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        throttle_glow.setTextureOffset(190, 99).addBox(-1.1F, -0.9235F, 15.6022F, 1.0F, 1.0F, 1.0F, -0.1F, false);

        shell_glow = new LightModelRenderer(this);
        shell_glow.setRotationPoint(-10.7F, 21.0F, -0.8F);
        glow.addChild(shell_glow);


        bone150 = new ModelRenderer(this);
        bone150.setRotationPoint(1.2F, -28.2F, 16.0F);
        shell_glow.addChild(bone150);
        setRotationAngle(bone150, 0.4363F, -0.5236F, 0.0F);
        bone150.setTextureOffset(96, 75).addBox(-5.0134F, -2.1191F, -9.8F, 7.0F, 1.0F, 2.0F, 0.0F, false);
        bone150.setTextureOffset(96, 75).addBox(-7.0134F, -2.1191F, -11.8F, 11.0F, 1.0F, 2.0F, 0.0F, false);
        bone150.setTextureOffset(80, 75).addBox(-9.0134F, -2.1191F, -17.8F, 15.0F, 1.0F, 6.0F, 0.0F, false);
        bone150.setTextureOffset(72, 85).addBox(-11.0134F, -2.1191F, -24.8F, 19.0F, 1.0F, 7.0F, 0.0F, false);

        bone151 = new ModelRenderer(this);
        bone151.setRotationPoint(1.2F, -28.2F, 16.0F);
        shell_glow.addChild(bone151);
        setRotationAngle(bone151, 0.4363F, -1.5708F, 0.0F);
        bone151.setTextureOffset(93, 79).addBox(-6.6F, -1.0423F, -7.4906F, 7.0F, 1.0F, 2.0F, 0.0F, false);
        bone151.setTextureOffset(93, 79).addBox(-8.6F, -1.0423F, -9.4906F, 11.0F, 1.0F, 2.0F, 0.0F, false);
        bone151.setTextureOffset(80, 75).addBox(-10.6F, -1.0423F, -15.4906F, 15.0F, 1.0F, 6.0F, 0.0F, false);
        bone151.setTextureOffset(72, 85).addBox(-12.6F, -1.0423F, -22.4906F, 19.0F, 1.0F, 7.0F, 0.0F, false);

        bone152 = new ModelRenderer(this);
        bone152.setRotationPoint(1.2F, -28.2F, 16.0F);
        shell_glow.addChild(bone152);
        setRotationAngle(bone152, 0.4363F, -2.618F, 0.0F);
        bone152.setTextureOffset(86, 77).addBox(-5.1866F, 0.0769F, -5.0907F, 7.0F, 1.0F, 2.0F, 0.0F, false);
        bone152.setTextureOffset(86, 77).addBox(-7.1866F, 0.0769F, -7.0907F, 11.0F, 1.0F, 2.0F, 0.0F, false);
        bone152.setTextureOffset(80, 75).addBox(-9.1866F, 0.0769F, -13.0907F, 15.0F, 1.0F, 6.0F, 0.0F, false);
        bone152.setTextureOffset(72, 85).addBox(-11.1866F, 0.0769F, -20.0907F, 19.0F, 1.0F, 7.0F, 0.0F, false);

        bone153 = new ModelRenderer(this);
        bone153.setRotationPoint(1.2F, -28.2F, 16.0F);
        shell_glow.addChild(bone153);
        setRotationAngle(bone153, 0.4363F, 2.618F, 0.0F);
        bone153.setTextureOffset(78, 75).addBox(-2.1866F, 0.1191F, -5.0F, 7.0F, 1.0F, 2.0F, 0.0F, false);
        bone153.setTextureOffset(78, 75).addBox(-4.1866F, 0.1191F, -7.0F, 11.0F, 1.0F, 2.0F, 0.0F, false);
        bone153.setTextureOffset(80, 75).addBox(-6.1866F, 0.1191F, -13.0F, 15.0F, 1.0F, 6.0F, 0.0F, false);
        bone153.setTextureOffset(72, 85).addBox(-8.1866F, 0.1191F, -20.0F, 19.0F, 1.0F, 7.0F, 0.0F, false);

        bone154 = new ModelRenderer(this);
        bone154.setRotationPoint(1.2F, -28.2F, 16.0F);
        shell_glow.addChild(bone154);
        setRotationAngle(bone154, 0.4363F, 1.5708F, 0.0F);
        bone154.setTextureOffset(79, 77).addBox(-0.6F, -0.9577F, -7.3094F, 7.0F, 1.0F, 2.0F, 0.0F, false);
        bone154.setTextureOffset(79, 77).addBox(-2.6F, -0.9577F, -9.3094F, 11.0F, 1.0F, 2.0F, 0.0F, false);
        bone154.setTextureOffset(80, 75).addBox(-4.6F, -0.9577F, -15.3094F, 15.0F, 1.0F, 6.0F, 0.0F, false);
        bone154.setTextureOffset(72, 85).addBox(-6.6F, -0.9577F, -22.3094F, 19.0F, 1.0F, 7.0F, 0.0F, false);

        bone155 = new ModelRenderer(this);
        bone155.setRotationPoint(1.2F, -28.2F, 16.0F);
        shell_glow.addChild(bone155);
        setRotationAngle(bone155, 0.4363F, 0.5236F, 0.0F);
        bone155.setTextureOffset(74, 63).addBox(-2.0134F, -2.0769F, -9.7093F, 7.0F, 1.0F, 2.0F, 0.0F, false);
        bone155.setTextureOffset(74, 63).addBox(-4.0134F, -2.0769F, -11.7093F, 11.0F, 1.0F, 2.0F, 0.0F, false);
        bone155.setTextureOffset(80, 75).addBox(-6.0134F, -2.0769F, -17.7093F, 15.0F, 1.0F, 6.0F, 0.0F, false);
        bone155.setTextureOffset(72, 85).addBox(-8.0134F, -2.0769F, -24.7093F, 19.0F, 1.0F, 7.0F, 0.0F, false);

        rotartop = new LightModelRenderer(this);
		rotartop.setRotationPoint(0.3F, -44.2F, 0.0F);
		rotartop.setTextureOffset(164, 32).addBox(-3.9F, 1.0F, -3.5F, 7.0F, 2.0F, 7.0F, 0.0F, false);
		rotartop.setTextureOffset(164, 32).addBox(-3.9F, -5.0F, -3.5F, 7.0F, 2.0F, 7.0F, 0.0F, false);
		rotartop.setTextureOffset(169, 36).addBox(0.2F, -9.0F, -3.2F, 2.0F, 28.0F, 2.0F, 0.0F, false);
		rotartop.setTextureOffset(169, 36).addBox(0.2F, -9.0F, 1.0F, 2.0F, 28.0F, 2.0F, 0.0F, false);
		rotartop.setTextureOffset(169, 36).addBox(-3.1F, -9.0F, -3.2F, 2.0F, 28.0F, 2.0F, 0.0F, false);
		rotartop.setTextureOffset(169, 36).addBox(-3.1F, -9.0F, 1.0F, 2.0F, 28.0F, 2.0F, 0.0F, false);

		rotarbottom = new LightModelRenderer(this);
		rotarbottom.setRotationPoint(0.3F, -23.2F, 0.0F);
		rotarbottom.setTextureOffset(164, 32).addBox(-3.9F, 14.0F, -3.5F, 7.0F, 2.0F, 7.0F, 0.0F, false);
		rotarbottom.setTextureOffset(164, 32).addBox(-3.9F, 8.0F, -3.5F, 7.0F, 2.0F, 7.0F, 0.0F, false);
		rotarbottom.setTextureOffset(182, 14).addBox(0.2F, -8.0F, -3.2F, 2.0F, 28.0F, 2.0F, 0.125F, false);
		rotarbottom.setTextureOffset(182, 14).addBox(0.2F, -8.0F, 1.0F, 2.0F, 28.0F, 2.0F, 0.125F, false);
		rotarbottom.setTextureOffset(182, 14).addBox(-3.1F, -8.0F, -3.2F, 2.0F, 28.0F, 2.0F, 0.125F, false);
		rotarbottom.setTextureOffset(182, 14).addBox(-3.1F, -8.0F, 1.0F, 2.0F, 28.0F, 2.0F, 0.125F, false);

        phone = new ModelRenderer(this);
        phone.setRotationPoint(-3.8F, -11.0F, 11.1F);
        glow.addChild(phone);
        setRotationAngle(phone, 0.0F, 1.5708F, 0.3491F);
        phone.setTextureOffset(112, 20).addBox(-1.6F, 4.5314F, 11.3125F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        phone.setTextureOffset(186, 142).addBox(-8.0F, 5.6327F, 13.8417F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        phone.setTextureOffset(186, 142).addBox(-4.5F, 4.9597F, 7.2117F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        phone.setTextureOffset(188, 97).addBox(-3.5F, 4.9597F, 7.2117F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        phone.setTextureOffset(96, 18).addBox(-8.0F, 5.6666F, 14.9182F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        phone.setTextureOffset(187, 121).addBox(-3.5F, 4.9936F, 8.2882F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        phone.setTextureOffset(110, 17).addBox(0.2F, 4.5314F, 11.3125F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        phone.setTextureOffset(184, 95).addBox(-4.2F, 5.1629F, 13.6707F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        phone.setTextureOffset(110, 17).addBox(-0.7F, 4.5314F, 12.3125F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        phone.setTextureOffset(179, 123).addBox(-5.1F, 5.1629F, 14.6707F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        phone.setTextureOffset(110, 17).addBox(-1.6F, 4.5314F, 13.3125F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        phone.setTextureOffset(187, 140).addBox(-6.0F, 5.1629F, 15.6707F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        phone.setTextureOffset(110, 17).addBox(0.2F, 4.5314F, 13.3125F, 1.0F, 1.0F, 1.0F, -0.2F, false);

        land_type_glow = new LightModelRenderer(this);
        land_type_glow.setRotationPoint(4.0F, 0.0F, 2.0F);
        glow.addChild(land_type_glow);
        setRotationAngle(land_type_glow, 0.4363F, -0.5236F, 0.0F);
        land_type_glow.setTextureOffset(104, 13).addBox(-5.0821F, -1.5669F, 1.6773F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        land_type_glow.setTextureOffset(192, 130).addBox(-2.367F, -1.2394F, 1.1965F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        land_type_glow.setTextureOffset(187, 116).addBox(-1.535F, -1.5051F, -1.503F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        land_type_glow.setTextureOffset(94, 22).addBox(-1.6421F, -1.4441F, -3.5017F, 2.0F, 1.0F, 1.0F, 0.0F, false);

        telepathic_circuit_glow = new LightModelRenderer(this);
        telepathic_circuit_glow.setRotationPoint(-5.8F, -1.4F, 16.5F);
        glow.addChild(telepathic_circuit_glow);
        setRotationAngle(telepathic_circuit_glow, 0.0F, -1.5708F, 0.0F);


        crystal2 = new LightModelRenderer(this);
        crystal2.setRotationPoint(-4.0F, 0.5518F, 20.9068F);
        telepathic_circuit_glow.addChild(crystal2);
        setRotationAngle(crystal2, -0.4363F, 0.0F, 0.0F);
        crystal2.setTextureOffset(109, 109).addBox(-0.4F, -2.2253F, -0.5453F, 1.0F, 3.0F, 1.0F, 0.0F, false);
        crystal2.setTextureOffset(109, 109).addBox(-0.4F, -2.2253F, -0.1453F, 1.0F, 3.0F, 1.0F, -0.2F, false);
        crystal2.setTextureOffset(109, 109).addBox(-0.1F, -1.7253F, -0.3453F, 1.0F, 3.0F, 1.0F, -0.2F, false);
        crystal2.setTextureOffset(109, 109).addBox(-0.7F, -1.7253F, -0.7453F, 1.0F, 3.0F, 1.0F, -0.2F, false);
        crystal2.setTextureOffset(109, 109).addBox(-0.4F, -3.0253F, -0.4453F, 1.0F, 3.0F, 1.0F, -0.2F, false);

        lever7 = new LightModelRenderer(this);
        lever7.setRotationPoint(1.0F, -1.4482F, 14.9068F);
        telepathic_circuit_glow.addChild(lever7);
        setRotationAngle(lever7, -0.4363F, 0.0F, 0.0F);
        lever7.setTextureOffset(8, 146).addBox(-2.0F, -1.0095F, 2.0026F, 1.0F, 1.0F, 4.0F, 0.0F, false);
        lever7.setTextureOffset(188, 119).addBox(-8.2F, -0.6892F, 5.2622F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        lever7.setTextureOffset(152, 141).addBox(-8.2F, -0.5498F, 7.0927F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        lever7.setTextureOffset(152, 141).addBox(-10.0F, -0.6892F, 5.2622F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        lever7.setTextureOffset(183, 91).addBox(-11.9F, -0.6892F, 5.2622F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        lever7.setTextureOffset(153, 184).addBox(-10.0F, -0.5498F, 7.0927F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        lever7.setTextureOffset(153, 184).addBox(-11.9F, -0.5498F, 7.0927F, 1.0F, 1.0F, 1.0F, -0.1F, false);

        nope2 = new ModelRenderer(this);
        nope2.setRotationPoint(-4.0124F, -1.8123F, 17.5686F);
        telepathic_circuit_glow.addChild(nope2);
        setRotationAngle(nope2, 0.4363F, 3.1416F, 0.0F);
        nope2.setTextureOffset(106, 112).addBox(-1.5F, -1.5577F, -1.4094F, 3.0F, 3.0F, 3.0F, -0.4F, false);

        block2_glow = new LightModelRenderer(this);
        block2_glow.setRotationPoint(-2.8F, -8.5F, 13.6F);
        glow.addChild(block2_glow);
        setRotationAngle(block2_glow, -0.0524F, -1.0472F, -0.0873F);


        lever9 = new LightModelRenderer(this);
        lever9.setRotationPoint(-2.9869F, -0.7455F, 16.8686F);
        block2_glow.addChild(lever9);
        setRotationAngle(lever9, 0.0F, 0.0F, 0.0349F);
        lever9.setTextureOffset(7, 146).addBox(-1.7935F, -0.3712F, -4.4235F, 2.0F, 2.0F, 6.0F, 0.0F, false);
        lever9.setTextureOffset(7, 146).addBox(-2.7968F, 0.7509F, -1.0227F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        lever9.setTextureOffset(7, 146).addBox(-0.4887F, 2.2419F, 6.3644F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        lever9.setTextureOffset(7, 146).addBox(-2.5976F, 2.2172F, 6.4084F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        lever9.setTextureOffset(174, 140).addBox(-0.4887F, 2.2419F, 7.3644F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        lever9.setTextureOffset(185, 101).addBox(-3.7968F, 0.7509F, -0.0227F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        lever9.setTextureOffset(185, 101).addBox(-1.4887F, 2.2419F, 7.3644F, 1.0F, 1.0F, 1.0F, -0.2F, false);
        lever9.setTextureOffset(187, 119).addBox(-3.5976F, 2.2172F, 7.4084F, 1.0F, 1.0F, 1.0F, -0.2F, false);

        bone45_glow = new LightModelRenderer(this);
        bone45_glow.setRotationPoint(-8.3F, -13.7F, 0.0F);
        glow.addChild(bone45_glow);
        setRotationAngle(bone45_glow, -0.2618F, 0.0F, 0.0F);
        bone45_glow.setTextureOffset(189, 141).addBox(-3.2F, 0.8145F, 21.2285F, 1.0F, 2.0F, 1.0F, -0.2F, false);
    }

    @Override
    public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
        //previously the render function, render code was moved to a method below
    }

    @Override
    public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {

    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void render(CoralConsoleTile tile, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
    	matrixStack.push();
		float pixel = 0.06125F * 2.5F; 
        
		//rotor top
        matrixStack.push();
        matrixStack.translate(0, tile.isInFlight() ? -(float) Math.cos(tile.flightTicks * 0.1) * 0.15F  + pixel : 0.0F, 0);
        this.rotartop.render(matrixStack, buffer, packedLight, packedOverlay);
        matrixStack.pop();
        
        //rotor bottom
        matrixStack.push();
        matrixStack.translate(0, tile.isInFlight() ? (float) Math.cos(tile.flightTicks * 0.1) * 0.15F  - pixel : 0.0F, 0);
        this.rotarbottom.render(matrixStack, buffer, packedLight, packedOverlay);
        matrixStack.pop();
        
        
        tile.getControl(ThrottleControl.class).ifPresent(throttle -> {
        	this.lever3.rotateAngleX = (float) Math.toRadians(100 - (float) 75 * throttle.getAmount() - 75);
        });

        tile.getControl(HandbrakeControl.class).ifPresent(handbrake -> {
        	this.lever4.rotateAngleX = (float)Math.toRadians(handbrake.isFree() ? 45 : -45);
            this.lever5.rotateAngleX = (float)Math.toRadians(handbrake.isFree() ? 45 : -45);
        });

        tile.getControl(IncModControl.class).ifPresent(inc -> {
//        	 lever2.rotateAngleZ = (float)Math.toRadians(inc.getAnimationTicks() * 30);
        	 lever2.rotateAngleZ = -120.5F * (inc.index / (float) IncModControl.COORD_MODS.length);
        });
        
        tile.getControl(LandingTypeControl.class).ifPresent(land -> {
//        	matrixStack.push();
//        	matrixStack.translate(-0.25F * (land.getLandType() == EnumLandType.UP ? 1 : 0), 0, 0);
//        	this.land_type.render(matrixStack, buffer, packedLight, packedOverlay);
//        	matrixStack.pop();
        });
        
        tile.getControl(XControl.class).ifPresent(x -> {
			this.x.setBright(x.getAnimationTicks() != 0 ? 1.0F : 0.0F);
		});
		
		tile.getControl(YControl.class).ifPresent(y -> {
			this.y.setBright(y.getAnimationTicks() != 0 ? 1.0F : 0.0F);
		});
		
		tile.getControl(ZControl.class).ifPresent(z -> {
			this.z.setBright(z.getAnimationTicks() != 0 ? 1.0F : 0.0F);
		});
        
        this.facing_control.rotateAngleY = (float) Math.toRadians(WorldHelper.getAngleFromFacing(tile.getExteriorFacingDirection()));

        tile.getControl(RandomiserControl.class).ifPresent(rand -> {
        	this.randomiser.rotateAngleY = (float) Math.toRadians((rand.getAnimationTicks() / 10.0) * 360);
        });
        
        int glowTime = (int)(tile.getWorld().getGameTime() % 120);
        this.block2_glow.setBright(glowTime > 40 ? 1.0F : 0.0F);
        
        this.keyboard_by_monitor_glow.setBright(glowTime > 60 ? 1.0F : 0.0F);
        this.monitor_screen.setBright(1F);
        this.keyboard_behind_monitor_glow.setBright(glowTime < 60 ? 1.0F : 0.0F);
        this.land_type_glow.setBright(1F);
        this.shell_glow.setBright(1F);
        this.throttle_glow.setBright(1F);
        this.telepathic_circuit_glow.setBright(glowTime < 30 ? 1.0F : 0.0F);
        
        this.rotarbottom.setBright(1F);
        this.rotartop.setBright(1F);
       

        all.render(matrixStack, buffer, packedLight, packedOverlay);
		glow.render(matrixStack, buffer, packedLight, packedOverlay);
		
		
		matrixStack.pop();

    }
}