package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.consoles.KeltConsoleModel;
import net.tardis.mod.controls.MonitorControl;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;

public class KeltConsoleRenderer extends TileEntityRenderer<KeltConsoleTile> {
	
	public static final KeltConsoleModel MODEL = new KeltConsoleModel();
	public static final WorldText WORLD_TEXT = new WorldText(0.22F, 0.15F, 0.002F, 0xFFFFFF);

	public KeltConsoleRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	@Override
	public void render(KeltConsoleTile tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
		matrixStackIn.push();
		matrixStackIn.translate(0.5, 1.8, 0.5);
		
		float scale = 1.2F;
		matrixStackIn.scale(scale, scale, scale);
		matrixStackIn.rotate(Vector3f.ZP.rotationDegrees(180));
		
		ResourceLocation texture = new ResourceLocation(Tardis.MODID, "textures/consoles/kelt.png");
		MODEL.render(tile, 1.2F, matrixStackIn, bufferIn.getBuffer(RenderType.getEntityTranslucent(texture)), combinedLightIn, OverlayTexture.NO_OVERLAY, 1, 1, 1, 1.0F);
		
		matrixStackIn.push();
		matrixStackIn.scale(0.5F, 0.5F, 0.5F);
		matrixStackIn.translate(1, 2, 0.5);
		matrixStackIn.rotate(Vector3f.XN.rotationDegrees(24));
		matrixStackIn.rotate(Vector3f.ZP.rotationDegrees(24));
		matrixStackIn.translate(0.3, -0.25, 0.25);
        Minecraft.getInstance().getItemRenderer().renderItem(tile.getSonicItem(), TransformType.NONE, combinedLightIn, combinedOverlayIn, matrixStackIn, bufferIn);
        matrixStackIn.pop();
        
        //Monitor text
        tile.getControl(MonitorControl.class).ifPresent(monitor -> {
			matrixStackIn.push();
			matrixStackIn.translate(-0.12, 0.555, -0.68);
			WORLD_TEXT.renderText(matrixStackIn, bufferIn, combinedLightIn, Helper.getConsoleText(tile));
			matrixStackIn.pop();
        });
		
		matrixStackIn.pop();
	}
}
