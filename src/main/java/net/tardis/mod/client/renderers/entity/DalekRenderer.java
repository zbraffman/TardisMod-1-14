package net.tardis.mod.client.renderers.entity;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.LivingRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.client.models.entity.dalek.DalekModel;
import net.tardis.mod.client.models.entity.dalek.DalekSpecialWeaponModel;
import net.tardis.mod.client.models.entity.dalek.IDalekModel;
import net.tardis.mod.client.renderers.TRenderTypes;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.entity.hostile.dalek.DalekEntity;
import net.tardis.mod.helper.TRenderHelper;

import java.util.HashMap;

public class DalekRenderer extends LivingRenderer<DalekEntity, EntityModel<DalekEntity>> {

    private static HashMap<ResourceLocation, EntityModel<DalekEntity>> MODELS = new HashMap();
    private static final EntityModel<DalekEntity> dalekModel = new DalekModel();
    private static final EntityModel<DalekEntity> dalekSpecModel = new DalekSpecialWeaponModel();

    public DalekRenderer(EntityRendererManager rendererManager) {
        super(rendererManager, new DalekModel(), 1);
        MODELS.put(Constants.DalekTypes.DALEK_DEFAULT, dalekModel);
        MODELS.put(Constants.DalekTypes.DALEK_RUSTY, dalekModel);
        MODELS.put(Constants.DalekTypes.DALEK_SPEC, dalekSpecModel);
    }

    @Override
    public void render(DalekEntity entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn) {
        entityModel = MODELS.get(entityIn.getDalekType().getRegistryName());
        if (entityModel instanceof IDalekModel) {
            IDalekModel dalekModel = (IDalekModel) entityModel;
            dalekModel.setDalek(entityIn);

            super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);

            if (!entityIn.getBlockPos().equals(BlockPos.ZERO)) {
                for (int j = 0; j < 3; j++) {
                    matrixStackIn.push();
                    Vector3d start = entityIn.getPositionVec().add(0, entityIn.getEyeHeight(), 0);
                    BlockPos attackPos = entityIn.getBlockPos();
                    Vector3d end = new Vector3d(attackPos.getX(), attackPos.getY(), attackPos.getZ());
                /*    dalekModel.getLaser().translateRotate(matrixStackIn);
                    matrixStackIn.translate(0, entityIn.getEyeHeight(),0);*/
                    matrixStackIn.translate(start.x - interpolate(entityIn.lastTickPosX, entityIn.getPosX(), partialTicks), start.y - interpolate(entityIn.lastTickPosY, entityIn.getPosY(), partialTicks), start.z - interpolate(entityIn.lastTickPosZ, entityIn.getPosZ(), partialTicks));

                    faceVec(matrixStackIn, start, end);
                    matrixStackIn.rotate(Vector3f.XP.rotationDegrees(90));

                    TRenderHelper.drawGlowingLine(matrixStackIn.getLast().getMatrix(), bufferIn.getBuffer(TRenderTypes.LASER), (float) start.distanceTo(end), 0.03F, 0, 0, 1, 1F, 15728640);
                    matrixStackIn.pop();
                }
            }
        }

    }

    public static void faceVec(MatrixStack matrixStack, Vector3d src, Vector3d dst) {
        double x = dst.x - src.x;
        double y = dst.y - src.y;
        double z = dst.z - src.z;
        double diff = MathHelper.sqrt(x * x + z * z);
        float yaw = (float) (Math.atan2(z, x) * 180 / Math.PI) - 90;
        float pitch = (float) -(Math.atan2(y, diff) * 180 / Math.PI);

        matrixStack.rotate(Vector3f.YP.rotationDegrees(-yaw));
        matrixStack.rotate(Vector3f.XP.rotationDegrees(pitch));
    }

    public static float interpolate(float f1, float f2, float partial) {
        return f1 + (f2 - f1) * partial;
    }

    public static double interpolate(double d1, double d2, float partial) {
        return d1 + (d2 - d1) * partial;
    }


    @Override
    public ResourceLocation getEntityTexture(DalekEntity entity) {
        ResourceLocation texture = entity.getDalekType().getRegistryName();
        return new ResourceLocation(texture.getNamespace(), "textures/entity/dalek/dalek_" + entity.getTexture() + ".png");
    }
}
