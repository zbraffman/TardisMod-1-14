package net.tardis.mod.client.renderers.exteriors;

import java.util.HashMap;
import java.util.function.Supplier;

import com.google.common.collect.Maps;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.blocks.BrokenExteriorBlock;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.registries.BrokenExteriors;
import net.tardis.mod.tileentities.BrokenExteriorTile;

public class BrokenExteriorRenderer extends TileEntityRenderer<BrokenExteriorTile> {
	
	private static HashMap<BrokenExteriors, BrokenTypeRenderer> RENDERERS = Maps.newHashMap();
	
	public BrokenExteriorRenderer(TileEntityRendererDispatcher dispatcher) {
		super(dispatcher);
	}

	@Override
	public void render(BrokenExteriorTile te, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
		
		matrixStackIn.push();
		matrixStackIn.translate(0.5, -0.5, 0.5);
		matrixStackIn.rotate(Vector3f.ZP.rotationDegrees(180));
		
		if(te.getBlockState() != null && te.getBlockState().hasProperty(BlockStateProperties.HORIZONTAL_FACING))
			matrixStackIn.rotate(Vector3f.YP.rotationDegrees(WorldHelper.getAngleFromFacing(te.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING))));
		
		BrokenTypeRenderer render = RENDERERS.get(te.getBrokenType());
		if(render != null) {
			render.renderer.render(matrixStackIn, bufferIn.getBuffer(RenderType.getEntityCutout(render.texture.get())), combinedLightIn, combinedOverlayIn);
		}

		matrixStackIn.pop();
	}
	
	public static void registerBrokenRenderer(BrokenExteriors ext, BrokenTypeRenderer renderer) {
		RENDERERS.put(ext, renderer);
	}
	
	public static class BrokenTypeRenderer {
		
		public Supplier<ResourceLocation> texture;
		public IRender renderer;
		
		public BrokenTypeRenderer(Supplier<ResourceLocation> texture, IRender render) {
			this.texture = texture;
			this.renderer = render;
		}
	}
	
	public static interface IRender{
		void render(MatrixStack matrix, IVertexBuilder buffer, int combinedLight, int combinedOverlay);
	}
}
