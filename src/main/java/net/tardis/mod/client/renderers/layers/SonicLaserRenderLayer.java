package net.tardis.mod.client.renderers.layers;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.entity.model.PlayerModel;
import net.minecraft.client.settings.PointOfView;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.HandSide;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.tardis.mod.client.renderers.TRenderTypes;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.TRenderHelper;
import net.tardis.mod.items.SonicItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.registries.SonicModeRegistry;
import net.tardis.mod.sonic.capability.SonicCapability;

/**
 * Created by Swirtzly
 * on 25/03/2020 @ 12:59
 */
public class SonicLaserRenderLayer extends LayerRenderer<AbstractClientPlayerEntity, PlayerModel<AbstractClientPlayerEntity>> {

    private final IEntityRenderer<AbstractClientPlayerEntity, PlayerModel<AbstractClientPlayerEntity>> renderPlayer;

    public SonicLaserRenderLayer(IEntityRenderer<AbstractClientPlayerEntity, PlayerModel<AbstractClientPlayerEntity>> entityRendererIn) {
        super(entityRendererIn);
        this.renderPlayer = entityRendererIn;
    }


    public static void onRenderWorldLast(RenderWorldLastEvent event) {
        /*if (Minecraft.getInstance().player == null)
            return;
        MatrixStack matrixStack = event.getMatrixStack();
        PlayerEntity player = Minecraft.getInstance().player;
        if (Minecraft.getInstance().gameSettings.getPointOfView() == PointOfView.FIRST_PERSON && player.isHandActive() && PlayerHelper.isInHand(player.getActiveHand(), player, TItems.SONIC.get())) {
            if (SonicCapability.getForStack(player.getActiveItemStack()).orElse(null).getCharge() > 0) {
                if (SonicItem.getCurrentMode(player.getHeldItem(player.getActiveHand())).getMode() == SonicModeRegistry.LASER_INTERACT.get()) {
                    double distance = player.getPositionVec().add(0, player.getEyeHeight(), 0).distanceTo(Minecraft.getInstance().objectMouseOver.getHitVec());
                    matrixStack.translate(0, -player.getEyeHeight() + 1.5f - (player.isSneaking() ? 0.3 : 0), 0);
                    matrixStack.rotate(Vector3f.YP.rotation(-player.rotationYaw));
                    matrixStack.rotate(Vector3f.XP.rotation(player.rotationPitch));
                    Vector3d start = new Vector3d(-0.1F, 0, 0.3);
                    Vector3d end = start.add(0, 0, distance);
                    IRenderTypeBuffer.Impl buffer = Minecraft.getInstance().getRenderTypeBuffers().getBufferSource();
                    IVertexBuilder vertexBuilder = buffer.getBuffer(TRenderTypes.LASER);
                    TRenderHelper.drawGlowingLine(matrixStack.getLast().getMatrix(), vertexBuilder, (float)start.distanceTo(end), player.world.rand.nextFloat() * 0.2F, 0.93D, 0.61D, 0D, 1F, 1);
                    
                }
            }
        }*/
    }

    @Override
    public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn,
	        AbstractClientPlayerEntity entityIn, float limbSwing, float limbSwingAmount, float partialTicks,
	        float ageInTicks, float netHeadYaw, float headPitch) {
        /*double distance = entityIn.getPositionVec().add(0, entityIn.getEyeHeight(), 0).distanceTo(Minecraft.getInstance().objectMouseOver.getHitVec());
        if (entityIn.isHandActive() && PlayerHelper.isInHand(entityIn.getActiveHand(), entityIn, TItems.SONIC.get())) {
            if (SonicCapability.getForStack(entityIn.getActiveItemStack()).orElse(null).getCharge() > 0) {
                if (SonicItem.getCurrentMode(entityIn.getHeldItem(entityIn.getActiveHand())).getMode() == SonicModeRegistry.LASER_INTERACT.get()) {

                    if (entityIn.getPrimaryHand() == HandSide.RIGHT) {
                        renderPlayer.getEntityModel().bipedRightArm.translateRotate(matrixStackIn);
                    } else {
                        renderPlayer.getEntityModel().bipedLeftArm.translateRotate(matrixStackIn);
                    }
                    matrixStackIn.translate(-0.2, 0.5, 0);
                    Vector3d start = new Vector3d(0.1D, 0, 0);
                    Vector3d end = start.add(0, 0, -distance);
                    IVertexBuilder vertexBuilder = bufferIn.getBuffer(TRenderTypes.LASER);
                    TRenderHelper.drawGlowingLine(matrixStackIn.getLast().getMatrix(), vertexBuilder, (float)start.distanceTo(end), 0.5F, 0.93D, 0.61D, 0D, 1, packedLightIn);
                }
            }
        }*/
    }

}
