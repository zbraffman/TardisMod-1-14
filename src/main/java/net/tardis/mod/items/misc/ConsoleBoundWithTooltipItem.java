package net.tardis.mod.items.misc;

import java.util.List;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.constants.Constants;
/** Base class for non capability holding items
 * <br> Inherit this you don't want to reimplement tooltips for your attunable item*/
public class ConsoleBoundWithTooltipItem extends ConsoleBoundItem{
	
	public ConsoleBoundWithTooltipItem(Properties properties) {
		super(properties);
	}
	
	@Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        if (this.getTardis(stack) != null) {
            tooltip.add(new TranslationTextComponent(Constants.Translations.TOOLTIP_ATTUNED_OWNER).appendSibling(new StringTextComponent(this.getTardisName(stack)).mergeStyle(TextFormatting.LIGHT_PURPLE)));
        }
        else {
        	tooltip.add(Constants.Translations.TOOLTIP_NO_ATTUNED);
        }
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

}
