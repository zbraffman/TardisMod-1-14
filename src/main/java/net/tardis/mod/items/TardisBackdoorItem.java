package net.tardis.mod.items;

import net.minecraft.block.BlockState;
import net.minecraft.block.DoorBlock;
import net.minecraft.item.Item;
import net.minecraft.item.ItemUseContext;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.ActionResultType;
import net.tardis.mod.properties.Prop;

public class TardisBackdoorItem extends Item {

    public TardisBackdoorItem(){
        super(Prop.Items.ONE.get());
    }

    @Override
    public ActionResultType onItemUse(ItemUseContext context) {

        BlockState doorState = context.getWorld().getBlockState(context.getPos());

        if(context.getWorld().getBlockState(context.getPos()).getBlock().isIn(BlockTags.DOORS)){

            if(doorState.hasProperty(DoorBlock.HALF)){

            }

            context.getItem().shrink(1);
            return ActionResultType.SUCCESS;
        }

        return ActionResultType.PASS;
    }
}
