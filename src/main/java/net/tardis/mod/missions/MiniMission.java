package net.tardis.mod.missions;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import javax.annotation.Nullable;

import com.google.common.collect.Lists;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.server.CustomServerBossInfo;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Util;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.BossInfo;
import net.minecraft.world.BossInfo.Color;
import net.minecraft.world.BossInfo.Overlay;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerBossInfo;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.Tardis;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.experimental.advancement.GenericTardisTrigger;
import net.tardis.mod.experimental.advancement.TTriggers;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.missions.misc.Dialog;
import net.tardis.mod.missions.misc.DialogOption;
import net.tardis.mod.missions.stages.MissionStage;
import net.tardis.mod.missions.stages.MissionStages;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.MissionUpdateMessage;

public abstract class MiniMission implements INBTSerializable<CompoundNBT> {

    private World world;
    private BlockPos pos;
    private int range;
    private MissionStage currentStage;
    private Map<ResourceLocation, MissionStage> stages = new HashMap<>();
    private boolean awarded = false;
    private MiniMissionType type;
    private IFormattableTextComponent barName;
    protected final IFormattableTextComponent originalBarName;
    private CustomServerBossInfo bossBar;
    private List<UUID> trackingPlayers = Lists.newArrayList();
	private List<Dialog> dialogs = Lists.newArrayList(); //Used to allow for language generators to generate lang file contents easily
	private List<DialogOption> dialogOptions = Lists.newArrayList();
    private AxisAlignedBB missionBox = null;
    private GenericTardisTrigger completedAdvancementTrigger = TTriggers.DUMMY;
    private EntityType<?> hostEntity;

    public MiniMission(MiniMissionType type, World world, BlockPos pos, int range) {
        this.world = world;
        this.pos = pos;
        this.range = range;
        this.type = type;
        this.originalBarName = new TranslationTextComponent(Util.makeTranslationKey("missions", type.getRegistryName()));
        this.barName = this.originalBarName;
        this.bossBar = new CustomServerBossInfo(Helper.createRL("mini_mission"), this.getMissionName());
        this.bossBar.setColor(Color.WHITE);
        this.bossBar.setOverlay(Overlay.PROGRESS);
        this.bossBar.setPercent(0);
        this.hostEntity = TEntities.SHIP_CAPTAIN.get();
        this.registerStages();
    }

    public MiniMission(MiniMissionType type, World world, CompoundNBT tag) {
        this(type, world, BlockPos.ZERO, 0);
        this.deserializeNBT(tag);
    }
    
    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();

        tag.putLong("pos", this.pos.toLong());
        tag.putInt("range", this.range);
        tag.putString("current_stage", this.currentStage.getRegistryName().toString());
        tag.putBoolean("awarded", this.awarded);
        ListNBT stages = new ListNBT();
        for (Entry<ResourceLocation, MissionStage> stage : this.stages.entrySet()) {
            CompoundNBT stageTag = stage.getValue().serializeNBT();
            stages.add(stageTag);
        }
        tag.put("stages", stages);

        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT tag) {
        this.pos = BlockPos.fromLong(tag.getLong("pos"));
        this.range = tag.getInt("range");
        this.awarded = tag.getBoolean("awarded");
        ListNBT stages = tag.getList("stages", NBT.TAG_COMPOUND);
        for (INBT nbt : stages) {
            CompoundNBT comp = (CompoundNBT)nbt;
            MissionStage stage = new MissionStage();
            stage.deserializeNBT(comp);
            if (stage != null) {
            	this.stages.put(stage.getRegistryName(), stage);
            }
        }
        this.currentStage = this.stages.get(new ResourceLocation(tag.getString("current_stage")));
    }
    
    public void registerStages() {
    	this.registerStage(MissionStages.NOT_STARTED.get());
		this.registerStage(MissionStages.RETURN_TO_MISSION_HOST.get());
		this.registerStage(MissionStages.MISSION_COMPLETED.get());
    }
    
    public void registerStage(MissionStage stage) {
    	this.stages.put(stage.getRegistryName(), stage);
    }
    
    /** Get current stage's objective*/
    public int getCurrentObjective() {
        return this.currentStage.getCurrentObjectiveIndex();
    }
    /** Sets the objective index for the current stage*/
    public void setObjective(int objective) {
        this.setObjectiveForStage(this.currentStage.getRegistryName(), objective);
    }
    
    public void setObjectiveForStage(ResourceLocation stageKey, int objective) {
    	MissionStage stage = stages.get(stageKey);
    	stage.setObjectiveIndex(objective);
    	if (stage.isComplete()) {
    		this.setCurrentStage(this.getNextStage().resetObjectiveIndex());
    	}
    	if (this.bossBar != null)
            this.getBossBar().setPercent(this.getProgressBarPercent());
        this.update();
    }

    public void advanceObjective() {
        this.setObjective(this.currentStage.getCurrentObjectiveIndex() + 1);
    }
    
    public int getMaxObjectivesForCurrentStage() {
    	return this.currentStage.getMaxObjectives();
    }
    
    public int getMaxObjectivesForStage(ResourceLocation stageID) {
    	return this.stages.get(stageID).getMaxObjectives();
    }
    
    public int getTotalObjectives() {
    	int max = 0;
    	for (MissionStage stage : stages.values()) {
    		max += stage.getMaxObjectives();
    	}
    	return max;
    }
    
    public MissionStage getCurrentStage() {
    	return this.currentStage;
    }
    
    public void setCurrentStage(MissionStage stage) {
    	this.currentStage = stage;
    	this.update();
    }
    
    public abstract MissionStage getNextStage();
    
    public Map<ResourceLocation, MissionStage> getStages(){
    	return this.stages;
    }
    
    public MissionStage getStageByKey(ResourceLocation id) {
    	return this.stages.get(id);
    }
    
    public int getTotalNumStages() {
    	return this.stages.size();
    }
    
    public int getNumStagesCompleted() {
    	int numStagesComplete = 0;
    	for (MissionStage stage : stages.values()) {
    		if (stage.isComplete()) {
    			numStagesComplete++;
    		}
    	}
    	return numStagesComplete;
    }
    
    public int getMaxStage() {
    	return this.stages.size();
    }

    public abstract float getProgressBarPercent();
    /** Get the dialog options for the Dialog GUI. <p> Must be on the logical Client-Side*/
    public abstract Dialog getDialogForObjective(LivingEntity speaker, PlayerEntity player, int stage);
    
    /** If this mission has all stages completed*/
    public boolean isMissionComplete() {
        return getNumStagesCompleted() == stages.size();
    }
    
    public IFormattableTextComponent getMissionName() {
        return this.barName;
    }
    
    public void setMissionName(IFormattableTextComponent newName) {
    	this.barName = newName;
    	this.bossBar.setName(barName);
    }

    public int getRange() {
        return this.range;
    }

    public BlockPos getPos() {
        return this.pos;
    }

    public World getWorld() {
        return this.world;
    }
    
    public abstract ItemStack getReward();

    public boolean getAwarded() {
        return this.awarded;
    }
    
    public void setAwarded(boolean awarded) {
        this.awarded = awarded;
        this.update();
    }

    public void onPlayerEnterMissionArea(PlayerEntity player) {
        if (player instanceof ServerPlayerEntity) {
            this.bossBar.addPlayer((ServerPlayerEntity) player);
        }
    }

    public void onPlayerLeaveMissionArea(PlayerEntity player) {
        if (player instanceof ServerPlayerEntity) {
            this.bossBar.removePlayer((ServerPlayerEntity) player);
        }
    }

    public MiniMissionType getType() {
        return this.type;
    }

    public ServerBossInfo getBossBar() {
        return this.bossBar;
    }
    /** Set the boss bar's colour*/
    public void setBossBarColour(BossInfo.Color color) {
    	this.bossBar.setColor(color);
    }

    /** Set the boss bar's overlay style type E.g. Notched or progress*/
    public void setBossBarStyle(BossInfo.Overlay styleType) {
    	this.bossBar.setOverlay(styleType);
    }

    public boolean isInsideArea(Entity ent) {
    	if(world.getDimensionKey() != ent.world.getDimensionKey())
			return false;
        return this.pos.withinDistance(ent.getPosition(), range);
    }

    public List<UUID> getTrackingPlayers() {
        return Collections.unmodifiableList(this.trackingPlayers);
    }

    public void addTrackingPlayer(ServerPlayerEntity player) {
        if (!this.trackingPlayers.contains(player.getUniqueID())) {
            this.trackingPlayers.add(player.getUniqueID());
            this.bossBar.addPlayer(player);
        }
    }

    public void removeTrackingPlayer(ServerPlayerEntity player) {
        if (this.trackingPlayers.contains(player.getUniqueID())) {
            this.trackingPlayers.remove(player.getUniqueID());
            this.bossBar.removePlayer(player);
        }
    }

    public AxisAlignedBB getMissionBB() {
        if (this.missionBox == null)
            this.missionBox = new AxisAlignedBB(this.pos).grow(this.range);
        return this.missionBox;
    }
    /** Get the Advancement Criterion Trigger that will be used to unlock an advancement when mission is complete*/
    public GenericTardisTrigger getCompletedAdvancementTrigger() {
    	return this.completedAdvancementTrigger;
    }
    
    public EntityType<?> getMissionHostEntity(){
    	return this.hostEntity;
    }

    public void update() {
        if (!world.isRemote)
            Network.sendToAllInWorld(new MissionUpdateMessage(this), (ServerWorld) world);
    }

	//========= Character Dialog Helpers ===========
    /** Helper methods to make dialog creation easy */
    public Dialog createDialogForCharacter(String translationSuffixKey) {
        Dialog dialog = new Dialog(createDialogTranslationKey(translationSuffixKey));
        dialogs.add(dialog);
        return dialog;
    }
    
    public String createDialogTranslationKey(String translationSuffixKey) {
        return "mission." + Tardis.MODID + "." + this.getType().getRegistryName().getPath().toString() + ".dialog.character." + translationSuffixKey;
    }

    public TranslationTextComponent createDialogTranslationKey(String modId, String translationSuffixKey) {
        return new TranslationTextComponent("mission." + modId + "." + this.getType().getRegistryName().getPath().toString() +  ".dialog.character." + translationSuffixKey);
    }
    
    
	
    //========= Player Dialog Option Helpers =========
    
    public DialogOption createPlayerResponse(@Nullable Dialog dialog, TranslationTextComponent responseKey) {
        return new DialogOption(dialog, responseKey);
    }
    
    /**
     * @param dialog
     * @param response - the translation key that will be appended to the default translation key
     * @return
     */
    public DialogOption createPlayerResponse(@Nullable Dialog dialog, String responseKey) {
        DialogOption dialogOption = new DialogOption(dialog, new TranslationTextComponent(createPlayerResponseTranslationKey(responseKey)));
        dialogOptions.add(dialogOption);
        return dialogOption;
    }
    
    public DialogOption createPlayerResponseWithArguments(@Nullable Dialog dialog, String responseKey, String args) {
        DialogOption dialogOption = new DialogOption(dialog, new TranslationTextComponent(createPlayerResponseTranslationKey(responseKey), args));
        dialogOptions.add(dialogOption);
        return dialogOption;
    }
	
	/** Dialog Option Helpers*/
	
	
    public String createPlayerResponseTranslationKey(String translationSuffixKey) {
        return "mission." + Tardis.MODID + "." + this.getType().getRegistryName().getPath().toString() + ".dialog_option.player." + translationSuffixKey;
    }
    
    
    public String createDialogOptionTranslationKey(String translationSuffixKey) {
        return "mission." + Tardis.MODID + "." + this.getType().getRegistryName().getPath().toString() + ".dialog_option.player" + translationSuffixKey;
    }
    
    public TranslationTextComponent createDialogOptionTranslationKey(String modId, String translationSuffixKey) {
        return new TranslationTextComponent("mission." + modId + "." + this.getType().getRegistryName().getPath().toString() + ".dialog_option.player" + translationSuffixKey);
    }
}
