package net.tardis.mod.tileentities;


import javax.annotation.Nullable;

import net.minecraft.block.BlockState;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.FluidTags;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler.FluidAction;
import net.minecraftforge.fluids.capability.templates.FluidTank;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.items.TItems;
import net.tardis.mod.recipe.AlembicRecipe;

public class AlembicTile extends TileEntity implements ITickableTileEntity{

	public static final int MAX_TIME = 200;
	private ItemStackHandler handler = new ItemStackHandler(6);
	private FluidTank waterTank = new FluidTank(1000, stack -> stack.getFluid() == Fluids.WATER);
	private int progress = 0;
	private int burnTime = 0;
	private int maxBurnTime = 1;
	private int mercury = 0;
	
	private AlembicRecipe recipe;
	
	public AlembicTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public AlembicTile() {
		super(TTiles.ALEMBIC.get());
	}

	@Override
	public void tick() {
		
		if(this.burnTime > 0) {
			
			
			//Reset progress if recipe has changed
			AlembicRecipe newRecipe = this.getRecipe();
			if(newRecipe != recipe)
				this.progress = 0;
			this.recipe = newRecipe;
			
			//If can process this recipe
			if(this.shouldAdvanceProgress()) {
				--this.burnTime;
				//Recipe work
				++this.progress;
				world.setBlockState(pos, this.getBlockState().with(BlockStateProperties.ENABLED, true), 2);
				if(this.progress >= 200) {
					this.handleCraftComplete();
				}
			}
			else {
				this.progress = 0;
			}
		}
		this.handleFuelSlot();
		this.handleWaterSlots();
		this.handleMercuryBottleSlots();
		
	}
	
	private void handleFuelSlot() {
		//Handle fuel items
		int fuelBurnTime = ForgeHooks.getBurnTime(this.handler.getStackInSlot(3));
		if(this.burnTime <= 0 && fuelBurnTime > 0 && !this.handler.getStackInSlot(2).isEmpty()) {
			this.burnTime = this.maxBurnTime = fuelBurnTime;
			this.handler.extractItem(3, 1, false);
			this.markDirty();
		}
	}
	
	private void handleWaterSlots() {
		//Handle tank filling
		this.handler.getStackInSlot(0).getCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY).ifPresent(cap -> {
			//If a water holder is in the top tank slot
			if(cap.getFluidInTank(0).getFluid().isIn(FluidTags.WATER)) {
				//If it can drain from it
				FluidStack drainResult = cap.drain(new FluidStack(Fluids.WATER, this.waterTank.getSpace()), FluidAction.EXECUTE); 
				if(!drainResult.isEmpty()) {
					this.waterTank.fill(drainResult, FluidAction.EXECUTE);
					//Move the container to the bottom slot and decrement the top one
					this.handler.getStackInSlot(0).shrink(1);
					this.handler.insertItem(1, cap.getContainer(), false);
					this.markDirty();
				}
			}
		});
	}
	
	private void handleMercuryBottleSlots() {
		if(this.handler.getStackInSlot(4).getItem() == Items.GLASS_BOTTLE) {
			if(this.mercury >= 500) {
				if(this.handler.insertItem(5, new ItemStack(TItems.MERCURY_BOTTLE.get()), false).isEmpty()) {
					this.handler.getStackInSlot(4).shrink(1);
					this.mercury -= 500;
					this.markDirty();
				}
			}
		}
	}
	
	private void handleCraftComplete() {
		//Finish
		this.progress = 0;
		//Remove the ingredients from the slot
		this.handler.getStackInSlot(2).shrink(recipe.getRequiredIngredientCount());
		this.waterTank.drain(recipe.getRequiredWater(), FluidAction.EXECUTE);
		
		recipe.onCraft(this);
		world.playSound(null, getPos(), SoundEvents.BLOCK_BREWING_STAND_BREW, SoundCategory.AMBIENT, 0.25F, 1F);
		world.setBlockState(pos, this.getBlockState().with(BlockStateProperties.ENABLED, false), 2);
		this.markDirty();
	}
	
	@Nullable
	private AlembicRecipe getRecipe() {
		for(AlembicRecipe rec : AlembicRecipe.getAllRecipes(world)) {
			if(rec.matches(this))
				return rec;
		}
		return null;
	}
	
	public boolean shouldAdvanceProgress() {
		//If there is no recipe, cannot continue
		if(recipe == null)
			return false;
		
		//if output can't be stacked, cannot continue
		if(!this.handler.insertItem(5, recipe.getResult(), true).isEmpty())
			return false;
		
		//If recipe doesn't match
		if(!recipe.matches(this))
			return false;
		
		return true;
	}
	
	public FluidTank getWaterTank() {
		return this.waterTank;
	}
	
	public int getMercury() {
		return this.mercury;
	}
	
	public int getBurnTime() {
		return this.burnTime;
	}
	
	public int getMaxBurnTime() {
		return this.maxBurnTime;
	}
	
	public float getPercent() {
		return this.progress / 200.0F;
	}
	
	public static boolean isFuel(ItemStack stack) {
	      return ForgeHooks.getBurnTime(stack) > 0;
	   }
	
	@Override
	public void read(BlockState state, CompoundNBT compound) {
		super.read(state, compound);
		this.burnTime = compound.getInt("burn_time");
		this.maxBurnTime = compound.getInt("max_burn_time");
		this.progress = compound.getInt("progress");
		this.mercury = compound.getInt("mercury");
		this.waterTank.readFromNBT(compound.getCompound("water_tank"));
		this.handler.deserializeNBT(compound.getCompound("inv_handler"));
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		compound.putInt("burn_time", this.burnTime);
		compound.putInt("max_burn_time", this.maxBurnTime);
		compound.putInt("progress", this.progress);
		compound.putInt("mercury", this.mercury);
		compound.put("water_tank", this.waterTank.writeToNBT(new CompoundNBT()));
		compound.put("inv_handler", this.handler.serializeNBT());
		return super.write(compound);
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(this.getPos(), -1, this.serializeNBT());
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		this.deserializeNBT(pkt.getNbtCompound());
	}

	@Override
	public void handleUpdateTag(BlockState state, CompoundNBT tag) {
		super.handleUpdateTag(state, tag);

		this.deserializeNBT(tag);
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.serializeNBT();
	}
	
	public ItemStackHandler getItemStackHandler() {
		return this.handler;
	}

	public void setMercury(int mercury) {
		this.mercury = MathHelper.clamp(this.mercury + mercury, 0, 1000);
	}
	
}
