package net.tardis.mod.tileentities.machines;

import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.IAffectTARDISLanding;
import net.tardis.mod.tileentities.TTiles;

public class LandingPadTile extends TileEntity implements IAffectTARDISLanding {

	
	public LandingPadTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public LandingPadTile() {
		super(TTiles.LANDING_PAD.get());
	}

	public boolean getOccupied() {
		return !world.getBlockState(this.getPos().up(2)).isAir(world, pos.up(2));
	}

	@Override
	public SpaceTimeCoord affectTARDIS(ServerWorld world, SpaceTimeCoord currentLanding, ConsoleTile console) {
		if(!this.getOccupied()){
			return new SpaceTimeCoord(world.getDimensionKey(), this.getPos().up(), this.getFacing());
		}
		return null;
	}

	@Override
	public int getEffectiveRange() {
		return 16;
	}

	public Direction getFacing(){
		if(this.getBlockState().hasProperty(BlockStateProperties.HORIZONTAL_FACING))
			return this.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING);
		return Direction.NORTH;
	}
}
