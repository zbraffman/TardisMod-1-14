package net.tardis.mod.containers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.helper.TInventoryHelper;
import net.tardis.mod.tileentities.WaypointBankTile;

public class WaypointBankContainer extends Container{

	private WaypointBankTile tile;
	
	protected WaypointBankContainer(ContainerType<?> type, int id) {
		super(type, id);
	}
	
	/** Server Only constructor */
	public WaypointBankContainer(int id, PlayerInventory player, WaypointBankTile tile) {
		this(TContainers.WAYPOINT.get(), id);
		init(player, tile);
	}
	
	/** Client Only constructor */
	public WaypointBankContainer(int id, PlayerInventory player, PacketBuffer buf) {
		this(TContainers.WAYPOINT.get(), id);
		TileEntity te = player.player.world.getTileEntity(buf.readBlockPos());
		if(te instanceof WaypointBankTile)
			init(player, (WaypointBankTile)te);
	}
	
	//Common 
	public void init(PlayerInventory inv, WaypointBankTile waypoint) {
		this.tile = waypoint;
		
		this.addSlot(new SlotItemHandler(waypoint.getItemHandler(), 0, 79, 74));
		
		for(Slot s : TInventoryHelper.createPlayerInv(inv, 0, 26))
			this.addSlot(s);
	}

	@Override
	public boolean canInteractWith(PlayerEntity playerIn) {
		return true;
	}

	public WaypointBankTile getTile() {
		return this.tile;
	}

}
