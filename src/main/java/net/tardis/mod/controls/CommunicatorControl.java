package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.vector.Vector3d;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.constants.Constants.Gui;
import net.tardis.mod.contexts.gui.ConsoleContext;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.misc.ITickable;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.TardisNameGuiMessage;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.subsystem.AntennaSubsystem;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnellConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class CommunicatorControl extends BaseControl implements ITickable{
	
	//private static String TRANS = "status.tardis.communicator.located";

	public CommunicatorControl(ControlEntry entry, ConsoleTile console, ControlEntity entity) {
		super(entry, console, entity);
		if(this.getConsole() != null)
			this.getConsole().registerTicker(this);
	}

	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return EntitySize.flexible(0.3F, 0.3F);
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.1625F, 0.1625F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.1875F, 0.1875F);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile || this.getConsole() instanceof XionConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return EntitySize.flexible(0.25F, 0.25F);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return EntitySize.flexible(0.25F, 0.25F);

		return EntitySize.flexible(0.4F, 0.4F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		
		if(!console.getWorld().isRemote)
			this.startAnimation();
		
		if(!console.getDistressSignals().isEmpty()){
			if (console.getWorld().isRemote) {
				if(this.usePhoneSounds(console)){
					ClientHelper.shutTheFuckUp(TSounds.COMMUNICATOR_RING.get(), SoundCategory.BLOCKS);
					console.getWorld().playSound(player, console.getPos(), TSounds.COMMUNICATOR_PHONE_PICKUP.get(), SoundCategory.BLOCKS, 1F, 1F);
				}
				ClientHelper.openGUI(Constants.Gui.COMMUNICATOR, null);
			}
		}
		
		if(console.getDistressSignals().isEmpty()) {
			if(console.getWorld().isRemote)
				ClientHelper.openGUI(Gui.TARDIS_DISTRESS, new ConsoleContext(console));
			else if (!console.getWorld().isRemote)
				Network.sendTo(TardisNameGuiMessage.create(player.getServer()), (ServerPlayerEntity)player);
		}
		
		return true;
	}

	@Override
	public Vector3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vector3d(6 / 16.0, 12 / 16.0, -4 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vector3d(-0.5577870538580397, 0.33124999701976776, -0.6428223124704939);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vector3d(-0.70, 0.375, -0.1);
		}
		if(this.getConsole() instanceof HartnellConsoleTile)
			return new Vector3d(0.617, 0.469, -0.171);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vector3d(-0.588, 0.375, 0.334);
		
		if(this.getConsole() instanceof XionConsoleTile)
			return new Vector3d(-0.725851740394144, 0.3, -0.5289371401696679);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vector3d(-0.6431637508770778, 0.46875, -0.36798860366348973);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return new Vector3d(-0.7746081391896208, 0.46875, 0.4170838642472918);
		
		return new Vector3d(0, 8 / 16.0, -6 / 16.0);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return null;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return console instanceof NemoConsoleTile ? SoundEvents.BLOCK_BELL_USE : TSounds.COMMUNICATOR_STEAM.get();
	}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {}

	@Override
	public void tick(ConsoleTile console) {
		
		if(console == null || console.getWorld() == null)
			return;
		
		if(!console.getWorld().isRemote && !console.getDistressSignals().isEmpty() && console.getWorld().getGameTime() % 100 == 0) {

			if(this.usePhoneSounds(console)){
				console.getWorld().playSound(null, console.getPos(), TSounds.COMMUNICATOR_RING.get(), SoundCategory.BLOCKS, 1F, 1F);
			} else {
				console.getWorld().playSound(null, console.getPos(), TSounds.COMMUNICATOR_BEEP.get(), SoundCategory.BLOCKS, 1F, 1F);
			}

			console.getSubsystem(AntennaSubsystem.class).ifPresent(sys -> {
				if(!sys.canBeUsed())
					console.getDistressSignals().clear();
			});
		}
	}
	
	public boolean usePhoneSounds(ConsoleTile tile) {
		return this.getConsole() instanceof CoralConsoleTile || this.getConsole() instanceof XionConsoleTile || this.getConsole() instanceof ToyotaConsoleTile;
	}

}
