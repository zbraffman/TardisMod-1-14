package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction.Axis;
import net.minecraft.util.math.vector.Vector3d;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnellConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class YControl extends AxisControl{
	
	public YControl(ControlEntry entry, ConsoleTile console, ControlEntity entity) {
		super(entry, console, entity, Axis.Y);
	}

	@Override
	public EntitySize getSize() {

		if (this.getConsole() instanceof ToyotaConsoleTile){
			return EntitySize.flexible(0.0625F, 0.0625F);
		}
		if(getConsole() instanceof CoralConsoleTile)
			return EntitySize.flexible(0.0625F, 0.0625F);
		
		return EntitySize.flexible(0.0625F, 0.0625F);
	}

	@Override
	public Vector3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vector3d(-6.5 / 16.0, 10.5 / 16.0, 4 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vector3d(-0.4568998228936493, 0.625, 0.2641350176457876);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vector3d(0.3180649539937758, 0.5, -0.65);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return new Vector3d(-0.575, 0.5, 0.34);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vector3d(0.01, 0.6, 0.508);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return new Vector3d(0.6938372031211344, 0.4, 0.39478559304915184);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vector3d(0.7204998497509241, 0.5, 0.6790756118658776);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return new Vector3d(0.8001080830727285, 0.4375, -0.4954128603912813);
		
		return new Vector3d(-8.1 / 16.0, 9.85 / 16.0, -4.75 / 16.0);
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}

}
