package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;
import java.util.function.Supplier;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.recipe.SpectrometerRecipe;
import net.tardis.mod.schematics.Schematic;
import net.tardis.mod.schematics.Schematics;

public class SpectrometerRecipeGen implements IDataProvider{
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    private final DataGenerator generator;
    
    public SpectrometerRecipeGen(DataGenerator generator) {
        this.generator = generator;
    }
    
    @Override
    public void act(DirectoryCache cache) throws IOException {

        final Path path = this.generator.getOutputFolder();
        //Register core interiors for data gen
        if (ConsoleRoom.getRegistry().isEmpty())
            ConsoleRoom.registerCoreConsoleRooms();

        //TODO: Make recipes for all consoles, exteriors, interiors
//        //Consoles
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Consoles.NEMO);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Consoles.STEAM);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Consoles.GALVANIC);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Consoles.CORAL);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Consoles.HARTNELL);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Consoles.TOYOTA);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Consoles.XION);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Consoles.NEUTRON);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Consoles.POLYMEDICAL);
//        
//        //Exteriors
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Exteriors.STEAMPUNK);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Exteriors.TRUNK);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Exteriors.TELEPHONE);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Exteriors.FORTUNE);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Exteriors.SAFE);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Exteriors.TT_CAPSULE);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Exteriors.CLOCK);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Exteriors.TT_2020);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Exteriors.JAPAN);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Exteriors.APERTURE);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Exteriors.POLICE_BOX);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Exteriors.POLICE_BOX_MODERN);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Exteriors.DISGUISE);
        
        //Interiors
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.BLAST_FURNACE), () -> Schematics.Interiors.STEAM);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.ACACIA_LOG), () -> Schematics.Interiors.JADE);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.NAUTILUS_SHELL), () -> Schematics.Interiors.NAUTILUS);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.QUARTZ_PILLAR), () -> Schematics.Interiors.OMEGA);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.POLISHED_DIORITE), () -> Schematics.Interiors.ALABASTER);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Interiors.ARCHITECT);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Interiors.CORAL);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Interiors.PANAMAX);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Interiors.TOYOTA);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Interiors.TRAVELER);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.EMERALD), () -> Schematics.Interiors.ENVOY);
//        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Interiors.AMETHYST);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.POLISHED_BLACKSTONE), () -> Schematics.Interiors.IMPERIAL);
    }

    @Override
    public String getName() {
        return "TARDIS Spectrometer Recipe Generator";
    }
    
    public void createSpectrometerRecipe(Path path, DirectoryCache cache, int progressTicks, Ingredient ingredient, Supplier<Schematic> output) throws IOException {
        IDataProvider.save(GSON, cache, this.createRecipe(progressTicks, ingredient, output.get()), getPath(path, output.get()));
    }
    
    public static Path getPath(Path path, Schematic output) {
        ResourceLocation key = output.getId();
        return path.resolve("data/" + key.getNamespace() + "/recipes/spectrometer/" + key.getPath() + ".json");
    }
    
    public JsonObject createRecipe(int processTicks, Ingredient ingredient, Schematic output) {
    	SpectrometerRecipe recipe = new SpectrometerRecipe(processTicks, ingredient, output.getId());
    	JsonObject recipeJson = recipe.serialise();
        return recipeJson;
    }
}