package net.tardis.mod.datagen;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;

import java.io.IOException;
import java.nio.file.Path;

public abstract class BaseDataProvider implements IDataProvider {

    private DataGenerator gen;
    private String name;

    public BaseDataProvider(String name, DataGenerator gen){
        this.gen = gen;
    }

    @Override
    public void act(DirectoryCache cache) throws IOException {
        this.act(cache, this.getBasePath());
    }

    public abstract void act(DirectoryCache cache, Path base) throws IOException;

    public Path getBasePath(){
        return this.gen.getOutputFolder();
    }

    @Override
    public String getName() {
        return name;
    }
}
