package net.tardis.mod.protocols;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ConsoleUpdateMessage;
import net.tardis.mod.network.packets.ConsoleUpdateMessage.DataTypes;
import net.tardis.mod.network.packets.console.AntigravsData;
import net.tardis.mod.tileentities.ConsoleTile;

public class AntiGravProtocol extends Protocol {

	public static final String TRANS = "message.tardis.anti_gravs.";
	public static final TranslationTextComponent TRANS_ON = new TranslationTextComponent("protocol.tardis.antigrav_on");
	public static final TranslationTextComponent TRANS_OFF = new TranslationTextComponent("protocol.tardis.antigrav_off");

	@Override
	public void call(World world, PlayerEntity playerIn, ConsoleTile console) {
		if(!world.isRemote) {
			console.setAntiGrav(!console.getAntiGrav());
			for(PlayerEntity player : world.getEntitiesWithinAABB(PlayerEntity.class, new AxisAlignedBB(console.getPos()).grow(16))) {
				player.sendStatusMessage(new TranslationTextComponent(TRANS + console.getAntiGrav()), true);
			}
			Network.sendToAllAround(new ConsoleUpdateMessage(DataTypes.ANTIGRAVS, new AntigravsData(console.getAntiGrav())), playerIn.world.getDimensionKey(), playerIn.getPosition(), 64);
		}
		else ClientHelper.openGUI(Constants.Gui.NONE, new GuiContext());
	}

	@Override
	public TranslationTextComponent getDisplayName(ConsoleTile tile) {
		return tile.getAntiGrav() ? TRANS_ON : TRANS_OFF;
	}

	@Override
	public String getSubmenu() {
		return Constants.Strings.EXTERIOR_PROPERTIES;
	}
}
