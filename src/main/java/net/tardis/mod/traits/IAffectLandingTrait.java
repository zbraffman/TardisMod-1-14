package net.tardis.mod.traits;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public interface IAffectLandingTrait {

    /**
     * Return pos if it should not effect
     * @param world
     * @param pos
     * @return
     */
    BlockPos redirect(World world, BlockPos pos);
}
