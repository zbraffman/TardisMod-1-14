package net.tardis.mod.registries;

import java.util.List;
import java.util.function.Supplier;

import com.google.common.collect.Lists;

import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.sonic.AbstractSonicMode;
import net.tardis.mod.sonic.interactions.SonicBlockInteraction;
import net.tardis.mod.sonic.interactions.SonicEntityInteraction;
import net.tardis.mod.sonic.interactions.SonicLaserInteraction;
import net.tardis.mod.sonic.interactions.SonicTardisDestinationInteraction;

/**
 * Created by Swirtzly
 * on 22/08/2019 @ 19:50
 */

public class SonicModeRegistry {
        
    public static final DeferredRegister<SonicModeEntry> SONIC_MODES = DeferredRegister.create(SonicModeEntry.class, Tardis.MODID);
    public static Supplier<IForgeRegistry<SonicModeEntry>> SONIC_MODE_REGISTRY = SONIC_MODES.makeRegistry("sonic_mode", () -> new RegistryBuilder<SonicModeEntry>().setMaxID(Integer.MAX_VALUE - 1));
    
    public static final RegistryObject<SonicModeEntry> BLOCK_INTERACT = SONIC_MODES.register("block_interaction", () -> SonicModeEntry.create(SonicBlockInteraction::new));
    public static final RegistryObject<SonicModeEntry> ENTITY_INTERACT = SONIC_MODES.register("entity_interaction", () -> SonicModeEntry.create(SonicEntityInteraction::new));
//    public static final RegistryObject<SonicModeEntry> LASER_INTERACT = SONIC_MODES.register("laser_interaction", () -> SonicModeEntry.create(SonicLaserInteraction::new));
    public static final RegistryObject<SonicModeEntry> SET_DESTINATION = SONIC_MODES.register("set_destination", () -> SonicModeEntry.create(SonicTardisDestinationInteraction::new));
    
    
    public static AbstractSonicMode getSonicModeEntry(int index) {
    	List<SonicModeEntry> modes = Lists.newArrayList(SONIC_MODE_REGISTRY.get().getValues());
    	return modes.get(index).getSonicType();
    }
    
    public static class SonicModeEntry extends ForgeRegistryEntry<SonicModeEntry> {
        private ISonicModeFactory<AbstractSonicMode> factory;

        public SonicModeEntry(ISonicModeFactory<AbstractSonicMode> factory) {
            this.factory = factory;
        }
        
        public static SonicModeEntry create(ISonicModeFactory<AbstractSonicMode> factory) {
        	return new SonicModeEntry(factory);
        }

        public AbstractSonicMode getSonicType() {
        	AbstractSonicMode mode = factory.create(this);
            return mode;
        }
    }
    
    public interface ISonicModeFactory<T extends AbstractSonicMode> {
    	T create(SonicModeEntry entry);
    }


}
