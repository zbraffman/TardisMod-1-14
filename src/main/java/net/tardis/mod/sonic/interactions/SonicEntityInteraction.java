package net.tardis.mod.sonic.interactions;

import java.util.ArrayList;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.item.ArmorStandEntity;
import net.minecraft.entity.item.TNTEntity;
import net.minecraft.entity.monster.CreeperEntity;
import net.minecraft.entity.monster.SkeletonEntity;
import net.minecraft.entity.monster.WitherSkeletonEntity;
import net.minecraft.entity.passive.SheepEntity;
import net.minecraft.entity.passive.SquidEntity;
import net.minecraft.entity.passive.horse.SkeletonHorseEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.TInventoryHelper;
import net.tardis.mod.registries.SonicModeRegistry.SonicModeEntry;
import net.tardis.mod.sonic.AbstractSonicMode;

/**
 * Created by Swirtzly
 * on 17/03/2020 @ 08:37
 */
public class SonicEntityInteraction extends AbstractSonicMode {

    public SonicEntityInteraction(SonicModeEntry mode) {
		super(mode);
	}

	@Override
    public boolean processBlock(PlayerEntity player, BlockState blockState, ItemStack sonic, BlockPos pos) {
        PlayerHelper.sendMessageToPlayer(player, Constants.Translations.INVALID_SONIC_RESULT, true);
        return false;
    }

    @Override
    public boolean processEntity(PlayerEntity user, Entity targeted, ItemStack sonic) {
        if (user.world.isRemote) return false;
        if (user.isHandActive()) {
            //if (!user.getCooldownTracker().hasCooldown(sonic.getItem()) && user.isHandActive()) {
            if (targeted instanceof CreeperEntity && TConfig.SERVER.detonateCreeper.get()) {
                if (handleDischarge(user, sonic, 15)) {
                    CreeperEntity creeper = (CreeperEntity) targeted;
                    creeper.ignite();
                    return true;
                } else {
                    return false;
                }
            }

            if (targeted instanceof TNTEntity) {
                targeted.remove();
                InventoryHelper.spawnItemStack(targeted.world, targeted.getPosX(), targeted.getPosY(), targeted.getPosZ(), new ItemStack(Items.GUNPOWDER, 3));
                return true;
            }

            if (targeted instanceof SheepEntity && TConfig.SERVER.shearSheep.get()) {
                if (handleDischarge(user, sonic, 15)) {
                    SheepEntity sheep = (SheepEntity) targeted;
                    if (!sheep.getSheared()) {
                        sheep.shear(SoundCategory.PLAYERS);
                        return true;
                    }
                } else {
                    return false;
                }
            }

            if (targeted instanceof SkeletonEntity || targeted instanceof SkeletonHorseEntity || targeted instanceof WitherSkeletonEntity) {
                if (TConfig.SERVER.dismantleSkeleton.get()) {
                    if (handleDischarge(user, sonic, 50)) {
                        //TODO Cost 50, this is quite op
                        //TODO: Add a high frequency mode
                        //                 	InventoryHelper.spawnItemStack(user.world, targeted.posX, targeted.posY, targeted.posZ, new ItemStack(Items.BONE, user.world.rand.nextInt(4) + 1));
                        //                  InventoryHelper.spawnItemStack(user.world, targeted.posX, targeted.posY, targeted.posZ, new ItemStack(Items.SKELETON_SKULL, 1));
                        TInventoryHelper.dropEntityLoot(targeted, user);
                        targeted.remove();

                        //ADDED COOLDOWN UNTIL CHARGE IS DEVELOPED
                        //                  user.getCooldownTracker().setCooldown(sonic.getItem(), 200);
                        return true;
                    } else {
                        return false;
                    }
                }
            }

            if (targeted instanceof SquidEntity && TConfig.SERVER.inkSquid.get()) {
                if (handleDischarge(user, sonic, 15)) {
                    SquidEntity squid = (SquidEntity) targeted;
                    squid.attackEntityFrom(DamageSource.GENERIC, 1);
                    squid.squirtInk();
                    return true;
                } else {
                    return false;
                }
            }

            if (targeted instanceof ArmorStandEntity) {
                if (handleDischarge(user, sonic, 5)) {
                    ArmorStandEntity standEntity = (ArmorStandEntity) targeted;
                    standEntity.setShowArms(!standEntity.getShowArms());
                    return true;
                } else {
                    return false;
                }
            } else {
                PlayerHelper.sendMessageToPlayer(user, Constants.Translations.INVALID_SONIC_RESULT, true);
                return false;
            }
        }

        return false;
    }

    @Override
    public boolean hasAdditionalInfo() {
        return true;
    }

    @Override
    public void updateHeld(PlayerEntity playerEntity, ItemStack stack) {

    }

    @Override
    public ArrayList<TranslationTextComponent> getAdditionalInfo() {
        ArrayList<TranslationTextComponent> list = new ArrayList<>();
        list.add(new TranslationTextComponent("sonic.modes.info.interactable_entities"));

        EntityType[] useableOn = new EntityType[]{EntityType.SHEEP, EntityType.CREEPER, EntityType.SQUID, EntityType.SKELETON, EntityType.WITHER_SKELETON, EntityType.SKELETON_HORSE, EntityType.ARMOR_STAND};

        for (EntityType entityType : useableOn) {
            list.add((TranslationTextComponent) new TranslationTextComponent("- ").appendSibling(new TranslationTextComponent(entityType.getTranslationKey()).mergeStyle(TextFormatting.GRAY)));
        }
        return list;
    }

    @Override
    public void processSpecialEntity(PlayerInteractEvent.EntityInteract event) {
        //Armor stands do not allow items to interact with them properly, as right clicking on a armor stand
        //Puts the users active item into the armor stands hand, we don't want this behaviour unless the player is sneaking
        if (event.getEntityLiving() instanceof ArmorStandEntity) {
            event.setCanceled(true);
        }
        processEntity(event.getPlayer(), event.getTarget(), event.getItemStack());
    }
}
