package net.tardis.mod.sonic;

import java.util.ArrayList;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.registries.SonicModeRegistry.SonicModeEntry;
import net.tardis.mod.sonic.capability.ISonic;
import net.tardis.mod.sonic.capability.SonicCapability;

public abstract class AbstractSonicMode {
	private SonicModeEntry mode;
	
	public AbstractSonicMode(SonicModeEntry mode) {
		this.mode = mode;
	}
	
    public boolean processBlock(PlayerEntity player, BlockState blockState, ItemStack sonic, BlockPos pos) {
		return false;
    }

    public boolean processEntity(PlayerEntity user, Entity targeted, ItemStack sonic) {
    	return false;
    }

    public String getLangKey() {
        return "sonic.mode." + getMode().getRegistryName().getPath();
    }

    public String getDescriptionLangKey() {
        return "sonic.mode." + getMode().getRegistryName().getPath() + ".desc";
    }
    
    public SonicModeEntry getMode() {
    	return this.mode;
    }

    public ArrayList<TranslationTextComponent> getAdditionalInfo() {
        return new ArrayList<>();
    }

    public boolean hasAdditionalInfo() {
    	return false;
    }

    public void updateHeld(PlayerEntity playerEntity, ItemStack stack) {
    	
    }

    public boolean consumeCharge(float amount, ItemStack stack) {
        return true;
    }

    public double getReachDistance() {
        return 15D;
    }

    public void processSpecialBlocks(PlayerInteractEvent.RightClickBlock event) {

    }

    public void processSpecialEntity(PlayerInteractEvent.EntityInteract event) {

    }

    public boolean handleDischarge(Entity entity, ItemStack stack, float dischargeAmount) {
        ISonic data = SonicCapability.getForStack(stack).orElseGet(null);
        float currentCharge = data.getCharge();
        if (dischargeAmount > data.getCharge()) {
            if (entity instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) entity;
                PlayerHelper.sendMessageToPlayer(player, new TranslationTextComponent("message.tardis.not_enough_charge", dischargeAmount - data.getCharge()), true);
            }
            return false;
        } else {
            data.setCharge(currentCharge - dischargeAmount);
            return true;
        }
    }
}