package net.tardis.mod.recipe;

import java.util.Collection;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.items.wrapper.RecipeWrapper;
import net.tardis.mod.schematics.Schematic;
import net.tardis.mod.schematics.Schematics;
import net.tardis.mod.tileentities.machines.NeutronicSpectrometerTile;
/** Recipe type for items that are turned into Unlock Schematics.
 * These unlock schematics are added to a Sonic, which can then be used to unlock objects for a Tardis*/
public class SpectrometerRecipe implements IRecipe<RecipeWrapper> {

    /** A non-empty stack to return as the 'result' so JEI doesn't piss itself */
    private static final ItemStack DUMMY = new ItemStack(Items.APPLE);

    public Ingredient input;
    public int ticks;
    private ResourceLocation registryName;
    private ResourceLocation schematic;
    
    public static final int DEFAULT_TICKS = 200;

    public static Collection<SpectrometerRecipe> getAllRecipes(World world){
        return world.getRecipeManager().getRecipesForType(TardisRecipeSerialisers.SPECTROMETER_TYPE);
    }

    public SpectrometerRecipe(int ticks, Ingredient input, ResourceLocation schematic){
        this.input = input;
        this.schematic = schematic;
        this.ticks = ticks;
    }

    @Override
    public boolean matches(RecipeWrapper inv, World worldIn) {

        ItemStack input = inv.getStackInSlot(NeutronicSpectrometerTile.INPUT_SLOT);

        //If the machine is empty
        if(input.isEmpty())
            return false;

        return this.input.test(input);
    }

    /** Get number of ticks required to process the input ingredient*/
    public int getTicks(){
        return this.ticks;
    }

    /** Get the input ingredient */
    public Ingredient getIngredient() {
        return this.input;
    }

    public ResourceLocation getSchematic(){
        return this.schematic;
    }
    
    public SpectrometerRecipe setRegistryName(ResourceLocation id) {
        this.registryName = id;
        return this;
    }

    public Schematic getSchematicObject() {
       return Schematics.SCHEMATIC_REGISTRY.get(this.schematic);
    }

    /**The recipe's itemstack crafting table result. Don't call this anywhere as our output is actually a schematic */
    @Override
    public ItemStack getCraftingResult(RecipeWrapper inv) {
        return DUMMY;
    }

    @Override
    public boolean canFit(int width, int height) {
        return true;
    }

    /**The recipe's itemstack result. Don't call this anywhere as our output is actually a schematic */
    @Override
    public ItemStack getRecipeOutput() {
        return DUMMY;
    }

    @Override
    public ResourceLocation getId() {
        return this.registryName;
    }

    /** Gets the ingredients in the WeldRecipe. <br> Use this for JEI display purposes*/
    @Override
    public NonNullList<Ingredient> getIngredients() {
        NonNullList<Ingredient> nonnulllist = NonNullList.create();
        nonnulllist.add(this.input);
        return nonnulllist;
    }

    @Override
    public IRecipeSerializer<?> getSerializer() {
        return TardisRecipeSerialisers.SPECTROMETER_SERIALIZER.get();
    }

    @Override
    public IRecipeType<?> getType() {
        return TardisRecipeSerialisers.SPECTROMETER_TYPE;
    }
    
    public JsonObject serialise() {
        JsonObject root = new JsonObject();
        root.add("type", new JsonPrimitive(TardisRecipeSerialisers.SPECTROMETER_TYPE_LOC.toString()));
        root.add("processing_ticks", new JsonPrimitive(this.ticks));
        root.add("ingredient", this.input.serialize());
        root.add("result", new JsonPrimitive(this.schematic.toString()));
        return root;
    }
    
    public static SpectrometerRecipe deserialiseFromJson(ResourceLocation recipeId, JsonObject inputJson) {
        int ticks = 200;
        if(inputJson.has("processing_ticks"))
            ticks = inputJson.get("processing_ticks").getAsInt();

        SpectrometerRecipe recipe = new SpectrometerRecipe(ticks, Ingredient.deserialize(inputJson.get("ingredient")), new ResourceLocation(inputJson.get("result").getAsString()));
        recipe.setRegistryName(recipeId);
        return recipe;
    }
    
    public static SpectrometerRecipe deserialiseFromPacketBuffer(ResourceLocation recipeId, PacketBuffer buffer) {
        SpectrometerRecipe recipe = new SpectrometerRecipe(buffer.readInt(), Ingredient.read(buffer), buffer.readResourceLocation());
        recipe.setRegistryName(recipeId);
        return recipe;
    }
}
