package net.tardis.mod.entity.projectiles;

import net.minecraft.block.BlockState;
import net.minecraft.block.TNTBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.TNTEntity;
import net.minecraft.entity.projectile.ThrowableEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.ai.WeaponTypes;

/**
 * Created by Swirtzly
 * on 06/10/2019 @ 12:17
 */
public class LaserEntity extends ThrowableEntity {

    public float damage = 10F, scale = 0.5F;
    public Vector3d color = new Vector3d(0, 1, 1);
    private DamageSource source = DamageSource.GENERIC;
    private WeaponTypes.IWeapon weapon;
    private LivingEntity thrower;

    public LaserEntity(EntityType<? extends ThrowableEntity> type, World worldIn) {
        super(type, worldIn);
    }

    public LaserEntity(EntityType<? extends ThrowableEntity> type, double x, double y, double z, World worldIn) {
        super(type, x, y, z, worldIn);
    }


    public LaserEntity(EntityType<? extends ThrowableEntity> type, LivingEntity livingEntityIn, World worldIn) {
        super(type, livingEntityIn, worldIn);
        this.thrower = livingEntityIn;
    }

    public LaserEntity(EntityType<? extends ThrowableEntity> type, double x, double y, double z, World worldIn, LivingEntity livingEntityIn, float damage, DamageSource source, Vector3d color) {
        super(type, x, y, z, worldIn);
        this.damage = damage;
        this.color = color;
        this.source = source;
        this.thrower = livingEntityIn;
    }
    
    public LaserEntity(EntityType<? extends ThrowableEntity> type, World worldIn, LivingEntity livingEntityIn, float damage, DamageSource source, Vector3d color) {
        super(type, livingEntityIn, worldIn);
        this.damage = damage;
        this.color = color;
        this.source = source;
        this.thrower = livingEntityIn;
    }


    public LaserEntity(World world) {
        this(TEntities.LASER.get(), world);
    }


    public void setWeaponType(WeaponTypes.IWeapon weap) {
        this.weapon = weap;
    }

    @Override
    public void tick() {
        super.tick();
        double speed = new Vector3d(getPosX(), getPosY(), getPosZ()).distanceTo(new Vector3d(prevPosX, prevPosY, prevPosZ));
        if (!this.world.isRemote && (ticksExisted > 30 * 20 || speed < 0.01)) {
            this.remove(); //This removes the laser if it goes too far away
        }
        if (isAlive()) {
            super.tick();
        }
    }

    @Override
    protected void onImpact(RayTraceResult result) {
        if (result == null || !isAlive())
            return;

        //Entity Hit
        if (result.getType() == RayTraceResult.Type.ENTITY) {
            EntityRayTraceResult entityHitResult = ((EntityRayTraceResult) result);
            if (entityHitResult.getEntity() == this.getShooter() || entityHitResult == null) return;
            Entity hitEntity = entityHitResult.getEntity();
            hitEntity.attackEntityFrom(source, weapon == null ? damage : weapon.damage());
        }
        //Block Hit
        else if (result.getType() == RayTraceResult.Type.BLOCK) {
            BlockRayTraceResult blockResult = (BlockRayTraceResult) result;
            BlockState block = world.getBlockState(blockResult.getPos());
            if (block.getBlock() instanceof TNTBlock) {
                BlockPos pos = blockResult.getPos();
                world.removeBlock(pos, false);
                TNTEntity tntEntity = new TNTEntity(world, (float) pos.getX() + 0.5F, pos.getY(), (float) pos.getZ() + 0.5F, (LivingEntity) getShooter());
                tntEntity.setFuse(0);
                world.addEntity(tntEntity);
                world.playSound(null, tntEntity.getPosX(), tntEntity.getPosY(), tntEntity.getPosZ(), SoundEvents.ENTITY_TNT_PRIMED, SoundCategory.BLOCKS, 1.0F, 1.0F);
                if (world.isRemote()) {
                    world.addParticle(ParticleTypes.SMOKE, true, this.getPosX(), this.getPosY(), this.getPosZ(), 0, 0, 0);
                }
            }
        }

        if (this.weapon != null) {
            this.weapon.onHit(this, result);
        }

        if (!this.world.isRemote) {
            this.remove();
        }
    }

    public void setColor(Vector3d color) {
        this.color = color;
    }

    public Vector3d getColor() {
        return color;
    }

    public void setSource(DamageSource source) {
        this.source = source;
    }

    public DamageSource getSource() {
        return source;
    }

    public float getDamage() {
        return damage;
    }

    public void setDamage(float damage) {
        this.damage = damage;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public LivingEntity getThrower() {
        return thrower;
    }

    @Override
    protected void registerData() {

    }

    @Override
    public void readAdditional(CompoundNBT compoundNBT) {
        super.readAdditional(compoundNBT);
        this.damage = compoundNBT.getFloat("damage");
        this.color = new Vector3d(compoundNBT.getDouble("red"), compoundNBT.getDouble("green"), compoundNBT.getDouble("blue"));
        this.scale = compoundNBT.getFloat("scale");
    }

    @Override
    public void writeAdditional(CompoundNBT compoundNBT) {
        super.writeAdditional(compoundNBT);
        compoundNBT.putFloat("damage", damage);
        compoundNBT.putDouble("red", color.x);
        compoundNBT.putDouble("green", color.y);
        compoundNBT.putDouble("blue", color.z);
        compoundNBT.putFloat("scale", scale);

    }

    @Override
    public boolean isInWater() {
        return false;
    }

    @Override
    public IPacket<?> createSpawnPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    @Override
    protected float getGravityVelocity() {
        return 0.0F;
    }
}
