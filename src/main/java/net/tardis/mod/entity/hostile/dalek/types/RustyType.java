package net.tardis.mod.entity.hostile.dalek.types;

import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.particles.ParticleTypes;
import net.tardis.mod.entity.hostile.dalek.DalekEntity;

public class RustyType extends DalekType {

    @Override
    public void setupDalek(DalekEntity dalekEntity) {
        super.setupDalek(dalekEntity);
        dalekEntity.getAttribute(Attributes.MAX_HEALTH).setBaseValue(10);
        dalekEntity.getAttribute(Attributes.MOVEMENT_SPEED).setBaseValue(0.1D);
        dalekEntity.getAttribute(Attributes.ATTACK_DAMAGE).setBaseValue(2D);
    }

    @Override
    public void tickSpecial(DalekEntity dalekEntity) {
        super.tickSpecial(dalekEntity);

        if (dalekEntity.ticksExisted % 25 == 0) {
            for (int i = 0; i < 2; ++i) {
                dalekEntity.world.addParticle(ParticleTypes.LARGE_SMOKE, dalekEntity.getPosXRandom(0.5D), dalekEntity.getPosYRandom(), dalekEntity.getPosZRandom(0.5D), 0.0D, 0.0D, 0.0D);
            }
        }
    }
}
