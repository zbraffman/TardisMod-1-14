package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
/** A TileEntity (BlockEntity) providing Block but the block property is not solid*/
public class NotSolidTileBlock extends TileBlock {

    public NotSolidTileBlock(Block.Properties prop) {
        super(prop.notSolid().setSuffocates((blockState, reader, blockPos) -> false));
    }

    //My tiles need these more often then not so..
    @Override
    public int getOpacity(BlockState state, IBlockReader worldIn, BlockPos pos) {
        return 0;
    }
}
