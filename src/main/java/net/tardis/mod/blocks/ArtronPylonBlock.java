package net.tardis.mod.blocks;

import net.tardis.mod.properties.Prop;

public class ArtronPylonBlock extends NotSolidTileBlock {

	public ArtronPylonBlock() {
		super(Prop.Blocks.BASIC_TECH.get());
	}

}
